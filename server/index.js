// [LOAD PACKAGES]
const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const controller = require('./controller/route');

// [ CONFIGURE mongoose ]

// CONNECT TO MONGODB SERVER
const db = mongoose.connection;
db.on('error', console.error);
db.once('open', () => {
  // CONNECTED TO MONGODB SERVER
  console.log('Connected to mongod server');
});

mongoose.connect('mongodb://circulus:circulus0314@dev.circul.us:27017/circulus');

// [CONFIGURE APP TO USE bodyParser]
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// [CONFIGURE SERVER PORT]

const port = 8080;

app.use('/api', controller);
app.use('/api/bot', require('./routes/bot'));
app.use('/api/pibo', require('./routes/pibo'));
app.use('/api/botstore', require('./routes/botstore'));
app.use('/api/user', require('./routes/user'));

// [RUN SERVER]
const server = app.listen(port, () => {
  console.log(`pibo-webapp-server has started on port ${port}`);
});
