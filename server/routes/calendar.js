const express = require("express");
const router = express.Router();
const Calendar = require("../models/Calendar");

router.get("/getMonthData", function(req, res) {
  if (req.query && !req.query.startDate && !req.query.endDate) {
    res.status(403).end();
  } else {
    Calendar.aggregate([
      {
        $match: {
          execDate: {
            $gte: new Date(req.query.startDate),
            $lte: new Date(req.query.endDate)
          }
        }
      },
      {
        $group: {
          _id: {
            $toLower: {
              $dayOfMonth: { date: "$execDate", timezone: "Asia/Seoul" }
            }
          },
          data: { $push: "$$ROOT" },
          scheduleCount: {
            $sum: { $cond: [{ $eq: ["$kind", "schedule"] }, 1, 0] }
          },
          recordCount: { $sum: { $cond: [{ $eq: ["$kind", "record"] }, 1, 0] } }
        }
      },
      {
        $group: {
          _id: null,
          dataList: {
            $push: {
              k: "$_id",
              v: {
                data: "$data",
                scheduleCount: "$scheduleCount",
                recordCount: "$recordCount"
              }
            }
          }
        }
      },
      {
        $replaceRoot: {
          newRoot: { $arrayToObject: "$dataList" }
        }
      }
    ]).exec((err, monthData) => {
      if (err) {
        res.status(500).send(err);
      }
      res.json({ monthData });
    });
  }
});

router.get("/getCalendar/:cuid", function(req, res) {
  Calendar.findOne({ cuid: req.params.cuid }).exec((err, calendar) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ calendar });
  });
});

function categoryCheck(body) {
  if (body.category === "schedule") {
    if (body.title || body.repeat || body.execDate) {
      return true;
    }
  } else if (body.category === "record") {
    if (body.title || body.recordUrl) {
      return true;
    }
  }
  return false;
}

router.post("/setCalendar", function(req, res) {
  if (categoryCheck(req.body)) {
    res.status(403).end();
  } else {
    const newCalendar = new Calendar(req.body);
    newCalendar.save((err, saved) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.json({ calendar: saved, success: true });
      }
    });
  }
});

router.delete("/deleteCalendar", function(req, res) {
  Calendar.findOne({ cuid: req.params.cuid }).exec((err, calendar) => {
    if (err) {
      res.status(500).send(err);
    }

    calendar.remove(() => {
      res.status(200).end();
    });
  });
});

module.exports = router;
