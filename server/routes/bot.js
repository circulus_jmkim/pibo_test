const express = require("express");
const Bot = require("../models/Bot");
const Pibo = require("../models/Pibo");
const router = express.Router();

const getCategoryIndex = item => {
  switch (item) {
    case "news":
      return 1;
    case "entertain":
      return 2;
    case "management":
      return 3;
    default:
      return 0;
  }
};

// 모든 봇 정보 가져오기
// bots - fetchBotsList(pid)
router.get("/", (req, res) => {
  Bot.find((err, bot) => {
    if (err) return res.status(500).send({ error: "database failure" });
    return res.json(bot);
  });
});

// 카테고리 별 봇 정보 가져오기
router.get("/category/:category", (req, res) => {
  const { category } = req.params;
  Bot.aggregate(
    [
      { $sort: { lastTime: -1 } },
      { $match: { category: getCategoryIndex(category) } },
      {
        $project: {
          title: 1,
          detail: 1,
          icon: 1,
          comments: 1,
          score: 1
        }
      },
      { $limit: 4 }
    ],
    (err, bot) => {
      if (err) return res.status(500).send({ error: "database failure" });
      const result = { result: true, category, data: bot };
      return res.json(result);
    }
  );
});

// 내 봇 리스트 가져오기
// bots - getBotsList(pid)
router.get("/:pid", (req, res) => {
  const { pid } = req.params;
  Pibo.aggregate(
    [{ $match: { robotId: pid } }, { $project: { bot: 1, _id: 0 } }],
    (err, bot) => {
      if (err) return res.status(500).send({ error: "database failure" });
      const data = bot ? bot[0].bot : [];
      const result = { result: true, data };
      console.log(result);
      return res.json(result);
    }
  );
});

// 봇 검색 결과 가져오기
// bots - searchResultList
router.get("/search/:searchStr", (req, res) => {
  const { searchStr } = req.params;
  const regStr = new RegExp(searchStr, "ig");
  Bot.aggregate(
    [
      { $match: { title: { $regex: regStr } } },
      {
        $project: {
          projectId: 1,
          title: 1,
          category: 1,
          score: 1,
          icon: 1,
          lastTime: 1,
          reviews: 1,
          detail: 1
        }
      },
      { $sort: { lastTime: -1 } }
    ],
    (err, bot) => {
      if (err) return res.status(500).send({ error: "database failure" });
      const result = { result: true, data: bot };
      return res.json(result);
    }
  );
});

// 내 봇의 검색 결과 가져오기
// botstore - my
// bot
router.get("/search/:pid/:searchStr", (req, res) => {
  const { searchStr, pid } = req.params;
  const regStr = new RegExp(searchStr, "ig");
  console.log(searchStr, pid);
  Pibo.aggregate(
    [
      { $match: { robotId: pid } },
      { $unwind: "$bot" },
      { $match: { "bot.title": regStr } },
      { $group: { _id: null, bot: { $push: "$bot" } } },
      { $project: { bot: 1, _id: 0 } }
    ],
    (err, output) => {
      if (err) return res.status(500).send({ error: "database failure" });
      const result = { result: true, data: output[0].bot };
      return res.json(result);
    }
  );
});

// 로봇의 봇 사용여부 변경
// bots - updateBotsList(pid, bid, use)
router.put("/update/:pid/", (req, res) => {
  const { pid } = req.params;
  const { bid, use } = req.body;
  Pibo.updateOne(
    { robotId: pid, "bot.id": bid },
    { $set: { "bot.$.use": use } },
    (err, output) => {
      if (err) return res.status(500).send({ error: "update failure" });
      if (!output.n)
        return res.status(404).json({ result: false, error: "book not found" });
      return res.json({ result: true });
    }
  );
});

// 로봇의 설치 봇 삭제
// bots = deleteBot(pid, bid)
router.put("/remove/", (req, res) => {
  const { pid, bid } = req.body;
  Pibo.updateOne(
    { robotId: pid, "bot.id": bid },
    { $set: { "bot.$.remove": true, "bot.$.use": false } },
    { upsert: true },
    (err, bot) => {
      if (err) return res.status(500).send({ error: "update failure" });
      return res.json(bot);
    }
  );
});

module.exports = router;
