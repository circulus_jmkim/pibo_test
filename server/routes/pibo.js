const express = require("express");
const mongoose = require("mongoose");
const User = require("../models/User");
const Pibo = require("../models/Pibo");
const router = express.Router();

// 파이보 데이터 저장
router.post("/", async (req, res) => {
  const { uid, pid, locale } = req.body;
  const time = new Date().valueOf();
  const pibo = new Pibo();
  pibo.address = "";
  pibo.bot = [];
  pibo.count = 0;
  pibo.detail = "";
  pibo.firstTime = time;
  pibo.geo = "";
  pibo.like = 0;
  pibo.icon = "";
  pibo.interest = [];
  pibo.lastTime = time;
  pibo.locale = locale;
  pibo.name = "pibo";
  pibo.robotId = pid;
  pibo.servo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  pibo.temp = 26;
  pibo.use = true;

  const piboPromise = await pibo.save();
  const userPromise = await User.updateOne(
    { _id: new mongoose.Types.ObjectId(uid) },
    {
      $set: { hw_enable: [pid] },
      $addToSet: { hw_list: pid }
    }
  );
  const [piboResult, userResult] = await Promise.all([
    piboPromise,
    userPromise
  ]);

  if ("robotId" in piboResult && userPromise.ok) {
    return res.json({ result: true, robotId: piboResult.robotId });
  }

  return res.json({ result: false, robotId: "" });
});

// 파이보 데이터 모두 가져오기
// pibo - getPiboData(pid)
router.get("/:pid", (req, res) => {
  const { pid } = req.params;
  console.log(pid);
  Pibo.aggregate(
    [
      { $match: { robotId: pid, use: true } },
      {
        $project: {
          _id: 0,
          name: 1,
          locale: 1,
          geo: 1,
          volume: 1,
          servo: 1,
          tempUnit: 1,
          bot: {
            $filter: {
              input: "$bot",
              as: "bot",
              cond: { $eq: ["$$bot.favorite", true] }
            }
          }
        }
      }
    ],
    (err, output) => {
      if (err) return res.status(500).send({ error: "database failure" });
      if (output && output.length > 0) {
        const pibo = output[0];
        const result = {
          result: true,
          data: {
            ...pibo
          }
        };
        console.log(result);
        return res.json(result);
      }
    }
  );
});

// 파이보 데이터 항목 별 가져오기 fetchCogList(pid, item)
router.get("/:pid/:item", (req, res) => {
  const { pid, item } = req.params;
  if (!(pid && item)) {
    return res.status(402).json({ error: "unvalid params" });
  }
  const getList = fetchItem => {
    console.log(fetchItem);
    switch (fetchItem) {
      case "name":
        return ["pibo", "mibo"];
      case "tempUnit":
        return ["celsius", "fahrenheit"];
      case "servo":
        return [];
      case "wifi":
        return [
          {
            address: "90:9F:33:A8:37:0C",
            signal: 100,
            encrypted: true,
            wpa: true,
            essid: "circulus5G"
          },
          {
            address: "90:9F:33:A9:37:0C",
            signal: 100,
            encrypted: true,
            wpa: true,
            essid: "circulus"
          },
          {
            address: "62:01:94:35:A1:F3",
            signal: 95,
            encrypted: true,
            wpa: true,
            essid: "Cozmo_61509E"
          }
        ];
      case "locale":
        return ["kr", "en", "zh", "jp"];
      case "instruction":
        return [];
      case "use":
        return [true, false];
      default:
        return [];
    }
  };
  if (item === "wifi") {
    return res.json({
      result: true,
      data: {
        item,
        list: getList(item),
        value: {
          address: "90:9F:33:A8:37:0C",
          signal: 100,
          encrypted: true,
          wpa: true,
          essid: "circulus5G"
        }
      }
    });
  }
  Pibo.findOne({ robotId: pid, use: true }, { [item]: 1 }, (err, cog) => {
    if (err) res.status(500).json({ error: "database failure" });
    const result = {
      result: true,
      data: {
        item,
        list: getList(item),
        value: cog[item]
      }
    };
    console.log(result);
    return res.json(result);
  });
});

// 파이보 설정 저장 savePiboCog(pid, data)
router.put("/:pid", (req, res) => {
  console.log("파이보 설정 저장", req.params.id, req.body);
  Pibo.updateOne(
    { robotId: req.params.pid, use: true },
    { $set: req.body },
    (err, output) => {
      if (err)
        res.status(500).json({ result: false, error: "database failure" });
      if (!output.n)
        return res.status(404).json({ result: false, error: "book not found" });
      return res.json({ result: true });
    }
  );
});

module.exports = router;
