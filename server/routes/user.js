const express = require("express");
const mongoose = require("mongoose");
const User = require("../models/User");
const router = express.Router();

router.get("/:uid", (req, res) => {
  const { uid } = req.params;
  User.aggregate(
    [
      { $match: { _id: new mongoose.Types.ObjectId(uid) } },
      {
        $project: {
          gender: 1,
          userId: 1,
          birthDate: 1,
          firstName: 1,
          interest: 1,
          lastName: 1,
          nickName: 1,
          userImg: 1,
          email: 1
        }
      },
      { $limit: 1 }
    ],
    (err, user) => {
      if (err) return res.status(500).send({ error: "database failure" });
      console.log(user);
      const result = {
        result: true,
        data: user && user[0]
      };
      return res.json(result);
    }
  );
});

router.get("/check/:uid", (req, res) => {
  const { uid } = req.params;
  User.aggregate(
    [
      { $project: { existId: { $eq: ["$userId", uid] }, _id: 0 } },
      { $match: { existId: true } }
    ],
    (err, user) => {
      if (err) return res.status(500).send({ error: "database failure" });
      const result = {
        result: !(user && user.length)
      };
      console.log(result);
      return res.json(result);
    }
  );
});

router.post("/signup", (req, res) => {
  console.log(req.body);
  const { password, ...rest } = req.body;
  const user = new User({ ...rest });
  user.password = user.generateHash(password);
  user.save((err, userdata) => {
    if (err) {
      res.json({ result: false, error: err });
      return;
    }
    res.json({ result: true, _id: userdata._id });
  });
});

router.post("/signin", (req, res) => {
  User.findOne({ userId: req.body.id, use: true }, async (err, user) => {
    if (err) return res.status(500).send({ error: "database failure" });
    if (!user) return res.json({ result: false, error: "unexist id" });
    const validPw = user.validPassword(req.body.pw);
    if (validPw) {
      const pid = user.hw_enable.length > 0 ? user.hw_enable[0] : "";
      const uid = user._id;
      const nickName = "nickName" in user ? user.nickName : "";
      const interest = "interest" in user ? user.interest : [];
      return res.json({ result: true, data: { uid, pid, nickName, interest } });
    }
    return res.json({ result: false, error: "unvalid password" });
  });
});

router.put("/update", (req, res) => {
  const {
    birthDate,
    email,
    firstName,
    interest,
    lastName,
    nickName,
    uid
  } = req.body;
  User.updateOne(
    { _id: new mongoose.Types.ObjectId(uid) },
    {
      $set: {
        birthDate,
        email,
        firstName,
        interest,
        lastName,
        nickName
      }
    },
    (err, output) => {
      if (err) return res.status(500).send({ error: "update failure" });
      if (!output.n || !output.nModified)
        return res.status(404).json({ result: false, error: "item not found" });
      return res.json({ result: true });
    }
  );
});

router.put("/withdraw", (req, res) => {
  User.updateOne(
    { _id: new mongoose.Types.ObjectId(req.body.uid) },
    {
      $set: {
        use: false
      }
    },
    (err, output) => {
      if (err) return res.status(500).send({ error: "update failure" });
      if (!output.n || !output.nModified)
        return res.status(404).json({ result: false, error: "item not found" });
      return res.json({ result: true });
    }
  );
});

module.exports = router;
