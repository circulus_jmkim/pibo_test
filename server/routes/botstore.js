const express = require("express");
const mongoose = require("mongoose");
const Review = require("../models/Bot");
const Bot = require("../models/Bot");
const router = express.Router();

const getCategoryItem = idx => {
  switch (idx) {
    case 1:
      return "news";
    case 2:
      return "entertain";
    case 3:
      return "management";
    default:
      return 0;
  }
};
// 봇 데이터 필요한 항목 추가하여 업데이트
router.put("/upsert/:bid", (req, res) => {
  const { bid } = req.params;
  Bot.updateOne(
    { _id: new mongoose.Types.ObjectId(bid) },
    {
      $set: {
        icon: "../image/image.png",
        title: "제목 대략 10자 이내",
        category: 2,
        userId: "pibo",
        score: 10.0,
        comments: [
          {
            id: "id1234",
            title: "first review",
            gpa: 4,
            name: "Matthew Harris",
            regdate: "2018-07-30",
            content:
              "Leverage agile frameworks to provide a robust synopsis for high level overviews."
          },
          {
            id: "id5678",
            title: "second review",
            gpa: 4,
            name: "Jake Smith",
            regdate: "2018-07-30",
            content:
              "Bring to the table win-win survival strategies to ensure proactive domination."
          },
          {
            id: "id9101",
            title: "third review",
            gpa: 4,
            name: "Elliot Baker",
            regdate: "2018-07-30",
            content:
              "Capitalise on low hanging fruit to identify a ballpark value added activity to beta test."
          },
          {
            id: "id1121",
            title: "fourth review",
            gpa: 4,
            name: "Jenny Hess",
            regdate: "2018-07-30",
            content: "Jenny requested permission to view your contact details"
          }
        ],
        detail: {
          update:
            "A dog is a type of domesticated animal. Known for its loyalty and faithfulness, it can be found as a welcome guest in many households across the world.",
          description: "Official piBO's weather bot",
          requires:
            "Three common ways for a prospective owner to acquire a dog is from pet shops, private owners, or shelters."
        },
        language: "nodejs",
        firstTime: 1517754291786.0,
        lastTime: 1517754291786.0,
        use: true,
        level: 50.0,
        locale: "ko"
      }
    },
    { upsert: true },
    (err, bot) => {
      if (err) return res.status(500).send({ error: "update failure" });
      return res.json(bot);
    }
  );
});

// 리뷰 데이터 저장
router.post("/review", (req, res) => {
  const time = new Date().valueOf();
  const review = new Review();
  review.botId = req.body.bid;
  review.userId = req.body.uid;
  review.title = req.body.title;
  review.content = req.body.content;
  review.score = req.body.score;
  review.locale = req.body.locale;
  review.firstTime = time;
  review.lastTime = time;
  review.save(err => {
    if (err) {
      res.json({ result: 0 });
      return;
    }

    res.json({ result: 1 });
  });
});

router.post("/:bid", async (req, res) => {
  const { bid } = req.params;
  const { pid, uid } = req.body;
  const botPromise = await Bot.aggregate([
    {
      $match: { _id: new mongoose.Types.ObjectId(bid), use: true }
    },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "userId",
        as: "maker"
      }
    },
    {
      $project: {
        icon: 1,
        title: 1,
        category: 1,
        userId: 1,
        score: 1,
        reviews: 1,
        detail: 1,
        like: 1,
        unlike: 1,
        maker: 1,
        level: 1,
        locale: 1
      }
    }
  ]);

  const userPromise = await User.aggregate([
    {
      $match: { _id: new mongoose.Types.ObjectId("5249862db6e916500e000001") }
    },
    {
      $lookup: {
        from: "reviews",
        localField: "userId",
        foreignField: "userId",
        as: "review"
      }
    },
    { $unwind: "$hw_enable" },
    {
      $lookup: {
        from: "pibos",
        localField: "hw_enable",
        foreignField: "robotId",
        as: "pibo"
      }
    },
    { $unwind: "$pibo" },
    {
      $project: {
        hasReview: {
          $in: ["5a7714b9b641a913b880a027", "$review.botId"]
        },
        isInstalled: {
          $in: ["5a7714b9b641a913b880a027", "$pibo.bot.id"]
        },
        nickName: 1,
        review: {
          $filter: {
            input: "$review",
            as: "review",
            cond: { $eq: ["$$review.botId", "5a7714b9b641a913b880a027"] }
          }
        },
        reviewList: 1,
        userImg: 1,
        hw_enable: 1,
        pibo: 1
      }
    }
  ]);

  const [botResult, userResult] = await Promise.all([botPromise, userPromise]);

  const userData = userResult[0];
  const botData = botResult[0];
  const data = {};
  data._id = botData._id;
  data.icon = botData.icon;
  data.title = botData.title;
  data.category = getCategoryItem(botData.category);
  data.user = {
    isInstalled: userData.isInstalled,
    img: userData.userImg,
    name: userData.nickName,
    review: {}
  };

  if (userResult.hasReview) {
    data.user = {
      review: {
        id: userResult.review[0]._id,
        title: userResult.review[0].title,
        score: userResult.review[0].score,
        name: userResult.nickName,
        lastTime: userResult.review[0].lastTime,
        content: userResult.review[0].content
      }
    };
  }
  data.makerImg = botData.maker[0].userImg;
  data.makerId = botData.maker[0].userId;
  data.makerNickname = botData.maker[0].nickName;
  data.score = botData.score;
  data.reviews = botData.reviews;
  data.detail = botData.detail;
  data.locale = botData.locale;
  return res.json({ result: true, data });
});

// 로봇에 봇이 설치되어 있는지 확인
router.get("/install/:pid/:bid", (req, res) => {
  const { pid, bid } = req.params;
  Pibo.find(
    {
      robotId: pid,
      "bot.id": bid,
      "bot.use": true
    },
    (err, output) => {
      if (err) return res.status(500).send({ error: "update failure" });
      if (output.length) {
        return res.json({ result: true });
      }
      return res.json({ result: false });
    }
  );
});

//user가 bot에 대해 쓴 리뷰를 가져온다
router.get("/review/:bid/:uid", (req, res) => {
  const { bid, uid } = req.params;
  Review.findOne({ userId: uid, botId: bid }, (err, output) => {
    if (err) return res.status(500).send({ error: "update failure" });
    return res.json(output);
  });
});

module.exports = router;
