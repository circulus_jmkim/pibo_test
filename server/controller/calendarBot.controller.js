const mongoose = require('mongoose');
const Calendar = require('../models/Calendar');
const Voice = require('../models/Voice');

exports.getMonthData = function (req, res) {
  if (req.query && !req.query.startDate && !req.query.endDate) {
    res.status(403).end();
  } else {
    Calendar.aggregate([{
      $match: {
        $and: [
          {
            execTime: {
              $gte: new Date(req.query.startDate),
              $lte: new Date(req.query.endDate),
            },
          },
          { repeat: 'none' },
        ],
      },
    },
    { $sort: { execTime: 1 } },
    {
      $group: {
        _id: { $toLower: { $dayOfMonth: { date: '$execTime', timezone: 'Asia/Seoul' } } },
        data: { $push: '$$ROOT' },
        scheduleCount: { $sum: { $cond: [{ $eq: ['$kind', 'schedule'] }, 1, 0] } },
        recordCount: { $sum: { $cond: [{ $eq: ['$kind', 'record'] }, 1, 0] } },
      },
    },
    {
      $group: {
        _id: null,
        dataList: {
          $push: {
            k: '$_id',
            v: { data: '$data', scheduleCount: '$scheduleCount', recordCount: '$recordCount' },
          },
        },
      },
    },
    {
      $replaceRoot: {
        newRoot: { $arrayToObject: '$dataList' },
      },
    },
    ]).exec((err, monthData) => {
      if (err) {
        res.status(500).send(err);
      } else {
        Calendar.find({ repeat: { $not: { $eq: 'none' } } }).exec((err2, repeatData) => {
          if (err2) {
            res.status(500).send(err);
          } else {
            res.json({ monthData, repeatData });
          }
        });
      }
    });
  }
};

exports.getCalendar = function (req, res) {
  Calendar.findOne({ cuid: req.params.cuid }).exec((err, calendar) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ calendar });
  });
};

function categoryCheck(body) {
  if (body.category === 'schedule') {
    if (body.title || body.repeat || body.execTime) {
      return true;
    }
  } else if (body.category === 'record') {
    if (body.title || body.recordUrl) {
      return true;
    }
  }
  return false;
}

exports.setCalendar = function (req, res) {
  if (categoryCheck(req.body)) {
    res.status(403).end();
  } else {
    const newCalendar = new Calendar(req.body);
    newCalendar.save((err, saved) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.json({ calendar: saved, success: true });
      }
    });
  }
};

exports.deleteCalendarItems = function (req, res) {
  const idList = req.body.deleteList;
  if (Array.isArray(idList) && idList.length > 0) {
    console.log(idList);
    const ObjectIdList = idList.map(row => new mongoose.Types.ObjectId(row.id));
    Calendar.deleteMany({ _id: { $in: ObjectIdList } }).exec((err, result) => {
      if (err) {
        res.status(500).send(err);
      }
      res.json({ result });
    });
  } else {
    res.status(403).end();
  }
};
exports.getRecords = function (req, res) {
  if (req.query && !req.query.skip && !req.query.limit) {
    res.status(403).send({ error: 'skip, limit required' });
  } else {
    Voice.aggregate( [ { $project : { data : 0 } }, { $sort : { firstTime : -1, _id: -1 } } ] )
    .skip(parseInt(req.query.skip, 10))
    .limit(parseInt(req.query.limit, 10))
    .exec((err, records) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.json({ records });
      }
    });
  }
};