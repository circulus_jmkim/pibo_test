const Dialog = require('../models/Dialog');

exports.deleteHistoryItem = function (req, res) {
  const _id = req.body._id;
  if (req.body && !_id) {
    res.status(403).send({ error: '_id required' });
  } else {
    Dialog.deleteOne({ _id: _id })
    .exec((err, deleteResult) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.json({ deleteResult });
      }
    });
  }
};

exports.getDialogData = function (req, res) {
  if (req.query && !req.query.skip && !req.query.limit) {
    res.status(403).send({ error: 'skip, limit required' });
  } else {
    Dialog.find({ userId : { $exists: true }, robotId : { $exists: true } })
    .sort( { _id: -1 } )
    .skip(parseInt(req.query.skip, 10))
    .limit(parseInt(req.query.limit, 10))
    .exec((err, historyData) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.json({ historyData });
      }
    });
  }
};
