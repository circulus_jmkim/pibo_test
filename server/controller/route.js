const express = require("express");
const router = express.Router();

const CalendarBotController = require("./calendarBot.controller");
const PhotoBotController = require("./photoBot.controller");
const HistoryBotController = require("./historyBot.controller");
const messageBotController = require("./messageBot.controller");

router.route("/calendarBot/getMonthData").get(CalendarBotController.getMonthData);
router.route("/calendarBot/getCalendar/:cuid").get(CalendarBotController.getCalendar);
router.route("/calendarBot/setCalendar").post(CalendarBotController.setCalendar);
router.route("/calendarBot/deleteCalendarItems").delete(CalendarBotController.deleteCalendarItems);
router.route("/calendarBot/getRecords").get(CalendarBotController.getRecords);

router.route("/photoBot/getPhotoData").get(PhotoBotController.getPhotoData);
router.route("/photoBot/setDialogData").get(PhotoBotController.setPhotoData);
router.route("/photoBot/deletePhotos").delete(PhotoBotController.deletePhotos);

router.route("/historyBot/getDialogData").get(HistoryBotController.getDialogData);
router.route("/historyBot/deleteHistoryItem").delete(HistoryBotController.deleteHistoryItem);

router.route("/messageBot/getMessagesData").get(messageBotController.getMessagesData);
router.route("/messageBot/deleteMessageItem").delete(messageBotController.deleteMessageItem);

module.exports = router;
