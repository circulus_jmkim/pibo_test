const Photo = require('../models/Photo');
const mongoose = require('mongoose');

exports.getPhotoData = function (req, res) {
  if (req.query && !req.query.skip && !req.query.limit) {
    res.status(403).send({ error: 'skip, limit required' });
  } else {
    Photo.aggregate( [ { $project : { data : 0 } }, { $sort : { firstTime : -1, _id: -1 } } ] )
    .skip(parseInt(req.query.skip, 10))
    .limit(parseInt(req.query.limit, 10))
    .exec((err, photoData) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.json({ photoData });
      }
    });
  }
};

exports.setPhotoData = function (req, res) {
  const batch = [];
  for (let i = 0; i < 200; i++) {
    const date = new Date('2017-09-01 14:24:14.681+09:00');
    date.setHours(i * 7);
    batch.push(new Photo({ robotId: 'b229480f', firstTime: date, title: 'photo title', uuid: 'f3f0834a2a42d69fb4ab70cfd4926157' }))
  }

  res.json({ result: batch, success: true });

  Photo.insertMany(batch, (err, saved) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.json({ result: saved, success: true });
    }
  });
}

exports.deletePhotos = function (req, res) {
  const idList = req.body.deleteList;
  if (Array.isArray(idList) && idList.length > 0) {
    const ObjectIdList = idList.map(id => new mongoose.Types.ObjectId(id));
    Photo.deleteMany({ _id: { $in: ObjectIdList } }).exec((err, result) => {
      if (err) {
        res.status(500).send(err);
      }
      res.json({ result });
    });
  } else {
    res.status(403).end();
  }
};
