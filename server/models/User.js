const mongoose = require("mongoose");
const bycrypt = require("bcrypt");

const { Schema } = mongoose;

const userSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  __v: Number,
  exp: Number,
  firstTime: { type: Date, required: true, default: new Date() },
  gender: { type: String, required: true },
  hw_list: Array,
  level: Number,
  like: Number,
  loginType: String,
  password: { type: String, require: true },
  race: String,
  type: String,
  use: { type: Boolean, required: true, default: true },
  userId: { type: String, require: true },
  hw_enable: Array,
  birthDate: { type: Date, required: true },
  firstName: { type: String, require: true },
  interest: { type: Array, default: [] },
  lastName: { type: String, require: true },
  nickName: { type: String, require: true, default: ''},
  userImg: String,
  email: { type: String, require: true },
  agreeLocation: { type: Boolean, default: false },
  agreePromotion: { type: Boolean, default: false }
});

userSchema.methods.generateHash = pw =>
  bycrypt.hashSync(pw, bycrypt.genSaltSync(8), null);

userSchema.methods.validPassword = function(pw) {
  return bycrypt.compareSync(pw, this.password);
};

module.exports = mongoose.model("User", userSchema);
