const mongoose = require('mongoose');

const { Schema } = mongoose;

const calendarSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  robotId: String,
  title: String,
  repeat: String,
  execDate: { type: 'Date', required: true },
  type: String,
  bot: String,
  botOrder: String,
  recordUrl: String,
  kind: String,
  createdAt: { type: 'Date', default: Date.now, required: true },
});

module.exports = mongoose.model('calendar', calendarSchema);
