const mongoose = require('mongoose');

const { Schema } = mongoose;

const dialogSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  userId: String,
  robotId: String,
  question: String,
  answer: String,
  emotion: String,
  createdAt: { type: 'Date', default: Date.now, required: true },
});

module.exports = mongoose.model('dialog', dialogSchema);
