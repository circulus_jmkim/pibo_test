const mongoose = require('mongoose');

const { Schema } = mongoose;

const voiceSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  tags: Array,
  comment: Array,
  userId: String,
  uuid: String,
  title: String,
  data : Buffer,
  lastTime: Number,
  use: Number,
});

module.exports = mongoose.model('voice', voiceSchema);
