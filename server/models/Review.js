const mongoose = require("mongoose");

const { Schema } = mongoose;

const reviewSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  botId: String,
  userId: String,
  title: String,
  content: String,
  score: Number,
  firstTime: Number,
  lastTime: Number,
  use: Boolean,
  locale: String
});

module.exports = mongoose.model("Review", reviewSchema);
