const mongoose = require('mongoose');

const { Schema } = mongoose;

const photoSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  use: Boolean,
  firstTime: { type: 'Date', default: Date.now, required: true },
  lastTime: Number,
  data: Buffer,
  title: String,
  uuid: String,
  robotId: String,
  comments: Array,
  tags: Array,
});

module.exports = mongoose.model('photo', photoSchema);
