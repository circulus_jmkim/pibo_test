const mongoose = require('mongoose');

const { Schema } = mongoose;

const messageSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  userId: String,
  robotId: String,
  message: String,
  status: String,  
  firstTime: { type: 'Date', default: Date.now, required: true },
});

module.exports = mongoose.model('message', messageSchema);
