const mongoose = require("mongoose");

const { Schema } = mongoose;

const piboSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  address: String,
  bot: Array,
  count: Number,
  detail: String,
  firstTime: { type: Date, required: true, default: new Date() },
  geo: Array,
  like: Number,
  icon: String,
  interest: Array,
  __v: Number,
  lastTime: { type: Date, required: true, default: new Date() },
  locale: String,
  name: { type: String, required: true, default: "pibo" },
  robotId: { type: String, required: true },
  servo: Array,
  temp: Number,
  tempUnit: { type: String, required: true, default: "celcius" },
  use: { type: Boolean, required: true, default: true }
});

module.exports = mongoose.model("Pibo", piboSchema);
