const mongoose = require("mongoose");

const { Schema } = mongoose;

const botSchema = new Schema({
  _id: { type: Schema.ObjectId, auto: true },
  icon: String,
  title: String,
  category: Number,
  userId: String,
  score: Number,
  reviews: Array,
  detail: {
    update: String,
    description: String,
    requires: String
  },
  projectId: String,
  clones: Array,
  language: String,
  firstTime: Number,
  lastTime: Number,
  like: Number,
  unlike: Number,
  use: Boolean,
  __v: Number,
  level: Number,
  requires: Array,
  count: 0,
  locale: String
});

module.exports = mongoose.model("Bot", botSchema);
