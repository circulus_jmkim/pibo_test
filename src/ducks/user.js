import {
  checkUserId,
  saveNewUserData,
  userSingIn,
  fetchUserData,
  updateUserData,
  withdrawAccount,
} from '../utils/api';
import { setLocalStorage } from '../utils/common';

export const LOADING = 'user/LOADING';
export const ERROR = 'user/ERROR';
export const FETCH_SUCCESS = 'user/FETCH_SUCCESS';
export const UPDATE_SUCCESS = 'user/UPDATE_SUCCESS';
export const WITHDRAW_SUCCESS = 'user/WITHDRAW_SUCCESS';
export const JOIN_PROCESS_SUCCESS = 'user/JOIN_PROCESS_SUCCESS';
export const SIGN_IN_SUCCESS = 'user/SIGN_IN_SUCCESS';
export const SIGN_IN_ERROR = 'user/SIGN_IN_ERROR';
export const SIGN_OUT_SUCCESS = 'user/SIGN_OUT_SUCCESS';

export function loading() {
  return {
    type: LOADING,
  };
}

export function error(errorMsg) {
  return {
    type: ERROR,
    errorMsg,
  };
}

export function signInSuccess({
  pid, uid, nickName, interest,
}) {
  return {
    type: SIGN_IN_SUCCESS,
    toConnect: !pid && pid.length < 1,
    toMain: pid.length > 1 && uid.length > 1,
    toAccount: nickName === '' || interest.length === 0,
  };
}

export function signOutSuccess() {
  return {
    type: SIGN_OUT_SUCCESS,
  };
}

export function signInError(errorMsg) {
  return {
    type: SIGN_IN_ERROR,
    errorMsg,
  };
}

export function fetchInfoSuccess(user) {
  return {
    type: FETCH_SUCCESS,
    user,
  };
}

export function joinProcessSuccess(data) {
  return {
    type: JOIN_PROCESS_SUCCESS,
    account: { data },
  };
}

export function updateInfoSuccess() {
  return {
    type: UPDATE_SUCCESS,
  };
}

export function withdrawSuccess() {
  return {
    type: WITHDRAW_SUCCESS,
  };
}

const initialState = {
  loading: false,
  errorMsg: '',
  user: {},
  account: {},
  toConnect: false,
  toMain: false,
  toAccount: false,
  isComplete: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true,
        isComplete: false,
      };
    case ERROR:
      return {
        ...state,
        loading: false,
        isComplete: false,
        errorMsg: action.errorMsg,
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        errorMsg: '',
        user: action.user,
        isComplete: false,
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        loading: false,
        errorMsg: '',
        isComplete: true,
      };
    case WITHDRAW_SUCCESS:
      return {
        loading: false,
        errorMsg: '',
        user: {},
        account: {},
        toConnect: false,
        toMain: false,
        toAccount: false,
        isComplete: true,
      };
    case JOIN_PROCESS_SUCCESS:
      return {
        ...state,
        errorMsg: '',
        account: action.account,
      };
    case SIGN_OUT_SUCCESS:
      return {
        loading: false,
        errorMsg: '',
        user: {},
        account: {},
        toConnect: false,
        toMain: false,
        toAccount: false,
        isComplete: true,
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        errorMsg: '',
        loading: true,
        toConnect: action.toConnect,
        toMain: action.toMain,
        toAccount: action.toAccount,
      };
    case SIGN_IN_ERROR:
      return {
        ...state,
        errorMsg: action.errorMsg,
        loading: false,
        isComplete: false,
      };
    default:
      return state;
  }
}

export const fetchUserInfo = uid => async (dispatch) => {
  dispatch(loading());
  const res = await fetchUserData(uid);
  if (res.result) {
    dispatch(fetchInfoSuccess(res.data));
  }
};

export const updateUserInfo = (uid, data) => async (dispatch) => {
  dispatch(loading());
  const res = await updateUserData(uid, data);
  if (res.result) {
    dispatch(updateInfoSuccess());
  }
  dispatch(error('업데이트에 실패했습니다.'));
};

export const withdrawUserAccount = uid => async (dispatch) => {
  dispatch(loading());
  const res = await withdrawAccount(uid);
  if (!res.result) {
    localStorage.clear();
    return dispatch(withdrawSuccess());
  }
  return dispatch(error('다시 시도해 주세요.'));
};

export const checkId = uid => async (dispatch) => {
  const res = await checkUserId(uid);
  if (!res.result) {
    return dispatch(error('이미 사용 중인 아이디입니다.'));
  }
  return dispatch(joinProcessSuccess({ userId: uid }));
};

export const insertUserData = userdata => async (dispatch) => {
  dispatch(loading());
  const res = await saveNewUserData(userdata);
  if (!res.result) {
    return dispatch(error('이미 사용 중인 아이디입니다.'));
  }
  return dispatch(joinProcessSuccess({}));
};

export const signIn = logindata => async (dispatch) => {
  dispatch(loading());
  const { id, pw } = logindata;

  if (id.length < 1 || pw.length < 1) {
    return dispatch(signInError('아이디, 패스워드를 입력하세요.'));
  }
  const res = await userSingIn(logindata);
  if (res.result) {
    const {
      uid, pid, nickName, interest,
    } = res.data;
    setLocalStorage('currentUser', { uid, pid, nickName });
    return dispatch(
      signInSuccess({
        uid,
        pid,
        nickName,
        interest,
      }),
    );
  }
  return dispatch(signInError(res.error));
};

export const signOut = () => async (dispatch) => {
  dispatch(loading());
  localStorage.clear();
  dispatch(signOutSuccess());
};
