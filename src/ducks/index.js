import { combineReducers } from 'redux';
import pibo from './pibo';
import bots from './bots';
import botstore from './botstore';
import user from './user';
import connect from './connect';
import historyBot from './bots/historyBot/historyBot';
import photoBot from './bots/photoBot/photoBot';
import messageBot from './bots/messageBot/messageBot';
import calendarBot from './bots/calendarBot/calendarBot';

export default combineReducers({
  pibo,
  bots,
  botstore,
  user,
  connect,
  historyBot,
  photoBot,
  messageBot,
  calendarBot,
});
