import { fetchSearchResults, fetchBotsByCategory, fetchBotDetail } from '../utils/api';

export const LOADING = 'botstore/LOADING';
export const SEARCH_LOADING = 'botstore/SEARCH_LOADING';
export const LIST_FETCH_SUCCESS = 'botstore/LIST_FETCH_SUCCESS';
export const SEARCH_FETCH_SUCCESS = 'botstore/SEARCH_FETCH_SUCCESS';
export const SEARCH_NO_RESULT = 'botstore/SEARCH_NO_RESULT';
export const BOT_FETCH_SUCCESS = 'botstore/BOT_SEARCH_SUCCESS';
export const ERROR = 'botstore/ERROR';

export function storeLoading() {
  return {
    type: LOADING,
  };
}
export function storeSearchLoading(searchStr) {
  return {
    type: SEARCH_LOADING,
    searchStr,
  };
}
export function fetchStoreBotDetailSuccess(bot) {
  return {
    type: BOT_FETCH_SUCCESS,
    bot,
  };
}
export function fetchStoreMainDataSuccess(mainData) {
  return {
    type: LIST_FETCH_SUCCESS,
    mainData,
  };
}
export function fetchSearchResultSuccess(searchResult) {
  return {
    type: SEARCH_FETCH_SUCCESS,
    searchResult,
  };
}
export function fetchSearchNoResult() {
  return {
    type: SEARCH_NO_RESULT,
  };
}
export function storeError() {
  return {
    type: ERROR,
  };
}

const initialState = {
  loading: false,
  searching: false,
  mainData: {},
  searchStr: '',
  searchResult: [],
  errorMsg: '',
  bot: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true,
        searchStr: '',
        searching: false,
        searchResult: [],
      };
    case SEARCH_LOADING:
      return {
        ...state,
        searchStr: action.searchStr,
        searching: true,
      };
    case ERROR:
      return {
        ...state,
        searchStr: '',
        searching: false,
        searchResult: [],
        errorMsg: action.errorMsg,
      };
    case BOT_FETCH_SUCCESS:
      return {
        ...state,
        searchStr: '',
        searching: false,
        searchResult: [],
        bot: action.bot,
      };
    case LIST_FETCH_SUCCESS:
      return {
        ...state,
        searchStr: '',
        searching: false,
        searchResult: [],
        mainData: action.mainData,
      };
    case SEARCH_FETCH_SUCCESS:
      return {
        ...state,
        searching: false,
        searchResult: action.searchResult,
      };
    case SEARCH_NO_RESULT:
      return {
        ...state,
        searchStr: '',
        searching: false,
        searchResult: [],
      };
    default:
      return state;
  }
}

const fecthAdList = async () => {
  // const res = await getData('bot');
  // if (res.result) {
  const data = [
    { _id: 'id', detail: 'detail' },
    { _id: 'id', detail: 'detail' },
    { _id: 'id', detail: 'detail' },
    { _id: 'id', detail: 'detail' },
  ];
  const ads = data.slice(0, 4).reduce((pre, acc) => {
    const { _id, detail } = acc;
    pre.push({
      id: _id,
      image: '../image/image.png',
      title: detail,
      link: 'down',
    });
    return pre;
  }, []);
  return ads;
  // }
  // return false;
};

const fetchCategoryList = async (category) => {
  const res = await fetchBotsByCategory(category);
  if (res.result) {
    const { data } = res;
    // const bots = data.slice(0, 4).reduce((pre, acc) => {
    //   const {
    //     _id, detail, score, comments, projectId,
    //   } = acc;
    //   pre.push({
    //     id: _id,
    //     image: '../image/image.png',
    //     title: projectId,
    //     stars: score,
    //     reviews: comments,
    //     detail,
    //     link: 'down',
    //   });
    //   return pre;
    // }, []);
    return { category, data };
  }
  return false;
};

const searchResultList = async (value) => {
  const res = await fetchSearchResults(value);
  if (res.result) {
    const { data } = res;
    return data;
  }
  return [];
};

const getBotDetails = async (bid) => {
  const res = await fetchBotDetail(bid);
  if (res.result) {
    const { data } = res;
    // const bot = {
    //   id: 'id1234',
    //   image: '../image/img_pibo.png',
    //   title: '제목 대략 10자 이내',
    //   category: '카테고리명',
    //   user: {
    //     img: 'https://react.semantic-ui.com/images/wireframe/square-image.png',
    //     isInstalled: true,
    //     review: {
    //       id: 'user1234',
    //       title: 'Fun, but Unchallenging',
    //       gpa: (4).toFixed(1),
    //       name: 'circulus',
    //       regdate: '2018-07-30',
    //       content:
    //         "First of all, the category NFL doesn't work at all, no matter how hard I try. As it's just a \"category of the week,\" though, it's not a big deal.",
    //     },
    //   },
    //   makerImg: 'https://react.semantic-ui.com/images/wireframe/square-image.png',
    //   maker: '제작자 이름',
    //   gpa: 4.0,
    //   reviews: [
    //     {
    //       id: 'id1234',
    //       title: 'title',
    //       gpa: 4,
    //       name: 'Matthew Harris',
    //       regdate: '2018-07-30',
    //       content:
    //         'Leverage agile frameworks to provide a robust synopsis for high level overviews.',
    //     },
    //     {
    //       id: 'id5678',
    //       title: 'title',
    //       gpa: 4,
    //       name: 'Jake Smith',
    //       regdate: '2018-07-30',
    //       content: 'Bring to the table win-win survival strategies to ensure proactive domination.',
    //     },
    //     {
    //       id: 'id9101',
    //       title: 'title',
    //       gpa: 4,
    //       name: 'Elliot Baker',
    //       regdate: '2018-07-30',
    //       content:
    //         'Capitalise on low hanging fruit to identify a ballpark value added activity to beta test.',
    //     },
    //     {
    //       id: 'id1121',
    //       title: 'title',
    //       gpa: 4,
    //       name: 'Jenny Hess',
    //       regdate: '2018-07-30',
    //       content: 'Jenny requested permission to view your contact details',
    //     },
    //   ],
    //   update:
    //     'A dog is a type of domesticated animal. Known for its loyalty and faithfulness, it can be found as a welcome guest in many households across the world.',
    //   description:
    //     'There are many breeds of dogs. Each breed varies in size and temperament. Owners often select a breed of dog that they find to be compatible with their own lifestyle and desires from a companion.',
    //   requires:
    //     'Three common ways for a prospective owner to acquire a dog is from pet shops, private owners, or shelters.',
    // };
    return data;
  }
  return {};
};

// 봇스토어 메인에 보여지는 데이터 가져오기
// 배너 데이터 리스트
// 카테고리별 봇정보 카드 리스트
export const fetchStoreMainData = async (dispatch) => {
  dispatch(storeLoading());
  const newsItems = await fetchCategoryList('news');
  const entertainItems = await fetchCategoryList('entertain');
  const adItems = await fecthAdList();
  const categoryItems = [newsItems, entertainItems];
  dispatch(fetchStoreMainDataSuccess({ adItems, categoryItems }));
};

// 검색 자동완성 데이터 제목 기준으로 검색
export const fetchAutoCompleteData = value => async (dispatch) => {
  dispatch(storeSearchLoading(value));
  if (value.length < 1) return dispatch(fetchSearchNoResult());
  const results = await searchResultList(value);
  return dispatch(fetchSearchResultSuccess(results));
};

// 봇 선택 시 상세정보 가져오기
export const fetchBotDetails = bid => async (dispatch) => {
  dispatch(storeLoading());
  const bot = await getBotDetails(bid);
  console.log('fetchBotDetails', bot);
  dispatch(fetchStoreBotDetailSuccess(bot));
};
