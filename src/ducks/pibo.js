// import { getData, updateData } from './api';
import {
  fetchPibo, fetchPiboInfo, updatePiboInfo, getGeo,
} from '../utils/api';

export const LOADING = 'pibo/LOADING';
export const SUCCESS = 'pibo/SUCCESS';
export const ERROR = 'pibo/ERROR';
export const SAVE_SUCCESS = 'pibo/SAVE_SUCCESS';
export const FETCH_SUCCESS = 'pibo/FETCH_SUCCESS';

export function piboLoading() {
  return {
    type: LOADING,
  };
}

export function getPiboDataSuccess(pibo) {
  return {
    type: SUCCESS,
    pibo,
  };
}

export function piboError(errorMsg) {
  return {
    type: ERROR,
    errorMsg,
  };
}

export function fetchPiboDataSuccess(data) {
  return {
    type: FETCH_SUCCESS,
    data,
  };
}

export function savePiboCogSuccess() {
  return {
    type: SAVE_SUCCESS,
  };
}

// 초기 상태
const initialState = {
  loading: false,
  pibo: {},
  data: {},
  errorMsg: '',
  isSaved: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true,
        isSaved: false,
        pibo: {},
        data: {},
      };
    case SUCCESS:
      return {
        ...state,
        loading: false,
        pibo: action.pibo,
      };
    case ERROR:
      return {
        ...state,
        loading: false,
        isSaved: false,
        errorMsg: action.errorMsg,
        pibo: {},
      };
    case SAVE_SUCCESS:
      return {
        ...state,
        loading: false,
        isSaved: true,
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
      };
    default:
      return state;
  }
}

// 파이보 데이터 모두 가져오기
export const getPiboData = pid => async (dispatch) => {
  dispatch(piboLoading());
  // const dd = await api.fetchPeople();
  const res = await fetchPibo(pid);
  if (res.result) {
    const { name, tempUnit, bot } = res.data;
    const data = {
      name,
      locale: res.locale,
      geo: res.geo,
      volume: res.volume,
      servo: res.servo,
      wifi: 'Circulus',
      battery: 100,
      temper: 27,
      tempUnit,
      bot,
    };
    dispatch(getPiboDataSuccess(data));
  }
};

// 파이보 설정 저장
export const savePiboCog = (pid, data) => async (dispatch) => {
  dispatch(piboLoading());
  const res = await updatePiboInfo(pid, data);
  if (res.result) {
    dispatch(savePiboCogSuccess());
  }
};

// 파이보 데이터 항목 별 가져오기(설정 값과 설정 할 수 있는 리스트 값을 가져와야함)
export const fetchCogList = (pid, item) => async (dispatch) => {
  dispatch(piboLoading());
  try {
    let res;
    switch (item) {
      // case 'wifi':
      //   res = await getApList();
      //   break;
      case 'geo':
        res = await getGeo(pid);
        break;
      default:
        res = await fetchPiboInfo(pid, item);
        break;
    }
    if (res.result) {
      const resData = res.data;
      return dispatch(fetchPiboDataSuccess(resData));
    }
    return dispatch(piboError(res.error));
  } catch (error) {
    return dispatch(piboError(error));
  }
};
