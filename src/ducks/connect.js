import axios from 'axios';
import {
  connectToRobot, getConnectedSSID, connectToAp, ROBOT_URL,
} from '../utils/api';

export const LOADING = 'connect/LOADING';
export const FETCH_APLIST_SUCCESS = 'connect/FETCH_APLIST_SUCCESS';
export const FETCH_APLIST_FAILED = 'connect/FETCH_APILIST_FAILED';
export const CONNECT_ROBOT_SUCCESS = 'connect/CONNECT_ROBOT_SUCCESS';
export const FETCH_SSID_SUCCESS = 'connect/FETCH_SSID_SUCCESS';
export const CONNECT_AP_SUCCESS = 'connect/CONNECT_AP_SUCCESS';
export const ERROR = 'connect/ERROR';

export function connectToRobotSuccess(robotId) {
  return {
    type: CONNECT_ROBOT_SUCCESS,
    robotId,
  };
}
export function connectToApSuccess() {
  return {
    type: CONNECT_AP_SUCCESS,
  };
}
export function fetchApListSuccess(data) {
  return {
    type: FETCH_APLIST_SUCCESS,
    data,
  };
}
export function fetchApListFailed() {
  return {
    type: FETCH_APLIST_FAILED,
  };
}
export function connectLoading() {
  return {
    type: LOADING,
  };
}

export function connectError(errorMsg) {
  return {
    type: ERROR,
    errorMsg,
  };
}

export function fetchSsidSuccess(ssid) {
  return {
    type: FETCH_SSID_SUCCESS,
    ssid,
  };
}

const initialState = {
  loading: false,
  errorMsg: '',
  ssid: '',
  aplist: [],
  robotId: '',
  stepComplete: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        error: '',
        loading: true,
      };
    case ERROR:
      return {
        ...state,
        loading: false,
        errorMsg: action.errorMsg,
      };
    case FETCH_SSID_SUCCESS:
      return {
        ...state,
        loading: false,
        errorMsg: '',
        ssid: action.ssid,
      };
    case FETCH_APLIST_FAILED:
      return {
        ...state,
        loading: false,
        errorMsg: 'failed load aplist',
      };
    case FETCH_APLIST_SUCCESS:
      return {
        ...state,
        robotId: action.data.robotId,
        aplist: action.data.aplist,
        loading: false,
        errorMsg: '',
      };
    case CONNECT_ROBOT_SUCCESS:
      return {
        ...state,
        robotId: action.robotId,
        loading: false,
        errorMsg: '',
      };
    case CONNECT_AP_SUCCESS:
      return {
        ...state,
        loading: false,
        errorMsg: '',
        stepComplete: true,
      };
    default:
      return state;
  }
}

export const fetchApList = () => async (dispatch) => {
  dispatch(connectLoading());
  const { http } = window;
  if (http) {
    http.get(
      `${ROBOT_URL}/wifi`,
      {},
      {},
      (res) => {
        const { data } = res;
        const { robotId, list } = JSON.parse(data);
        alert(res);
        if (robotId && list) {
          return dispatch(fetchApListSuccess({ robotId, aplist: list }));
        }
        return dispatch(fetchApListFailed());
      },
      () => dispatch(fetchApListFailed()),
    );
  }

  try {
    axios
      .get(`${ROBOT_URL}/wifi`)
      .then((res) => {
        const result = JSON.stringify(res);
        const { data } = JSON.parse(result);
        const { robotId, list } = data;
        if (robotId && list) {
          return dispatch(fetchApListSuccess({ robotId, aplist: list }));
        }
        return dispatch(fetchApListFailed());
      })
      .catch(error => dispatch(fetchApListFailed()));
    // const url = `${ROBOT_URL}/wifi`;
    // const res = await fetch(url, { mode: 'cors' });
    // const result = res.json();
    // return result;
  } catch (error) {
    alert(error);
    return dispatch(fetchApListFailed());
  }
};

export const fetchConnectResult = () => async (dispatch) => {
  // 로봇을 찾는 통신 부분
  // 로봇을 찾고 난 후 받는 데이터(로봇의 시리얼 등...)
  // 데이터를 기반으로 새로운 로봇 정보를 db에 추가한다.
  dispatch(connectLoading());
  const robotId = localStorage.getItem('robotId');
  if (robotId) {
    return dispatch(connectToRobotSuccess(robotId));
  }
  return dispatch(connectToRobotSuccess(''));
};

export const fetchCurrentSSID = () => async (dispatch) => {
  dispatch(connectLoading());
  const ssid = await getConnectedSSID();
  if (ssid.search('pibo_') > -1) {
    return dispatch(fetchSsidSuccess(ssid));
  }
  // return dispatch(connectError('파이보 Wi-Fi에 연결되지 않았습니다.'));
  return dispatch(connectError(ssid));
};

export const connectAp = ap => async (dispatch) => {
  dispatch(connectLoading());
  const res = await connectToAp(ap);
  dispatch(connectToApSuccess());
};
