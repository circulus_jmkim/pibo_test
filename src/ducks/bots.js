import {
  fetchMyBots, updateBotUse, removeBot, fetchSearchResults,
} from '../utils/api';

export const LOADING = 'bots/LOADING';
export const ERROR = 'bots/ERROR';
export const FETCH_SUCCESS = 'bots/FETCH_SUCCESS';
export const DELETE_SUCCESS = 'bots/DELETE_SUCCESS';
export const UPDATE_SUCCESS = 'bots/UPDATE_SUCCESS';
export const SAVE_SUCCESS = 'bots/SAVE_SUCCESS';
export const SEARCH_LOADING = 'bots/SEARCH_LOADING';
export const SEARCH_NO_RESULT = 'bots/SEARCH_NO_RESULT';
export const SEARCH_FETCH_SUCCESS = 'bots/SEARCH_FETCH_SUCCESS';

export function botsLoading() {
  return {
    type: LOADING,
  };
}

export function botsError(errorMsg) {
  return {
    type: ERROR,
    errorMsg,
  };
}

export function getBotsSuccess(bots) {
  return {
    type: FETCH_SUCCESS,
    bots,
  };
}
export function updateBotsSuccess(bots) {
  return {
    type: UPDATE_SUCCESS,
    bots,
  };
}
export function deleteBotSuccess() {
  return {
    type: DELETE_SUCCESS,
  };
}

export function saveBotSuccess() {
  return {
    type: SAVE_SUCCESS,
  };
}

export function searchLoading() {
  return {
    type: SEARCH_LOADING,
  };
}

export function fetchSearchNoResult() {
  return {
    type: SEARCH_NO_RESULT,
  };
}

export function fetchSearchResultSuccess(searchResult) {
  return {
    type: SEARCH_FETCH_SUCCESS,
    searchResult,
  };
}

const initialState = {
  loading: false,
  errorMsg: '',
  bots: [],
  searching: false,
  searchStr: '',
  searchResult: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true,
        bots: [],
      };
    case SEARCH_LOADING:
      return {
        ...state,
        searchStr: action.searchStr,
        searching: true,
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        bots: action.bots,
      };
    case UPDATE_SUCCESS:
      return {
        ...state,
        loading: false,
        bots: action.bots,
      };
    case ERROR:
      return {
        ...state,
        loading: false,
        errorMsg: action.errorMsg,
      };
    case SEARCH_FETCH_SUCCESS:
      return {
        ...state,
        searching: false,
        searchResult: action.searchResult,
      };
    case SEARCH_NO_RESULT:
      return {
        ...state,
        searchStr: '',
        searching: false,
        searchResult: [],
      };
    default:
      return state;
  }
}

// 즐겨찾는 bot list 가져오기
export const getFavBotsList = () => (dispatch) => {
  dispatch(botsLoading());
  const data = [
    { id: 'id1234', title: 'Fahey', image: '../image/image.png' },
    { id: 'id5678', title: 'Ziemann ', image: '../image/image.png' },
    { id: 'id9101112', title: 'Zemlak', image: '../image/image.png' },
    { id: 'id13141516', title: 'Gerlach', image: '../image/image.png' },
  ];
  dispatch(getBotsSuccess(data));
};

// 봇리스트 가져오는 통신 부분
const getBotsList = async (pid) => {
  const res = await fetchMyBots(pid);
  if (res.result) {
    const { data } = res;
    return data;
  }
  return false;
};

// pid: 로봇아이디
// 봇리스트 로드하는 부분
export const fetchBotsList = pid => async (dispatch) => {
  dispatch(botsLoading());
  try {
    const bots = await getBotsList(pid);
    dispatch(getBotsSuccess(bots));
  } catch (error) {
    dispatch(botsError(error));
  }
};

// pid: 로봇아이디
// updateList: [{bid, state}] 봇아이디, 변경스테이트
// 로봇의 봇 상태 변경
export const updateBotsList = (pid, bid, use) => async (dispatch) => {
  dispatch(botsLoading());
  const res = await updateBotUse(pid, bid, use);
  if (res) {
    const updateList = await getBotsList(pid);
    dispatch(updateBotsSuccess(updateList));
  }
};

// pid: 로봇아이디
// bid: 봇아이디
// 로봇에 설치된 봇 삭제
export const deleteBot = (pid, bid) => async (dispatch) => {
  dispatch(botsLoading());
  await removeBot(pid, bid);
  dispatch(deleteBotSuccess());
};

// 검색 자동완성 데이터 제목 기준으로 검색
export const fetchAutoCompleteData = (pid, value) => async (dispatch) => {
  dispatch(searchLoading(value));
  if (value.length < 1) return dispatch(fetchSearchNoResult());
  const res = await fetchSearchResults(value, pid);
  if (res.result) {
    return dispatch(fetchSearchResultSuccess(res.data));
  }
  return dispatch(fetchSearchNoResult());
};
