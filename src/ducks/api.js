import axios from 'axios-jsonp-pro';

const API_URL = 'http://dev.circul.us/jsonp/db_';

export const getData = async (collection, params = {}) => {
  const url = `${API_URL}find`;
  const config = {
    method: 'jsonp',
    url,
    params: {
      $_name: collection,
      ...params,
    },
  };
  console.log('select', config);
  const res = await axios(config);
  console.log('select result', res);
  return res;
};

export const updateData = async (collection, params = {}, values = {}) => {
  const url = `${API_URL}updateOne`;
  const config = {
    method: 'jsonp',
    url,
    params: {
      $_name: collection,
      $_param: params,
      $_value: values,
    },
  };
  console.log('update', config);
  const res = await axios(config);
  return res;
};

export const insertData = async (collection, params = {}, values = {}) => {
  const url = `${API_URL}upsertOne`;
  const config = {
    method: 'jsonp',
    url,
    params: {
      $_name: collection,
      $_param: params,
      $_value: values,
    },
  };
  const res = await axios(config);
  return res;
};
