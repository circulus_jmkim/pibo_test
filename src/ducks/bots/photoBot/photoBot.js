import { createAction, handleActions } from 'redux-actions';
import { Map, List, fromJS } from 'immutable';
import moment from 'moment';

import { getPhotosApi, deletePhotosApi } from './photoBotApi';

const GET_PHOTOS_GET_INI_DATA_STATUS = 'photobot/GET_PHOTOS_GET_INI_DATA_STATUS';
const GET_PHOTOS_PENDING = 'photobot/GET_PHOTOS_PENDING';
const GET_PHOTOS_SUCCESS = 'photobot/GET_PHOTOS_SUCCESS';
const GET_PHOTOS_FAILURE = 'photobot/GET_PHOTOS_FAILURE';
const DELETE_PHOTOS_PENDING = 'photobot/DELETE_PHOTOS_PENDING';
const DELETE_PHOTOS_SUCCESS = 'photobot/DELETE_PHOTOS_SUCCESS';
const DELETE_PHOTOS_FAILURE = 'photobot/DELETE_PHOTOS_FAILURE';
const TOGGLE_EDIT = 'photobot/TOGGLE_EDIT';
const CHECK_PHOTO = 'photobot/CHECK_PHOTO';

export const toggleEdit = createAction(TOGGLE_EDIT);
export const getInitData = createAction(GET_PHOTOS_GET_INI_DATA_STATUS);

export const checkPhoto = (parentIndex, childIndex) => (dispatch) => {
  dispatch({
    type: CHECK_PHOTO,
    payload: { parentIndex, childIndex },
  });
};

export const getPhotos = (skip, limit) => async (dispatch) => {
  dispatch({ type: GET_PHOTOS_PENDING });
  return getPhotosApi(skip, limit).then(
    (response) => {
      dispatch({
        type: GET_PHOTOS_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: GET_PHOTOS_FAILURE,
      payload: error,
    });
  });
};

export const deletePhotos = () => async (dispatch, getState) => {
  dispatch({ type: DELETE_PHOTOS_PENDING });
  const { photoData } = getState().photoBot.toJS();
  const deleteList = [];
  const deleteIndexList = [];
  photoData.forEach((photoList, i) => {
    photoList.forEach((photo, l) => {
      if (photo.checked) {
        const { _id: id } = photo;
        deleteList.push(id);
        deleteIndexList.push({ parentIndex: i, index: l });
      }
    });
  });

  return deletePhotosApi(deleteList).then(
    (response) => {
      dispatch({
        type: DELETE_PHOTOS_SUCCESS,
        payload: { response, deleteIndexList },
      });
    },
  ).catch((error) => {
    dispatch({
      type: DELETE_PHOTOS_FAILURE,
      payload: error,
    });
  });
};

const initialState = Map({
  getInitDataStatus: false,
  editing: false,
  pending: false,
  error: false,
  photoData: List(),
  photoDataLength: 0,
});

export default handleActions({
  [GET_PHOTOS_PENDING]: state => state.set('pending', true).set('error', false),
  [GET_PHOTOS_SUCCESS]: (state, action) => {
    let { data: { photoData } } = action.payload;
    const oldPhotoData = state.get('photoData').toJS();
    let prevPhotoDate = null;
    let newPhotoData = [[]];
    let photoDataIndex = 0;
    if (oldPhotoData.length > 0) {
      const lastArray = oldPhotoData[oldPhotoData.length - 1];
      prevPhotoDate = lastArray[lastArray.length - 1].firstTime;
      newPhotoData = oldPhotoData;
      photoDataIndex = oldPhotoData.length - 1;
    }

    photoData = photoData.map((photo) => {
      const newPhoto = photo;
      newPhoto.firstTime = moment(photo.firstTime);
      newPhoto.checked = false;
      return newPhoto;
    });

    photoData.forEach((element, index) => {
      const photo = element;
      if (index > 0) {
        prevPhotoDate = photoData[index - 1].firstTime;
      }

      if (prevPhotoDate && (prevPhotoDate.date() !== photo.firstTime.date())) {
        photoDataIndex += 1;
        newPhotoData.push([]);
        if (prevPhotoDate.year() !== photo.firstTime.year()) {
          photo.newYear = true;
        }
      }
      newPhotoData[photoDataIndex].push(element);
    });

    newPhotoData = fromJS(newPhotoData);
    return state.set('pending', false).set('photoData', newPhotoData).update('photoDataLength', value => value + photoData.length);
  },
  [GET_PHOTOS_FAILURE]: state => state.set('pending', false).set('error', true),
  [DELETE_PHOTOS_PENDING]: state => state.set('pending', true).set('error', false),
  [DELETE_PHOTOS_SUCCESS]:
    (state,
      { payload: { response: { data: { result: { n: deleteLength } } }, deleteIndexList } }) => {
      if (deleteLength === deleteIndexList.length) {
        return state.set('pending', false).update('photoData', (photoData) => {
          const photoDataList = photoData.toJS();
          deleteIndexList.reverse().forEach(({ parentIndex, index }) => {
            photoDataList[parentIndex].splice(index, 1);
          });
          return fromJS(photoDataList);
        });
      }
      return state.set('pending', false).set('error', true);
    },
  [DELETE_PHOTOS_FAILURE]: state => state.set('pending', false).set('error', true),
  [TOGGLE_EDIT]: state => state.update('editing', value => !value),
  [GET_PHOTOS_GET_INI_DATA_STATUS]: state => state.update('getInitDataStatus', value => !value),
  [CHECK_PHOTO]: (state, { payload }) => state.updateIn(['photoData', payload.parentIndex, payload.childIndex, 'checked'], value => !value),
}, initialState);
