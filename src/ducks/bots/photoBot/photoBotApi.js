import axios from 'axios';

import { serverUrl } from '../../../circulusConfig';

export const getPhotosApi = (skip, limit) => {
  const options = {
    method: 'GET',
    params: { skip, limit },
    url: `${serverUrl}/api/photoBot/getPhotoData`,
  };
  return axios(options);
};

export const deletePhotosApi = (idArray) => {
  const options = {
    method: 'DELETE',
    data: { deleteList: idArray },
    url: `${serverUrl}/api/photoBot/deletePhotos`,
  };
  return axios(options);
};

// export const getMonthDataApi = (startDate, endDate) => {
//   const options = {
//     method: 'GET',
//     params: { startDate, endDate },
//     url: `${serverUrl}/api/calendarBot/getMonthData`,
//   };
//   return axios(options);
// };

// export const deleteItemsApi = (idArray) => {
//   const options = {
//     method: 'DELETE',
//     data: { deleteList: idArray },
//     url: `${serverUrl}/api/calendarBot/deleteCalendarItems`,
//   };
//   return axios(options);
// };
