import { createAction, handleActions } from 'redux-actions';
import { Map, List, fromJS } from 'immutable';
import moment from 'moment';

import {
  getMonthDataApi,
  setCalendarApi,
  deleteItemsApi,
  getRecordsApi,
} from './calendarBotApi';

const GET_DEFAULT_DATA_SUCCESS = 'calendar/GET_DEFAULT_DATA';

const SET_DATE = 'calendar/SET_DATE';
const CHANGE_MONTH = 'calendar/CHANGE_MONTH';
const CHANGE_MENU = 'calendar/CHANGE_MENU';
const CHECK_ITEM = 'calendar/CHECK_ITEM';

const GET_MONTH_DATA_SUCCESS = 'calendar/GET_MONTH_DATA_SUCCESS';
const GET_MONTH_DATA_FAILURE = 'calendar/GET_MONTH_DATA_FAILURE';
const GET_MONTH_DATA_PENDING = 'calendar/GET_MONTH_DATA_PENDING';

const SET_SCHEDULE_PENDING = 'calendar/SET_SCHEDULE_PENDING';
const SET_SCHEDULE_SUCCESS = 'calendar/SET_SCHEDULE_SUCCESS';
const SET_SCHEDULE_FAILURE = 'calendar/SET_SCHEDULE_FAILURE';

const DELETE_ITEMS_PENDING = 'calendar/DELETE_ITEMS_PENDING';
const DELETE_ITEMS_SUCCESS = 'calendar/DELETE_ITEMS_SUCCESS';
const DELETE_ITEMS_FAILURE = 'calendar/DELETE_ITEMS_FAILURE';

const GET_RECORD_DATA_PENDING = 'calendar/GET_RECORD_DATA_PENDING';
const GET_RECORD_DATA_SUCCESS = 'calendar/GET_RECORD_DATA_SUCCESS';
const GET_RECORD_DATA_FAILURE = 'calendar/GET_RECORD_DATA_FAILURE';

export const setDate = createAction(SET_DATE, day => ({ day }));
export const changeMenu = createAction(CHANGE_MENU, menu => ({ menu }));
export const checkItem = createAction(CHECK_ITEM, index => ({ index }));

export const setCalendar = schedule => (dispatch) => {
  dispatch({ type: SET_SCHEDULE_PENDING });
  return setCalendarApi(schedule).then(
    (response) => {
      dispatch({
        type: SET_SCHEDULE_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: SET_SCHEDULE_FAILURE,
      payload: error,
    });
  });
};

export const getRecords = (skip, limit) => async (dispatch) => {
  dispatch({ type: GET_RECORD_DATA_PENDING });
  return getRecordsApi(skip, limit).then(
    (response) => {
      dispatch({
        type: GET_RECORD_DATA_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: GET_RECORD_DATA_FAILURE,
      payload: error,
    });
  });
};

export const getMonthData = () => async (dispatch, getState) => {
  const date = getState().calendarBot.get('date').clone();
  const startDate = date.startOf('month').format();
  const endDate = date.endOf('month').format();
  dispatch({ type: GET_MONTH_DATA_PENDING });
  return getMonthDataApi(startDate, endDate).then(
    (response) => {
      dispatch({
        type: GET_MONTH_DATA_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: GET_MONTH_DATA_FAILURE,
      payload: error,
    });
  });
};

export const getDefaultData = () => async (dispatch) => {
  try {
    await dispatch(getMonthData());
    dispatch({ type: GET_DEFAULT_DATA_SUCCESS });
  } catch (error) {
    console.log(error);
  }
};

export const changeMonth = nav => (dispatch) => {
  dispatch({ type: CHANGE_MONTH, payload: { nav } });
  dispatch(getDefaultData());
};

export const deleteItems = () => async (dispatch, getState) => {
  dispatch({ type: DELETE_ITEMS_PENDING });
  const { calendarBot } = getState();
  const monthData = calendarBot.get('monthData');
  const deleteList = [];
  Object.entries(monthData).forEach(([key, val]) => {
    val.data.forEach((item, index) => {
      if (item.checked) {
        const { _id: id } = item;
        deleteList.push({ id, day: key, index });
      }
    });
  });

  return deleteItemsApi(deleteList).then(
    (response) => {
      dispatch({
        type: DELETE_ITEMS_SUCCESS,
        payload: { response, deleteList },
      });
    },
  ).catch((error) => {
    dispatch({
      type: DELETE_ITEMS_FAILURE,
      payload: error,
    });
  });
};

const initialState = Map({
  getDefaultDataStatus: false,
  date: moment(),
  apiMessage: '',
  pending: false,
  error: false,
  monthData: Map({}),
  selectedMenu: 'all',
  records: List(),
});

export default handleActions({
  [GET_RECORD_DATA_PENDING]: state => state.set('pending', true).set('error', false),
  [GET_RECORD_DATA_SUCCESS]: (state, { payload: { data: { records } } }) => state.set('pending', false)
    .set('records', List(records)),
  [GET_RECORD_DATA_FAILURE]: state => state.set('pending', false).set('error', true),
  [CHECK_ITEM]: (state, { payload: { index } }) => {
    let date = state.get('date');
    date = date.get('date');
    return state.updateIn(['monthData', date, 'data', index, 'checked'], (value) => {
      if (typeof value === 'undefined') {
        return true;
      }
      return !value;
    });
  },
  [CHANGE_MENU]: (state, { payload: { menu } }) => state.set('selectedMenu', menu),
  [CHANGE_MONTH]: (state, { payload: { nav } }) => state.update('date', (value) => {
    const date = value.clone();
    const month = date.get('month');
    if (nav === 'next') {
      date.month(month + 1);
    } else {
      date.month(month - 1);
    }
    return date;
  }),
  [SET_DATE]: (state, { payload: { day } }) => state.set('date', day),
  [GET_DEFAULT_DATA_SUCCESS]: state => state.set('getDefaultDataStatus', true),
  [SET_SCHEDULE_PENDING]: state => state.set('pending', true).set('error', false),
  [SET_SCHEDULE_SUCCESS]: (state, action) => {
    const { payload: { data: { success }, statusText } } = action;
    if (statusText === 'OK' && success) return state.set('pending', false).set('apiMessage', statusText);
    return state.set('pending', false).set('error', true).set('apiMessage', statusText);
  },
  [SET_SCHEDULE_FAILURE]: state => state.set('pending', false).set('error', true),
  [GET_MONTH_DATA_PENDING]: state => state.set('pending', true).set('error', false),
  [GET_MONTH_DATA_SUCCESS]: (state, { payload: { data: { monthData, repeatData } } }) => {
    if (typeof monthData[0] === 'object' && Array.isArray(repeatData)) {
      const monthDataObject = monthData[0];
      const repeatDataList = repeatData;
      const date = state.get('date');
      const startDate = date.startOf('month').date();
      const endDate = date.endOf('month').date();
      const repeatList = {
        dailyList: [], weeklyList: [], monthlyList: [], yearlyList: [],
      };

      for (let i = 0; i < repeatDataList.length; i += 1) {
        const { repeat } = repeatDataList[i];
        if (repeat && repeat.length > 0) {
          const listName = `${repeat}List`;
          repeatList[listName].push(repeatDataList[i]);
        }
      }

      for (let i = startDate; i <= endDate; i += 1) {
        if (typeof monthDataObject[i] === 'undefined') {
          monthDataObject[i] = { data: [], scheduleCount: 0, recordCount: 0 };
        }

        monthDataObject[i].data = monthDataObject[i].data.concat(repeatList.dailyList);
        monthDataObject[i].scheduleCount += repeatList.dailyList.length;

        repeatList.weeklyList.forEach((data) => {
          const { repeatValue: { week } } = data;

          if (parseInt(week, 10) && (date.set('date', i).day() === parseInt(week, 10))) {
            monthDataObject[i].data.push(data);
            monthDataObject[i].scheduleCount += 1;
          }
        });

        repeatList.monthlyList.forEach((data) => {
          const { repeatValue: { day } } = data;
          if (parseInt(day, 10) && i === parseInt(day, 10)) {
            monthDataObject[i].data.push(data);
            monthDataObject[i].scheduleCount += 1;
          }
        });

        repeatList.yearlyList.forEach((data) => {
          const { repeatValue: { month, day } } = data;
          const monthInt = parseInt(month, 10);
          const dayInt = parseInt(day, 10);
          if (monthInt && dayInt && (date.month() === monthInt) && (i === dayInt)) {
            monthDataObject[i].data.push(data);
            monthDataObject[i].scheduleCount += 1;
          }
        });
      }
      return state.set('pending', false).set('error', false).set('monthData', monthDataObject);
    }
    return state.set('pending', false).set('error', false);
  },
  [GET_MONTH_DATA_FAILURE]: state => state.set('pending', false).set('error', true),
  [DELETE_ITEMS_PENDING]: state => state.set('pending', true).set('error', false),
  [DELETE_ITEMS_SUCCESS]:
    (state, { payload: { response: { data: { result: { n: deleteLength } } }, deleteList } }) => {
      if (deleteLength === deleteList.length) {
        return state.set('pending', false).update('monthData', (monthData) => {
          deleteList.reverse().forEach(({ day, index }) => {
            monthData[day].data.splice(index, 1);
          });
          return monthData;
        });
      }
      return state.set('pending', false).set('error', true);
    },
  [DELETE_ITEMS_FAILURE]: state => state.set('pending', false).set('error', true),
}, initialState);
