import axios from 'axios';
import qs from 'qs';

import { serverUrl } from '../../../circulusConfig';

export const setCalendarApi = (data) => {
  const options = {
    method: 'POST',
    data: qs.stringify(data),
    url: `${serverUrl}/api/calendarBot/setCalendar`,
  };
  return axios(options);
};

export const getMonthDataApi = (startDate, endDate) => {
  const options = {
    method: 'GET',
    params: { startDate, endDate },
    url: `${serverUrl}/api/calendarBot/getMonthData`,
  };
  return axios(options);
};

export const deleteItemsApi = (idArray) => {
  const options = {
    method: 'DELETE',
    data: { deleteList: idArray },
    url: `${serverUrl}/api/calendarBot/deleteCalendarItems`,
  };
  return axios(options);
};

export const getRecordsApi = (skip, limit) => {
  const options = {
    method: 'GET',
    params: { skip, limit },
    url: `${serverUrl}/api/calendarBot/getRecords`,
  };
  return axios(options);
};