import axios from 'axios';
import { serverUrl } from '../../../circulusConfig';

export const deleteHistoryItemApi = ({ _id }) => {
  const options = {
    method: 'DELETE',
    data: { _id },
    url: `${serverUrl}/api/historyBot/deleteHistoryItem`,
  };
  return axios(options);
};

export const getHistoryApi = (skip, limit) => {
  const options = {
    method: 'GET',
    params: { skip, limit },
    url: `${serverUrl}/api/historyBot/getDialogData`,
  };
  return axios(options);
};

export const sendMessageApi = ({
  robotId, userId, emotion, message,
}) => (
  new Promise((resolve) => {
    const date = new Date();
    resolve({
      status: 200,
      data: {
        userId,
        robotId,
        emotion,
        question: `나의 메세지${message}`,
        answer: `로봇 답변${date}`,
        createdAt: date,
      },
    });
  })
);

// export const setHistoryMessageApi = (message) => {
//   const options = {
//     method: 'POST',
//     data: qs.stringify(message),
//     url: `${serverUrl}/api/historyBot/setHistoryMessageApi`,
//   };
//   return axios(options);
// };
