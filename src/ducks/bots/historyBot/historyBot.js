import { handleActions } from 'redux-actions';
import { Map, List, fromJS } from 'immutable';
import moment from 'moment';

import { getHistoryApi, sendMessageApi, deleteHistoryItemApi } from './historyBotApi';

const GET_HISTORY_PENDING = 'historyBot/GET_HISTORY_PENDING';
const GET_HISTORY_SUCCESS = 'historyBot/GET_HISTORY_SUCCESS';
const GET_HISTORY_FAILURE = 'historyBot/GET_HISTORY_FAILURE';
const SEND_HISTORY_PENDING = 'historyBot/SEND_HISTORY_PENDING';
const SEND_HISTORY_SUCCESS = 'historyBot/SEND_HISTORY_SUCCESS';
const SEND_HISTORY_FAILURE = 'historyBot/SEND_HISTORY_FAILURE';
const DELETE_HISTORY_PENDING = 'historyBot/DELETE_HISTORY_PENDING';
const DELETE_HISTORY_SUCCESS = 'historyBot/DELETE_HISTORY_SUCCESS';
const DELETE_HISTORY_FAILURE = 'historyBot/DELETE_HISTORY_FAILURE';

export const deleteHistoryItem = index => async (dispatch, getState) => {
  dispatch({ type: DELETE_HISTORY_PENDING });
  const { historyData } = getState().historyBot.toJS();
  if (Number.isInteger(index)) {
    return deleteHistoryItemApi(historyData[index]).then(
      (response) => {
        dispatch({
          type: DELETE_HISTORY_SUCCESS,
          payload: { response, index },
        });
      },
    ).catch((error) => {
      dispatch({
        type: DELETE_HISTORY_FAILURE,
        payload: error,
      });
    });
  }
  return dispatch({
    type: DELETE_HISTORY_FAILURE,
  });
};

export const getHistory = (skip, limit) => async (dispatch) => {
  dispatch({ type: GET_HISTORY_PENDING });
  return getHistoryApi(skip, limit).then(
    (response) => {
      dispatch({
        type: GET_HISTORY_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: GET_HISTORY_FAILURE,
      payload: error,
    });
  });
};

export const sendMessage = message => async (dispatch) => {
  dispatch({ type: SEND_HISTORY_PENDING });
  return sendMessageApi({
    robotId: 'robotid test', userId: 'userId test', emotion: 'sad', message,
  }).then(
    (response) => {
      dispatch({
        type: SEND_HISTORY_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: SEND_HISTORY_FAILURE,
      payload: error,
    });
  });
};

const initialState = Map({
  pending: false,
  error: false,
  historyData: List(),
  sendHistoryPending: false,
  sendHistoryError: false,
});

export default handleActions({
  [GET_HISTORY_PENDING]: state => state.set('pending', true).set('error', false),
  [GET_HISTORY_SUCCESS]: (state, { payload: { data: { historyData } } }) => {
    const oldHistoryData = state.get('historyData').toJS();
    let prevHistoryDate = null;

    if (oldHistoryData.length > 0) {
      prevHistoryDate = oldHistoryData[0].createdAt;
    }

    let newHistoryData = historyData;
    if (Array.isArray(newHistoryData)) {
      newHistoryData.reverse().forEach((element, index) => {
        const history = element;
        history.createdAt = moment(history.createdAt);

        if (index > 0) {
          prevHistoryDate = newHistoryData[index - 1].createdAt;
        }

        if (prevHistoryDate && (prevHistoryDate.date() !== history.createdAt.date())) {
          history.dayChange = true;
        } else {
          history.dayChange = false;
        }
      });
      newHistoryData = fromJS(newHistoryData);
      return state.set('pending', false).update('historyData', valueList => newHistoryData.concat(valueList));
    }
    return state.set('pending', false);
  },
  [GET_HISTORY_FAILURE]: state => state.set('pending', false).set('error', true),
  [SEND_HISTORY_PENDING]: state => state.set('sendHistoryPending', true).set('sendHistoryError', false),
  [SEND_HISTORY_SUCCESS]: (state, { payload: { data } }) => {
    const newData = data;
    newData.createdAt = moment(newData.createdAt);
    const oldHistoryData = state.get('historyData').toJS();
    let prevHistoryData = null;
    if (oldHistoryData.length > 0) {
      prevHistoryData = oldHistoryData[oldHistoryData.length - 1].createdAt;
    }

    if (prevHistoryData && (prevHistoryData.date() !== newData.createdAt.date())) {
      newData.dayChange = true;
    } else {
      newData.dayChange = false;
    }
    return state.set('sendHistoryPending', false).update('historyData', valueList => valueList.concat(newData));
  },
  [SEND_HISTORY_FAILURE]: state => state.set('sendHistoryPending', false).set('sendHistoryError', true),
  [DELETE_HISTORY_PENDING]: state => state.set('pending', true).set('error', false),
  [DELETE_HISTORY_SUCCESS]: (state, { payload: { response, index } }) => {
    if (response) {
      return state.set('pending', false).update('historyData', historyData => historyData.splice(index, 1));
    }
    return state.set('pending', false).set('error', true);
  },
  [DELETE_HISTORY_FAILURE]: state => state.set('pending', false).set('error', true),
}, initialState);
