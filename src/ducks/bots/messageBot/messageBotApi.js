import axios from 'axios';
import { serverUrl } from '../../../circulusConfig';

export const deleteMessageItemApi = ({ _id }) => {
  const options = {
    method: 'DELETE',
    data: { _id },
    url: `${serverUrl}/api/messageBot/deleteMessageItem`,
  };
  return axios(options);
};

export const getMessagesApi = (skip, limit) => {
  const options = {
    method: 'GET',
    params: { skip, limit },
    url: `${serverUrl}/api/messageBot/getMessagesData`,
  };
  return axios(options);
};


export const sendMessageApi = ({
  robotId, userId, message,
}) => (
  new Promise((resolve) => {
    const date = new Date();
    const status = ['pending', 'succese', 'reacted'];

    resolve({
      status: 200,
      data: {
        userId,
        robotId,
        message,
        status: status[Math.floor((Math.random() * 3))],
        firstTime: date,
      },
    });
  })
);
