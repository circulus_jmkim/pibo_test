import { handleActions } from 'redux-actions';
import { Map, List, fromJS } from 'immutable';
import moment from 'moment';

import { getMessagesApi, sendMessageApi, deleteMessageItemApi } from './messageBotApi';

const GET_MESSAGE_PENDING = 'messageBot/GET_MESSAGE_PENDING';
const GET_MESSAGE_SUCCESS = 'messageBot/GET_MESSAGE_SUCCESS';
const GET_MESSAGE_FAILURE = 'messageBot/GET_MESSAGE_FAILURE';
const SEND_MESSAGE_PENDING = 'messageBot/SEND_MESSAGE_PENDING';
const SEND_MESSAGE_SUCCESS = 'messageBot/SEND_MESSAGE_SUCCESS';
const SEND_MESSAGE_FAILURE = 'messageBot/SEND_MESSAGE_FAILURE';
const DELETE_MESSAGE_PENDING = 'messageBot/DELETE_MESSAGE_PENDING';
const DELETE_MESSAGE_SUCCESS = 'messageBot/DELETE_MESSAGE_SUCCESS';
const DELETE_MESSAGE_FAILURE = 'messageBot/DELETE_MESSAGE_FAILURE';

export const deleteMessageItem = index => async (dispatch, getState) => {
  dispatch({ type: DELETE_MESSAGE_PENDING });
  const { messageData } = getState().messageBot.toJS();
  if (Number.isInteger(index)) {
    return deleteMessageItemApi(messageData[index]).then(
      (response) => {
        dispatch({
          type: DELETE_MESSAGE_SUCCESS,
          payload: { response, index },
        });
      },
    ).catch((error) => {
      dispatch({
        type: DELETE_MESSAGE_FAILURE,
        payload: error,
      });
    });
  }
  return dispatch({
    type: DELETE_MESSAGE_FAILURE,
  });
};

export const getMessages = (offset, limit) => async (dispatch) => {
  dispatch({ type: GET_MESSAGE_PENDING });
  return getMessagesApi(offset, limit).then(
    (response) => {
      dispatch({
        type: GET_MESSAGE_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: GET_MESSAGE_FAILURE,
      payload: error,
    });
  });
};

export const sendMessage = message => async (dispatch) => {
  dispatch({ type: SEND_MESSAGE_PENDING });
  return sendMessageApi({
    robotId: 'robotid test', userId: 'userId test', message,
  }).then(
    (response) => {
      dispatch({
        type: SEND_MESSAGE_SUCCESS,
        payload: response,
      });
    },
  ).catch((error) => {
    dispatch({
      type: SEND_MESSAGE_FAILURE,
      payload: error,
    });
  });
};

const initialState = Map({
  pending: false,
  error: false,
  messageData: List(),
  sendMessagePending: false,
  sendMessageError: false,
});

export default handleActions({
  [GET_MESSAGE_PENDING]: state => state.set('pending', true).set('error', false),
  [GET_MESSAGE_SUCCESS]: (state, { payload: { data: { messageData } } }) => {
    const oldMessageData = state.get('messageData').toJS();
    let prevMessageData = null;
    if (oldMessageData.length > 0) {
      prevMessageData = oldMessageData[oldMessageData.length - 1].createdAt;
    }

    let newMessageData = messageData;
    if (Array.isArray(newMessageData)) {
      newMessageData.reverse().forEach((element, index) => {
        const message = element;
        message.firstTime = moment(message.firstTime);

        if (index > 0) {
          prevMessageData = newMessageData[index - 1].firstTime;
        }

        if (prevMessageData && (prevMessageData.date() !== message.firstTime.date())) {
          message.dayChange = true;
        } else {
          message.dayChange = false;
        }
      });
      newMessageData = fromJS(newMessageData);
      return state.set('pending', false).update('messageData', valueList => newMessageData.concat(valueList));
    }
    return state.set('pending', false);
  },
  [GET_MESSAGE_FAILURE]: state => state.set('pending', false).set('error', true),
  [SEND_MESSAGE_PENDING]: state => state.set('sendMessagePending', true).set('sendMessageError', false),
  [SEND_MESSAGE_SUCCESS]: (state, { payload: { data } }) => {
    const newData = data;
    newData.firstTime = moment(newData.firstTime);
    const oldMessageData = state.get('messageData').toJS();
    let prevMessageData = null;
    if (oldMessageData.length > 0) {
      prevMessageData = oldMessageData[oldMessageData.length - 1].firstTime;
    }

    if (prevMessageData && (prevMessageData.date() !== newData.firstTime.date())) {
      newData.dayChange = true;
    } else {
      newData.dayChange = false;
    }
    return state.set('sendMessagePending', false).update('messageData', valueList => valueList.concat(newData));
  },
  [SEND_MESSAGE_FAILURE]: state => state.set('sendMessagePending', false).set('sendMessageError', true),
  [DELETE_MESSAGE_PENDING]: state => state.set('pending', true).set('error', false),
  [DELETE_MESSAGE_SUCCESS]: (state, { payload: { response, index } }) => {
    if (response) {
      return state.set('pending', false).update('messageData', messageData => messageData.splice(index, 1));
    }
    return state.set('pending', false).set('error', true);
  },
  [DELETE_MESSAGE_FAILURE]: state => state.set('pending', false).set('error', true),
}, initialState);
