/**
 * Circulus Client API 20180917
 * 20140819
 * 20150627
 */

// circulus

import io from 'socket.io-client';

const pibo = {
  _id: undefined, // service id
  _project: undefined, // project id thought circulus platform software
  _socket: undefined,
  _message: {},
  _get: {},
  _set: {},
  _read: {},
  _error: undefined, // for streaming
  _camId: undefined, // v2
  _room: undefined,
  _send: {},
  _callback: {},
  _check: {
    cb: undefined,
    timer: 0,
  },
  ready: undefined,
  onload() {
    // var socket = io.connect('http://125.143.162.247:9001');

    this._socket = io('http://iiot.circul.us:9001');
    this._onSocket(this._socket);

    // var socket = io('/iot', { secure : true });
    // generate default id
    const tokens = window.location.href.split('/cloud/');

    if (tokens.length > 1) {
      const infos = tokens[1].split('/');
      this._id = infos[0];
      this._project = infos[1];
    } else {
      this._id = `u${Date.now()}`;
    }

    /*
        self._message['stream'] = function(data){
            var arrayBuffer = data.buffer;
            var bytes = new Uint8Array(arrayBuffer);
            var blob  = new Blob([bytes.buffer]);

            var image = document.getElementById(_camId);

            var reader = new FileReader();
            reader.onload = function(e) {
                 image.src = e.target.result;
            };
            reader.readAsDataURL(blob);

            self._socket.emit('message', { cmd : 'stream' , data : {}});
        }
        */
  },
  check(cb) {
    const self = this;
    this.send('_check');
    this._check.cb = cb;

    this._check.timer = setTimeout(() => {
      self._check.cb(false);
    }, 5000);
  },
  init(room) {
    console.log(`Initialized : ${room} / ${this._id}`);
    this._room = room;
    this._socket.emit('join', {
      id: this._id, room, type: 'sw', project: this._project,
    });

    this.receive('_check', function () {
      clearTimeout(this._check.timer);

      if (this._check.cb) {
        this._check.cb(true);
      }
    });
  },
  join(room, id) {
    if (!id) {
      var id = this._id;
    }

    this._socket.emit('join', {
      id, room, project: this._project, type: 'sw',
    });
  },
  _onSocket(socket) {
    const self = this;

    socket.on('message', (data) => {
      console.log(`[CIRCULUS] Received : ${data.cmd}`);

      if (self._message[data.cmd]) {
        self._message[data.cmd](data.data);
      }
    });

    // v2
    socket.on('send', (data) => {
      console.log(`[CIRCULUS] Received : ${data.cmd}`);
      console.log(data);
      console.log(self._callback);

      if (self._send[data.room] && self._send[data.room][data.cmd]) {
        self._send[data.room][data.cmd](data.data);
      } else if (self._callback[data.room] && self._callback[data.room][data.cmd]) {
        console.log('callback', data.data);
        self._callback[data.room][data.cmd](data.data);
      } else {
        console.log(`[CIRCULUS] Fail to execution : ${data.cmd} / ${data.room}`);
      }
    });

    // 'error' event is default. must be change
    socket.on('err', (err) => {
      console.log(`[CIRCULUS] Error occured : ${err}`);

      if (self._error) {
        self._error(err);
      }
    });

    socket.on('set', (data) => {
      console.log(`[CIRCULUS] set : ${data.result}`);

      if (self._set[data.key]) {
        self._set[data.key](data.output);
      }
    });

    socket.on('read', (data) => {
      console.log(data);
      console.log(`[CIRCULUS] read : ${data.cmd}`);

      // data.data.id = data.id;

      if (self._read[data.cmd]) {
        self._read[data.cmd](data.data);
      }
    });

    socket.on('get', (data) => {
      console.log(`[CIRCULUS] get : ${data.result}`);

      if (self._get[data.key]) {
        self._get[data.key](data.output);
      }
    });
  },
  send(cmd, data, room, target) {
    if (!room) {
      room = this._room;
    }

    data.cmd = cmd;

    console.log(`[CIRCULUS] Send : ${cmd} / ${room}`);
    this._socket.emit('send', {
      cmd: 'command', data, room, id: this._id, project: this._project, target, type: 'sw',
    });
  },
  receive(cmd, func, room) {
    if (!room) {
      room = this._room;
    }

    if (!this._send[room]) {
      this._send[room] = {};
    }

    this._send[room][cmd] = func;
  },
  callback(cmd, func, room) {
    if (!room) {
      room = this._room;
    }

    if (!this._callback[room]) {
      this._callback[room] = {};
    }

    this._callback[room][cmd] = func;
  },
  motion(motion, option) {
    // option { speed : Number, acceleration: Number, interval: Number}
    if (!option) {
      option.motion = motion;
    } else {
      option = { motion };
    }
    this.send('motion', option, option.room, option.target);
  },
  // forward, backward, etc
  action(type, option) {
    if (option) {
      option.action = type;
    } else {
      option = { type };
    }
    this.send('action', option, option.room, option.target);
  },
  message(text, option) {
    if (option) {
      option.text = text;
    } else {
      option = { text };
    }
    this.send('message', option, option.room, option.target);
  },
  speak(text, option) {
    if (option) {
      option.text = text;
    } else {
      option = { text };
    }
    this.send('speak', option, option.room, option.target);
  },
  talk(text, option) {
    if (option) {
      option.text = text;
    } else {
      option = { text };
    }
    this.send('talk', option, option.room, option.target);
  },
  eye(color, option) { // rgba
    if (option) {
      option.color = color;
    } else {
      option = { color };
    }
    this.send('eye', option, option.room, option.target);
  },
  photo(type, option) {
    if (option) {
      option.type = type;
    } else {
      option = { type };
    }
    this.send('photo', option, option.room, option.target);
  },
  volume(volume, option) {
    this.send('volume', { volume }, option.room, option.target);
  },
  mute(option) {
    this.send('mute', {}, option.room, option.target);
  },
  info(cb) {
    this.send('info', {});

    this.callback('info', (data) => {
      if (cb) {
        cb(data);
      }
    });
  },
  onEvent: {
    // USB, TOUCH, HUMAN, IOT, POWER, HEAT, PLUG, BUTTON
  },
};

export default pibo;

/*
$c = {
	start : undefined,
	ready : function(func){
		this.start = func;
	}
};

    $s.info = function(){
        return _id;
    }

    $s.error = function(func){
     	_error = func;
    }

    $s.write = function(cmd, data){
    	socket.emit('write', { cmd : cmd, data : data , project : _project});
    };

    $s.read = function(cmd, func){
    	_read[cmd] = func;
    };


    // TTS API
    $s.tts = function(text, isKr){
		var audio = new Audio();

        if(isKr){
            //audio.src = 'http://api.ispeech.org/api/rest?apikey=0a693e8460e4c1cfb2e704291b29d73e&action=convert&voice=krkoreanfemale&text=' + text;
            audio.src = 'http://circulus.cloudapp.net:8080/?text=' + text;
            audio.play();
        } else {
            $.post("/tts", { msg: text }, function(data) {
                audio.src = data.url;
                audio.play();
            });
        }
    };
    */
/*
    $s.set = function(key, value, func){
        var tokens = window.location.href.split('/cloud/');
        var infos = tokens[1].split('/');

        var param = {
            projectId : infos[1],
            userId : infos[0],
            key : key
        };

        // callback
        _set[key] = func;
        socket.emit('set', { param : param, value : value});
    }

    $s.get = function(key, func){
        var tokens = window.location.href.split('/cloud/');
        var infos = tokens[1].split('/');

        var param = {
            projectId : infos[1],
            userId : infos[0],
            key : key
        };

        // callback
        _get[key] = func;
        socket.emit('get', { param : param });
    }
    */

// stream
/*
    $s.stream = function(id){
        _camId = id;
        socket.emit('message', { cmd : 'stream' , data : {}});
    }
    */

// timer add
/*
     var interval = setInterval(function(){
       // var tokens = window.location.href.split('/cloud/');
        //var infos = tokens[1].split('/');

         if($c.start){
	        clearInterval(interval);
            $c.start($s);
            us = $s;
         }
     },100);
     */
