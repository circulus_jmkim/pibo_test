import 'semantic-ui-css/semantic.min.css';
import React from 'react';

import { storiesOf } from '@storybook/react';

import Loading from '../components/Loading';
import TopMenu from '../components/TopMenu';
import BottomMenu from '../components/BottomMenu';
import PiboCard from '../components/pibo/PiboCard';
import PiboFavoriteCard from '../components/pibo/PiboFavoriteCard';
import PiboSettingsCard from '../components/pibo/PiboSettingsCard';

storiesOf('로딩', module)
  .add('로딩 화면', () => <Loading />);

storiesOf('상단메뉴', module)
  .add('로그인 전', () => <TopMenu title="타이틀" menuMode="WITH_LOGIN" />)
  .add('로그인 후', () => <TopMenu title="타이틀" menuMode="WITH_USER" />)
  .add('메뉴 버튼', () => <TopMenu title="타이틀" menuMode="WITH_MENU" />)
  .add('설정 버튼', () => <TopMenu title="타이틀" menuMode="WITH_CONFIG" />)
  .add('뒤로 버튼', () => <TopMenu title="타이틀" menuMode="WITH_BACK" />)
  .add('닫기 버튼', () => <TopMenu title="타이틀" menuMode="WITH_CLOSE" />)
  .add('완료 버튼', () => <TopMenu title="타이틀" menuMode="WITH_COMPLETE" />)
  .add('뒤로 버튼과 완료 버튼', () => <TopMenu title="타이틀" menuMode="WITH_BACK_AND_COMPLETE" />)
  .add('뒤로 버튼과 메뉴 버튼', () => <TopMenu title="타이틀" menuMode="WITH_BACK_AND_MENU" />);

storiesOf('하단메뉴', module)
  .add('파이보', () => <BottomMenu selectMenu="pibo" />)
  .add('봇츠', () => <BottomMenu selectMenu="bots" />)
  .add('봇스토어', () => <BottomMenu selectMenu="botstore" />);

storiesOf('파이보 페이지', module)
  .add('나의 파이보', () => (
    <PiboCard
      {...{
        callname: '파이보',
        wifi: 'Circulus',
        battery: 100,
        temper: 27,
        volume: 50,
        pid: '데이터 조회용 기기 키 값',
      }}
    />
  ))
  .add('즐겨찾는 봇', () => <PiboFavoriteCard />);
