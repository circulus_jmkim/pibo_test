import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const getLocale = () => navigator.language || navigator.userLanguage;

const startApp = () => {
  localStorage.setItem('locale', getLocale());
  ReactDOM.render(<App />, document.getElementById('root'));
  registerServiceWorker();
};

if (window.cordova) {
  document.addEventListener('deviceready', startApp, false);
} else {
  startApp();
}

registerServiceWorker();
