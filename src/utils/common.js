export const setLocalStorage = (name, value) => {
  const preData = localStorage.getItem(name);
  if (preData) {
    const obj = JSON.parse(preData);
    localStorage.setItem(name, JSON.stringify({ ...obj, ...value }));
  }
  localStorage.setItem(name, JSON.stringify(value));
};

export const getPid = () => {
  const currentUser = localStorage.getItem('currentUser');
  const parsedData = JSON.parse(currentUser);
  if (parsedData && 'pid' in parsedData) {
    const { pid } = parsedData;
    return pid;
  }
  return '';
};

export const getUid = () => {
  const currentUser = localStorage.getItem('currentUser');
  const parsedData = JSON.parse(currentUser);
  if (parsedData && 'uid' in parsedData) {
    const { uid } = parsedData;
    return uid;
  }
  return '';
};

export const getLocale = () => {
  const locale = localStorage.getItem('locale');
  if (!locale) {
    const lang = navigator.language || navigator.userLanguage;
    localStorage.setItem('locale', lang);
    return lang;
  }
  return locale;
};
