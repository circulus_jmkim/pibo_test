const API_URL = 'http://198.13.42.229:8080/api';
// const API_URL = 'http://localhost:8080/api';
export const ROBOT_URL = 'http://192.168.0.10';
const ww2 = {
  p: false,
  set setPermission(result) {
    this.p = result === 'PERMISSION_GRANTED';
  },
  get getPermission() {
    return this.p;
  },
};

export async function requestPermissions() {
  const { WifiWizard2 } = window;
  if (WifiWizard2) {
    ww2.setPermission = await WifiWizard2.requestPermission();
  }
}

// 모든 봇 정보 가져오기
export async function fetchAllBots() {
  const res = await fetch(`${API_URL}/bot`);
  const result = res.json();
  console.log('fetchAllBots', result);
  const bot = {};
  return bot;
}

// 카테고리 별 봇 정보 가져오기
export async function fetchBotsByCategory(category) {
  const res = await fetch(`${API_URL}/bot/category/${category}`);
  const result = res.json();
  console.log('fetchBotsByCategory', result);
  return result;
}

// 내 봇 리스트 가져오기
export async function fetchMyBots(pid) {
  const res = await fetch(`${API_URL}/bot/${pid}`);
  const result = res.json();
  console.log('fetchMyBots', result);
  return result;
}

// 로봇의 봇 사용여부 변경
export async function updateBotUse(pid, bid, use) {
  const res = await fetch(`${API_URL}/bot/update/${pid}`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ bid, use }),
  });
  const result = res.json();
  console.log('updateBotUse', result);
}

// 로봇의 봇 사용여부 변경
export async function removeBot(pid, bid) {
  const res = await fetch(`${API_URL}/bot/remove`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ pid, bid }),
  });
  const result = res.json();
  console.log('removeBot', result);
  const bot = {};
  return bot;
}

// 파이보 데이터 가져오기
export async function fetchPibo(pid) {
  const res = await fetch(`${API_URL}/pibo/${pid}`);
  const result = res.json();
  console.log('fetchPibo', result);
  return result;
}

// 파이보 데이터 항목 별 가져오기
export async function fetchPiboInfo(pid, item) {
  const res = await fetch(`${API_URL}/pibo/${pid}/${item}`);
  const result = res.json();
  console.log('fetchPiboInfo', result);
  return result;
}

// 파이보 설정 저장
export async function updatePiboInfo(pid, data) {
  const res = await fetch(`${API_URL}/pibo/${pid}`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  const result = res.json();
  console.log('updatePiboInfo', result);
  return result;
}

// 봇 검색 결과 가져오기
export async function fetchSearchResults(str, pid = '') {
  let url = `${API_URL}/bot/search`;
  if (pid) {
    url += `/${pid}/${str}`;
  } else {
    url += `/${str}`;
  }
  const res = await fetch(url);
  const result = res.json();
  console.log('fetchSearchResults', result);
  return result;
}

// 봇 상세 정보 가져오기
export async function fetchBotDetail(bid, uid, pid) {
  const res = await fetch(`${API_URL}/botstore/${bid}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ uid, pid }),
    // body: JSON.stringify({ uid: '525165f6550262c81300001b', pid: 'c5b6c22' }),
  });
  const result = res.json();
  console.log('fetchBotDetail', result);
  return result;
}

export async function fetchBotInstalled(pid, bid) {
  const res = await fetch(`${API_URL}/botstore/install/${pid}/${bid}`);
  const result = res.json();
  console.log('fetchBotInstalled', result);
  return result;
}

export async function fetchCommentsByUserId(bid, uid) {
  const res = await fetch(`${API_URL}/botstore/comment/${bid}/${uid}`);
  const result = res.json();
  console.log('fetchCommentsByUserId', result);
  return result;
}

export async function checkUserId(uid) {
  const res = await fetch(`${API_URL}/user/check/${uid}`);
  const result = res.json();
  console.log('checkUserId', result);
  return result;
}

export async function fetchUserData(uid) {
  const res = await fetch(`${API_URL}/user/${uid}`);
  const result = res.json();
  console.log('fetchUserInfo', result);
  return result;
}

export async function updateUserData(uid, data) {
  const res = await fetch(`${API_URL}/user/update`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ uid, ...data }),
  });
  const result = res.json();
  console.log('updateUserData', result);
  return result;
}

export async function withdrawAccount(uid) {
  const res = await fetch(`${API_URL}/user/withdraw`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ uid }),
  });
  const result = res.json();
  console.log('withdrawAccount', result);
  return result;
}

export async function saveNewUserData(userdata) {
  const {
    account, email, info, names, terms,
  } = userdata;
  const newUser = {
    firstTime: new Date(),
    gender: info.gender,
    password: account.pw,
    userId: account.id,
    birthDate: new Date(`${info.year}-${info.month}-${info.day}`),
    firstName: names.first,
    lastName: names.last,
    email: email.address,
    agreeLocation: terms.location,
    agreePromotion: terms.promotion,
  };
  const res = await fetch(`${API_URL}/user/signup`, {
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(newUser),
  });
  const result = res.json();
  return result;
}

export async function userSingIn(logindata) {
  const res = await fetch(`${API_URL}/user/signin`, {
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(logindata),
  });
  const result = res.json();
  return result;
}

export const getCurrentPosition = (options = {}) => new Promise((resolve, reject) => {
  navigator.geolocation.getCurrentPosition(resolve, reject, options);
});

export async function getGeo(pid) {
  const res = await fetchPiboInfo(pid, 'geo');
  if (res.result) {
    const { data } = res;
    const { item, list, value } = data;
    if (value.length < 2) {
      if (navigator.geolocation) {
        const position = await getCurrentPosition();
        const { coords } = position;
        const { latitude, longitude } = coords;
        return { result: true, data: { item, list, value: [latitude, longitude] } };
      }
      return { result: true, data: { item, list, value: [] } };
    }
  }
  return res;
}

export async function getConnectedSSID() {
  await requestPermissions();
  const { WifiWizard2 } = window;
  if (WifiWizard2 && ww2.getPermission) {
    const result = await WifiWizard2.getConnectedSSID();
    return result;
  }
  return '';
}

export async function connectToRobot(uid) {
  const res = await fetch(`${API_URL}/pibo/`, {
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ uid }),
  });
  const result = res.json();
  console.log(result);
  return result;
}

export async function connectToAp(apdata) {
  try {
    const { essid, wpa, password } = apdata;
    console.log(...apdata);
    const res = await fetch(
      `${ROBOT_URL}/confirm?id=${essid}&type=${wpa ? 'wpa' : 'wep'}&pass=${password}`,
    );
    const result = res.json();
    return result;
  } catch (error) {
    return { result: false, data: { item: 'wifi', error } };
  }
}
