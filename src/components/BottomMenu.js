import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon } from 'semantic-ui-react';

export default class BottomMenu extends Component {
  componentWillMount() {
    const { selectMenu } = this.props;
    this.setState({ activeMenu: selectMenu });
  }

  render() {
    const { handleMenuItemClick } = this.props;
    const { activeMenu } = this.state;

    return (
      <Menu
        icon="labeled"
        widths={3}
        fixed="bottom"
        color="teal"
        size="mini"
        secondary
        className="blurring"
      >
        <Menu.Item name="pibo" active={activeMenu === 'pibo'} onClick={handleMenuItemClick}>
          <Icon name="home" size="mini" />
          pibo
        </Menu.Item>
        <Menu.Item name="bots" active={activeMenu === 'bots'} onClick={handleMenuItemClick}>
          <Icon name="stack overflow" size="mini" />
          bots
        </Menu.Item>
        <Menu.Item name="botstore" active={activeMenu === 'botstore'} onClick={handleMenuItemClick}>
          <Icon name="cart arrow down" size="mini" />
          botstore
        </Menu.Item>
      </Menu>
    );
  }
}

BottomMenu.propTypes = {
  selectMenu: PropTypes.oneOf(['pibo', 'bots', 'botstore']).isRequired,
};
