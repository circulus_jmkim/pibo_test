import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import StoreSearch from './StoreSearch';
import StoreSearchList from './StoreSearchList';
import StoreList from './StoreList';

export default class StoreMain extends Component {
  state = {
    isLoading: false,
    results: [],
    bots: [],
    value: '',
  };

  componentWillMount() {
    const { results, searching, searchStr } = this.props;
    this.setState({ results, isLoading: searching, value: searchStr });
  }

  componentWillReceiveProps(nextProps) {
    const { results, searching, searchStr } = this.props;
    if (results !== nextProps.results) {
      this.setState({ results: nextProps.results });
    }
    if (searching !== nextProps.searching) {
      this.setState({ isLoading: nextProps.searching });
    }
    if (searchStr !== nextProps.searchStr) {
      this.setState({ value: nextProps.searchStr });
    }
  }

  resetComponent = () => {
    this.setState({ isLoading: false, results: [], value: '' });
  };

  handleSearchRemove = () => {
    this.resetComponent();
  };

  handleSearchResult = () => {
    const { resultSelect } = this.props;
    resultSelect();
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ value });
    setTimeout(() => {
      this.addSearchChange();
    }, 500);
  };

  addSearchChange = () => {
    const { isLoading, value } = this.state;
    if (!isLoading) {
      const { addSearchChange } = this.props;
      addSearchChange(value);
    }
  };

  render() {
    const { handleItemClick } = this.props;
    const { isLoading, value } = this.state;

    return (
      <Grid padded>
        <Grid.Row>
          <StoreSearch
            {...this.state}
            handleSearchChange={this.handleSearchChange}
            handleSearchResult={this.handleSearchResult}
            handleSearchRemove={this.handleSearchRemove}
            fluid
          />
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {(isLoading || value) && (
              <StoreSearchList {...this.state} handleItemClick={handleItemClick} />
            )}
            {!(isLoading || value) && <StoreList {...this.props} />}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
