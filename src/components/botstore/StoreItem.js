import React, { Component } from 'react';
import {
  Card, Rating, Statistic, Image, Button,
} from 'semantic-ui-react';

export default class StoreItem extends Component {
  handleClick = () => {
    const { _id, handleItemClick } = this.props;
    handleItemClick(_id);
  };

  render() {
    const {
      icon, title, score, reviews, detail, fluid,
    } = this.props;
    return (
      <Card fluid={fluid}>
        <Card.Content>
          <Image floated="left" size="mini" src={icon} />
          <Card.Header>
            {title}
          </Card.Header>
          <Card.Meta>
            <Rating icon="star" defaultRating={score / 2} maxRating={5} />
            <Statistic>
              {reviews && reviews.length}
            </Statistic>
          </Card.Meta>
          <Card.Description>
            {detail && detail.description}
          </Card.Description>
          <Button basic floated="right" color="blue" onClick={this.handleClick}>
            받기
          </Button>
        </Card.Content>
      </Card>
    );
  }
}
