import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import StoreItem from './StoreItem';

export default class StoreSearchList extends Component {
  render() {
    const { results, handleItemClick } = this.props;
    return (
      <Grid padded columns={16}>
        <Grid.Row width={16}>
          {results.map(item => (
            <StoreItem {...item} key={item._id} handleItemClick={handleItemClick} fluid />
          ))}
        </Grid.Row>
      </Grid>
    );
  }
}
