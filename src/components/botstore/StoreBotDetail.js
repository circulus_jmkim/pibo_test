import React, { Component } from 'react';
import {
  Image,
  Button,
  Segment,
  Grid,
  Header,
  Icon,
  Statistic,
  Rating,
  Accordion,
  Card,
} from 'semantic-ui-react';

export default class StoreBotDetail extends Component {
  state = {
    activeIndex: 0,
  };

  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  };

  render() {
    const {
      category,
      detail,
      icon,
      makerImg,
      makerNickname,
      reviews,
      score,
      title,
      user,
    } = this.props;
    const { activeIndex } = this.state;

    return (
      <Grid centered padded columns="equal">
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Grid padded>
                <Grid.Row>
                  <Grid.Column width={6} verticalAlign="middle">
                    <Image src={icon} />
                  </Grid.Column>
                  <Grid.Column width={10} verticalAlign="middle">
                    <Grid>
                      <Grid.Row>
                        <Header>
                          {title}
                        </Header>
                      </Grid.Row>
                      <Grid.Row>
                        {category}
                      </Grid.Row>
                      <Grid.Row>
                        {user
                          && user.isInstalled && (
                            <div>
                              <Button color="blue" size="mini" compact>
                                제거
                              </Button>
                              <Button color="blue" size="mini" compact inverted>
                                업데이트
                              </Button>
                            </div>
                        )}
                        {user
                          && !user.isInstalled && (
                            <Button color="blue" size="mini" compact>
                              받기
                            </Button>
                        )}
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={3}>
                    <Image src={makerImg} avatar />
                  </Grid.Column>
                  <Grid.Column width={11} verticalAlign="middle">
                    <Header size="small">
                      {makerNickname}
                    </Header>
                  </Grid.Column>
                  <Grid.Column width={2} verticalAlign="middle">
                    <Icon name="angle right" />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={1}>
                    <Statistic>
                      {score && (score / 2).toFixed(1)}
                    </Statistic>
                  </Grid.Column>
                  <Grid.Column width={4} verticalAlign="middle">
                    <Rating maxRating={5} defaultRating={(score / 2)} icon="star" size="tiny" />
                  </Grid.Column>
                  <Grid.Column>
                    <Statistic>
                      {reviews && reviews.length}
                    </Statistic>
                  </Grid.Column>
                  <Grid.Column width={2} floated="right" verticalAlign="middle">
                    <Icon name="angle right" />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Accordion fluid styled>
              <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick}>
                <Grid>
                  <Grid.Column width={14} floated="left">
                    업데이트 내역
                  </Grid.Column>
                  <Grid.Column width={2} floated="right" verticalAlign="middle">
                    {activeIndex !== 0 && <Icon name="angle up" />}
                    {activeIndex === 0 && <Icon name="angle down" />}
                  </Grid.Column>
                </Grid>
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 0}>
                <p>
                  {detail && detail.update}
                </p>
              </Accordion.Content>
            </Accordion>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Accordion fluid styled>
              <Accordion.Title active={activeIndex === 1} index={1} onClick={this.handleClick}>
                <Grid>
                  <Grid.Column width={14} floated="left">
                    기능 설명
                  </Grid.Column>
                  <Grid.Column width={2} floated="right" verticalAlign="middle">
                    {activeIndex !== 1 && <Icon name="angle up" />}
                    {activeIndex === 1 && <Icon name="angle down" />}
                  </Grid.Column>
                </Grid>
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 1}>
                <p>
                  {detail && detail.description}
                </p>
              </Accordion.Content>
            </Accordion>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Accordion fluid styled>
              <Accordion.Title active={activeIndex === 2} index={2} onClick={this.handleClick}>
                <Grid>
                  <Grid.Column width={14} floated="left">
                    요구 사항
                  </Grid.Column>
                  <Grid.Column width={2} floated="right" verticalAlign="middle">
                    {activeIndex !== 2 && <Icon name="angle up" />}
                    {activeIndex === 2 && <Icon name="angle down" />}
                  </Grid.Column>
                </Grid>
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 2}>
                <p>
                  {detail && detail.requires}
                </p>
              </Accordion.Content>
            </Accordion>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Header size="small">
리뷰 및 평가
              </Header>
              {user
                && user.isInstalled
                && user.review.hasOwnProperty('id') && (
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={3}>
                        <Image src={user.img} avatar />
                      </Grid.Column>
                      <Grid.Column width={4} verticalAlign="middle">
                        <Header size="small">
                          {user.name}
                        </Header>
                      </Grid.Column>
                      <Grid.Column width={9} verticalAlign="middle">
                        <Rating
                          icon="star"
                          defaultRating={user.review.score}
                          maxRating={5}
                          size="massive"
                        />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column>
                        <Card fluid>
                          <Card.Header>
                            {user.review.title}
                          </Card.Header>
                          <Card.Meta>
                            {user.review.lastTime}
                          </Card.Meta>
                          <Card.Description>
                            {user.review.content}
                          </Card.Description>
                        </Card>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
              )}
              {user
                && user.isInstalled
                && !user.hasOwnProperty('review') && (
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={3}>
                        <Image src={user.img} avatar />
                      </Grid.Column>
                      <Grid.Column width={4} verticalAlign="middle">
                        <Header size="small">
                          {user.name}
                        </Header>
                      </Grid.Column>
                      <Grid.Column width={9} verticalAlign="middle">
                        <Rating maxRating={5} size="massive" />
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row centered>
                      <Grid.Column width={15}>
                        <Button color="blue" fluid>
                          리뷰 작성하기
                        </Button>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
              )}
              <Grid>
                {reviews
                  && reviews.length
                  && reviews.map(item => (
                    <Grid.Row key={item.id}>
                      <Grid.Column>
                        <Card fluid>
                          <Card.Header>
                            {item.title}
                          </Card.Header>
                          <Card.Meta>
                            <Rating icon="star" defaultRating={item.gpa} maxRating={5} />
                            {item.name}
                            {' '}
                            {item.regdate}
                          </Card.Meta>
                          <Card.Description>
                            {item.content}
                          </Card.Description>
                        </Card>
                      </Grid.Column>
                    </Grid.Row>
                  ))}
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Button fluid>
              모든 리뷰 보기 (
              {reviews && reviews.length}
              )
            </Button>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row />
        <Grid.Row />
      </Grid>
    );
  }
}
