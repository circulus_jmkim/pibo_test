import React, { Component } from 'react';
import {
  Grid, Image, Button, Card, Loader,
} from 'semantic-ui-react';
import StoreSearch from './StoreSearch';

export default class StoreMyBot extends Component {
  state = {
    isLoading: false,
    results: [],
    bots: [],
    value: '',
  };

  componentWillMount() {
    const {
      results, searching, searchStr,
    } = this.props;
    this.setState({
      results, isLoading: searching, value: searchStr,
    });
  }

  componentWillReceiveProps(nextProps) {
    const { results, searching, searchStr } = this.props;
    if (results !== nextProps.results) {
      this.setState({ results: nextProps.results });
    }
    if (searching !== nextProps.searching) {
      this.setState({ isLoading: nextProps.searching });
    }
    if (searchStr !== nextProps.searchStr) {
      this.setState({ value: nextProps.searchStr });
    }
  }

  resetComponent = () => {
    this.setState({ isLoading: false, results: [], value: '' });
  };

  handleSearchRemove = () => {
    this.resetComponent();
  };

  handleSearchResult = () => {
    const { resultSelect } = this.props;
    resultSelect();
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ value });
    setTimeout(() => {
      this.addSearchChange();
    }, 500);
  };

  addSearchChange = () => {
    const { isLoading, value } = this.state;
    if (!isLoading) {
      const { addSearchChange } = this.props;
      addSearchChange(value);
    }
  };

  render() {
    const { handleUpdateClick, handleRemoveClick, bots } = this.props;
    const {
      isLoading, value, results,
    } = this.state;

    return (
      <Grid padded>
        <Grid.Row>
          <StoreSearch
            {...this.state}
            handleSearchChange={this.handleSearchChange}
            handleSearchResult={this.handleSearchResult}
            handleSearchRemove={this.handleSearchRemove}
            fluid
          />
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Loader active={isLoading} />
            <Grid padded columns={16}>
              <Grid.Row width={16}>
                {(value && results.length > 0) && (
                  results.map(item => (
                    <Card key={item.id} fluid>
                      <Card.Content>
                        <Image floated="left" size="mini" src={`../${item.image}`} />
                        <Card.Header>
                          {item.title}
                        </Card.Header>
                        <Button basic compact floated="right" size="mini" color="red" onClick={handleRemoveClick}>
  삭제
                        </Button>
                        <Button compact floated="right" size="mini" color="blue" onClick={handleUpdateClick}>
  업데이트
                        </Button>
                      </Card.Content>
                    </Card>
                  ))
                )}
                {!value && (
                  bots.map(item => (
                    <Card key={item.id} fluid>
                      <Card.Content>
                        <Image floated="left" size="mini" src={`../${item.image}`} />
                        <Card.Header>
                          {item.title}
                        </Card.Header>
                        <Button basic compact floated="right" size="mini" color="red" onClick={handleRemoveClick}>
  삭제
                        </Button>
                        <Button basic compact floated="right" size="mini" color="blue" onClick={handleUpdateClick}>
  업데이트
                        </Button>
                      </Card.Content>
                    </Card>
                  ))
                )}
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
