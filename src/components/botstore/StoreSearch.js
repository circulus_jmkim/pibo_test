import React, { Component } from 'react';
import { Search, Grid, Icon } from 'semantic-ui-react';

export default class StoreSearch extends Component {
  render() {
    const {
      isLoading,
      value,
      handleSearchResult,
      handleSearchChange,
      handleSearchRemove,
    } = this.props;

    return (
      <Grid.Column>
        <Search
          open={false}
          loading={isLoading}
          onResultSelect={handleSearchResult}
          onSearchChange={handleSearchChange}
          value={value}
          input={{ fluid: true, prompt: '검색어를 입력하세요.' }}
          icon={
            value.length ? (
              <Icon name="delete" link onClick={handleSearchRemove} />
            ) : (
              <Icon name="search" />
            )
          }
          fluid
        />
      </Grid.Column>
    );
  }
}
