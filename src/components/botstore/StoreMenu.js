import React, { Component } from 'react';
import { Sidebar, Menu, Segment } from 'semantic-ui-react';

export default class StoreMenu extends Component {
  render() {
    const {
      visible, MainPusher, handleSidebarHide, handleMyBotClick,
    } = this.props;
    return (
      <Sidebar.Pushable as={Segment}>
        <Sidebar
          as={Menu}
          animation="overlay"
          onHide={handleSidebarHide}
          vertical
          visible={visible}
          width="thin"
          direction="right"
        >
          <Menu.Item as="a" name="mybot" onClick={handleMyBotClick}>
            내 봇
          </Menu.Item>
        </Sidebar>
        <MainPusher dimmed={visible} />
      </Sidebar.Pushable>
    );
  }
}
