import React, { Component } from 'react';
import styled from 'styled-components';
import Slick from 'react-slick';
import { Grid, Header } from 'semantic-ui-react';
import StoreItem from './StoreItem';

const Image = styled.img`
  width: auto;
  height: 150px;
`;

export default class StoreList extends Component {
  static defaultProps = {
    adItems: [],
    categoryItems: [],
    handleItemClick: () => {},
  }

  render() {
    const { handleItemClick, adItems, categoryItems } = this.props;
    const adSettings = {
      centerMode: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      swipeToSlide: true,
      arrows: false,
    };

    const listSettings = {
      slidesToShow: 1,
      slidesToScroll: 1,
      swipeToSlide: true,
      arrows: false,
    };

    return (
      <Grid>
        <Grid.Row>
          <Slick {...adSettings}>
            {adItems.length > 0
              && adItems.map(item => (
                <div key={item.id}>
                  <Image src={item.image} />
                </div>
              ))}
          </Slick>
        </Grid.Row>
        {
          categoryItems.length > 0 && categoryItems.map((categoryItem, idx) => (
            <Grid.Row key={idx}>
              <Grid.Column width={14}>
                <Header textAlign="left">
                  {categoryItem.category}
                </Header>
              </Grid.Column>
              <Slick {...listSettings}>
                {
                categoryItem.data && categoryItem.data.map(item => (
                  <StoreItem
                    {...item}
                    key={item._id}
                    handleItemClick={handleItemClick}
                    fluid={false}
                  />
                ))
                }
              </Slick>
            </Grid.Row>
          ))
        }
      </Grid>
    );
  }
}
