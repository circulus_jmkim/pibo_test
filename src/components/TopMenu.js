import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Menu, Header, Icon } from 'semantic-ui-react';

export default class TopMenu extends Component {
  getMenuComps = (menuMode) => {
    const btnCtrl = {
      isBackButton: false,
      isUserIcon: false,
      isLoginLabel: false,
      isCompleteButton: false,
      isConfigButton: false,
      isMenuButton: false,
      isCloseButton: false,
    };

    switch (menuMode) {
      case 'WITH_LOGIN':
        btnCtrl.isUserIcon = true;
        btnCtrl.isLoginLabel = true;
        break;
      case 'WITH_USER':
        btnCtrl.isUserIcon = true;
        break;
      case 'WITH_MENU':
        btnCtrl.isMenuButton = true;
        break;
      case 'WITH_CONFIG':
        btnCtrl.isConfigButton = true;
        break;
      case 'WITH_BACK':
        btnCtrl.isBackButton = true;
        break;
      case 'WITH_CLOSE':
        btnCtrl.isCloseButton = true;
        break;
      case 'WITH_COMPLETE':
        btnCtrl.isCompleteButton = true;
        break;
      case 'WITH_BACK_AND_COMPLETE':
        btnCtrl.isBackButton = true;
        btnCtrl.isCompleteButton = true;
        break;
      case 'WITH_BACK_AND_MENU':
        btnCtrl.isBackButton = true;
        btnCtrl.isMenuButton = true;
        break;
      default:
        break;
    }
    return btnCtrl;
  };

  render() {
    const {
      title, handleCompleteClick, menuMode, handleBackClick,
    } = this.props;

    const btnCtrl = this.getMenuComps(menuMode);

    return (
      <Menu fixed="top" secondary fluid>
        <Menu.Item name="title">
          <Header size="large">
            {btnCtrl.isBackButton && (
              <Icon name="angle left" size="small" onClick={handleBackClick} />
            )}
            {title}
          </Header>
        </Menu.Item>
        <Menu.Menu position="right">
          {btnCtrl.isUserIcon && (
            <Menu.Item name="login" onClick={handleCompleteClick}>
              {btnCtrl.isLoginLabel && '로그인 하세요.'}
              <Icon name="user circle" size="large" />
            </Menu.Item>
          )}
          {btnCtrl.isCloseButton && (
            <Menu.Item name="close" onClick={handleCompleteClick}>
              <Icon name="close" size="large" />
            </Menu.Item>
          )}
          {btnCtrl.isCompleteButton && (
            <Menu.Item name="complete" onClick={handleCompleteClick}>
              완료
            </Menu.Item>
          )}
          {btnCtrl.isConfigButton && (
            <Menu.Item name="config" onClick={handleCompleteClick}>
              <Icon name="cog" size="large" />
            </Menu.Item>
          )}
          {btnCtrl.isMenuButton && (
            <Menu.Item name="menu" onClick={handleCompleteClick}>
              <Icon name="bars" size="large" />
            </Menu.Item>
          )}
        </Menu.Menu>
      </Menu>
    );
  }
}

TopMenu.propTypes = {
  title: PropTypes.string.isRequired,
  menuMode: PropTypes.oneOf([
    'WITH_LOGIN',
    'WITH_USER',
    'WITH_MENU',
    'WITH_CONFIG',
    'WITH_BACK',
    'WITH_CLOSE',
    'WITH_COMPLETE',
    'WITH_BACK_AND_COMPLETE',
    'WITH_BACK_AND_MENU',
  ]).isRequired,
};
