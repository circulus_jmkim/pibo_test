import React, { Component } from 'react';
import { Dimmer } from 'semantic-ui-react';
import styled from 'styled-components';

const CirculusLoader = () => (
  <StyledCirculusLoader viewBox="0 0 100 100">
    <defs>
      <clipPath id="cut-off-bottom">
        <rect x="0" y="0" width="94" height="42" />
      </clipPath>
    </defs>
    <circle
      className="brightHalf"
      fill="none"
      stroke="#D1D3D4"
      strokeWidth="4"
      cx="50"
      cy="50"
      r="44"
      clipPath="url(#cut-off-bottom)"
    >
      <animateTransform
        attributeName="transform"
        dur="3s"
        type="rotate"
        from="0 50 50"
        to="360 50 50"
        repeatCount="indefinite"
      />
    </circle>
    <circle
      className="blueHalf"
      fill="none"
      stroke="#03BFD7"
      strokeWidth="4"
      cx="50"
      cy="50"
      r="44"
      clipPath="url(#cut-off-bottom)"
    >
      <animateTransform
        attributeName="transform"
        dur="3s"
        type="rotate"
        from="180 50 50"
        to="540 50 50"
        repeatCount="indefinite"
      />
    </circle>
    <circle
      className="blueCircle"
      fill="none"
      stroke="#03BFD7"
      strokeWidth="3"
      cx="8"
      cy="54"
      r="6"
    >
      <animateTransform
        attributeName="transform"
        dur="3s"
        type="rotate"
        from="0 50 50"
        to="360 50 50"
        repeatCount="indefinite"
      />
    </circle>
    <circle
      className="grayCircle"
      fill="none"
      stroke="#D1D3D4"
      strokeWidth="3"
      cx="8"
      cy="54"
      r="6"
    >
      <animateTransform
        attributeName="transform"
        dur="3s"
        type="rotate"
        from="180 50 50"
        to="540 50 50"
        repeatCount="indefinite"
      />
    </circle>
  </StyledCirculusLoader>
);

const StyledCirculusLoader = styled.svg`
  width: 60px;
  height: 85px;
`;

const LoaderText = styled.p`
  font-family: 'Comfortaa';
  display: flex;
  justify-content: center;
  font-size: 1.2rem;
  font-weight: 100;
  color: #fff;
`;

export default class Loading extends Component {
  render() {
    return (
      <div>
        <Dimmer blurring="ture" active page>
          <CirculusLoader />
          <LoaderText inverted>
loading...
          </LoaderText>
        </Dimmer>
      </div>
    );
  }
}
