import React, { Component } from 'react';
import { Dimmer, Segment, Button } from 'semantic-ui-react';

export default class Popup extends Component {
  state = {
    isPopup: false,
  };

  componentDidMount() {
    const { isPopup } = this.props;
    this.setState({ isPopup });
  }

  handleClickLeft = () => {
    const { leftFunc } = this.props;
    leftFunc();
  };

  handleClickRight = () => {
    const { rightFunc } = this.props;
    rightFunc();
  };

  render() {
    const { isPopup } = this.state;
    const {
      leftLabel, rightLabel, contents, mode,
    } = this.props;

    const btnColor = (value) => {
      switch (value) {
        case 'alert':
          return 'red';
        case 'confirm':
          return 'blue';
        default:
          return 'grey';
      }
    };

    return (
      <Dimmer active={isPopup} onClickOutside={this.handleClickLeft} page>
        <Segment compact>
          {contents}
          <Button.Group attached="bottom">
            <Button onClick={this.handleClickLeft}>
              {leftLabel}
            </Button>
            <Button color={btnColor(mode)} onClick={this.handleClickRight}>
              {rightLabel}
            </Button>
          </Button.Group>
        </Segment>
      </Dimmer>
    );
  }
}
