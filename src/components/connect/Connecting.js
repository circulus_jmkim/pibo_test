import React, { Component } from 'react';
import { Header, Icon, Button } from 'semantic-ui-react';
import styled from 'styled-components';

const ConnectWrap = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const ConnectButton = styled(Button.Group)`
  margin: 0 !important;
  position: fixed !important;
  bottom: 0;
  left: 0;
  right: 0;
`;

const FilledBg = styled.div`
  width: 100vw;
  height: calc(55vh + 2rem);
  bottom: 0;
  position: absolute;
  background: linear-gradient(45deg, #fff, #03bfd7, #0894d7);
  background-size: 400% 400%;
  -webkit-animation: colors 20s ease infinite;
  -moz-animation: colors 20s ease infinite;
  animation: colors 20s ease infinite;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default class Connecting extends Component {
  state = {
    connected: false,
  };

  componentDidMount() {
    const { onConnect } = this.props;
    onConnect();
  }

  onNextClick = () => {
    const { onConnect, onNextClick, ssid } = this.props;
    if (!ssid || ssid.search('pibo_') < 0) {
      return onConnect();
    }
    return onNextClick();
  };

  render() {
    const { onPrevClick, loading, ssid } = this.props;
    return (
      <ConnectWrap>
        {loading && (
          <div style={{ textAlign: 'center' }}>
            <Icon
              name="spinner"
              size="huge"
              style={{ marginTop: '2rem', color: 'rgba(8, 148, 215, 1)' }}
              loading
            />
            <Header as="h3">
파이보 찾는 중...
            </Header>
          </div>
        )}
        {!loading && (
          <div style={{ position: 'relative', width: '70vw' }}>
            <img
              src="../image/pibo_trimmed.png"
              alt="pibo"
              style={{ width: '100%', marginTop: '1rem', position: 'absolute' }}
            />
            <img
              src="../image/pibo_arm.png"
              alt="pibo arm"
              style={{
                width: '100%',
                marginTop: '1rem',
                position: 'absolute',
                zIndex: '99',
              }}
            />
          </div>
        )}
        <FilledBg>
          {!loading
            && !ssid && (
              <Header as="h3" style={{ color: '#fff', width: '75%', textAlign: 'center' }}>
                디바이스의 Wi-Fi 목록에서
                <p style={{ fontSize: '1.5em', color: '#EFEFEF' }}>
pibo_#####
                </p>
                <p>
위와 같은 형식의 Wi-Fi를 선택하고
                </p>
                <p>
비밀번호 1234567890을
                </p>
                <p>
입력하여 연결하세요.
                </p>
                <p style={{ fontSize: '0.6em', color: '#ECECEC' }}>
                  pibo_##### 형식의 Wi-Fi가 목록에 보이지 않을 경우 파이보가 켜져 있는지 확인하세요.
                </p>
              </Header>
          )}
          {!loading
            && ssid && (
              <Header as="h3" style={{ color: '#fff', width: '70%', textAlign: 'center' }}>
                {ssid}
                <p>
Wi-Fi에 연결되었습니다.
                </p>
              </Header>
          )}
        </FilledBg>
        {!loading && (
          <ConnectButton widths="2" fluid attached="bottom">
            <Button
              name="prev"
              onClick={onPrevClick}
              style={{ background: 'rgba(209, 211, 212, 1)', color: '#fff', borderRadius: '0px' }}
            >
              연결취소
            </Button>
            <Button name="next" style={{ borderRadius: '0px' }} onClick={this.onNextClick}>
              다음
            </Button>
          </ConnectButton>
        )}
      </ConnectWrap>
    );
  }
}
