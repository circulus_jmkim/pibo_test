import React, { Component } from 'react';
import { Header, Button } from 'semantic-ui-react';
import styled from 'styled-components';

const ConnectWrap = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const ConnectButton = styled(Button.Group)`
  margin: 0 !important;
  position: fixed !important;
  bottom: 0;
  left: 0;
  right: 0;
`;

export default class Connected extends Component {
  componentDidMount() {
    const { onMount } = this.props;
    onMount();
  }

  render() {
    const { onNextClick } = this.props;

    return (
      <ConnectWrap>
        <div style={{ position: 'relative', width: '70vw' }}>
          <img
            src="../image/pibo_trimmed.png"
            alt="welcome"
            style={{ width: '100%', marginTop: '16vh', position: 'absolute' }}
          />
        </div>
        <Header as="h3">
파이보와 성공적으로 연결되었습니다.
        </Header>
        <ConnectButton widths="1" fluid attached="bottom">
          <Button
            name="next"
            onClick={onNextClick}
            style={{ background: 'rgba(8, 148, 215, 1)', color: '#fff', borderRadius: '0px' }}
          >
            시작하기
          </Button>
        </ConnectButton>
      </ConnectWrap>
    );
  }
}
