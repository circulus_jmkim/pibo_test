import React, { Component } from 'react';
import {
  Header, Icon, Button, Form,
} from 'semantic-ui-react';
import styled from 'styled-components';

const ConnectWrap = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const ConnectButton = styled(Button.Group)`
  margin: 0 !important;
  position: fixed !important;
  bottom: 0;
  left: 0;
  right: 0;
`;

const Input = styled.input`
  background: rgba(0, 0, 0, 0) !important;
  border: 0 !important;
  border-bottom: 1px solid #fff !important;
  border-radius: 0 !important;
  color: #fff !important;
  &::placeholder {
    color: #fff !important;
    font-weight: 100 !important;
  }
  &::selection {
    color: #fff;
  }
  &::-webkit-input-placeholder {
    color: #fff !important;
    font-weight: 100 !important;
  }
  &::after {
    content: '\f107';
    width: 40px;
    height: 40px;
  }
`;

const FilledBg = styled.div`
  width: 100vw;
  height: calc(55vh + 2rem);
  bottom: 0;
  position: absolute;
  background: linear-gradient(45deg, #fff, #03bfd7, #0894d7);
  background-size: 400% 400%;
  -webkit-animation: colors 20s ease infinite;
  -moz-animation: colors 20s ease infinite;
  animation: colors 20s ease infinite;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default class Connecting extends Component {
  state = {
    loading: false,
  };

  render() {
    const {
      onPrevClick, onNextClick, onChange, aplist, ap,
    } = this.props;
    const { loading } = this.state;
    return (
      <ConnectWrap>
        <Icon
          name="wifi"
          size="huge"
          style={{ textAlign: 'center', marginTop: '2rem', color: 'rgba(8, 148, 215, 1)' }}
        />
        <Header as="h3">
          {loading ? '와이파이 연결 중입니다.' : '연결할 와이파이를 확인해주세요.'}
        </Header>
        <FilledBg>
          <Form size="large" style={{ width: '70%' }}>
            <div className="field">
              <div className="ui fluid input">
                <Input
                  placeholder="와이파이를 선택하세요."
                  type="text"
                  name="apinput"
                  list="wifilist"
                  disabled={loading}
                  value={ap.essid}
                  onClick={onChange}
                  readOnly
                />
                <datalist id="wifilist">
                  {aplist.map((item, idx) => (
                    <option id={`ap${idx}`} key={item.address} value={item.essid} />
                  ))}
                </datalist>
              </div>
            </div>
            <div className="field">
              <div className="ui fluid input">
                <Input
                  placeholder="비밀번호"
                  type="password"
                  name="appw"
                  disabled={!ap.encrypted}
                  value={ap.password}
                  onChange={onChange}
                />
              </div>
            </div>
          </Form>
        </FilledBg>
        <ConnectButton widths="2" fluid attached="bottom">
          <Button
            name="prev"
            onClick={onPrevClick}
            disabled={loading}
            style={{ background: 'rgba(209, 211, 212, 1)', color: '#fff', borderRadius: '0px' }}
          >
            이전
          </Button>
          <Button
            inverted
            name="next"
            onClick={onNextClick}
            disabled={loading}
            style={{ background: '#fff', color: 'rgba(8, 148, 215, 1)', borderRadius: '0px' }}
          >
            Wi-Fi 연결
          </Button>
        </ConnectButton>
      </ConnectWrap>
    );
  }
}
