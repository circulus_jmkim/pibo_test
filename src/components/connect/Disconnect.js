import React, { Component } from 'react';
import { Header, Icon, Button } from 'semantic-ui-react';
import styled from 'styled-components';

const ConnectWrap = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

const ConnectButton = styled(Button.Group)`
  margin: 0 !important;
  position: fixed !important;
  bottom: 0;
  left: 0;
  right: 0;
`;

const FilledBg = styled.div`
  width: 100vw;
  height: 55vh;
  bottom: 2rem;
  position: absolute;
  background: linear-gradient(45deg, #fff, #03bfd7, #0894d7);
  background-size: 400% 400%;
  -webkit-animation: colors 20s ease infinite;
  -moz-animation: colors 20s ease infinite;
  animation: colors 20s ease infinite;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default class Disconnect extends Component {
  render() {
    const { onPrevClick, onNextClick } = this.props;

    return (
      <ConnectWrap>
        <Icon
          name="unlink"
          size="huge"
          style={{ textAlign: 'center', marginTop: '2rem', color: 'rgba(8, 148, 215, 1)' }}
        />
        <Header as="h3">
연결 된 파이보 없음
        </Header>
        <FilledBg>
          <Header as="h3" style={{ color: '#fff', width: '70%', textAlign: 'center' }}>
            아직 연결 된 파이보가 없습니다.
            <p>
나의 파이보와 연결해 보세요.
            </p>
          </Header>
        </FilledBg>
        <ConnectButton widths="2" fluid attached="bottom">
          <Button
            name="prev"
            onClick={onPrevClick}
            style={{ background: 'rgba(209, 211, 212, 1)', color: '#fff', borderRadius: '0px' }}
          >
            이전
          </Button>
          <Button
            inverted
            name="next"
            onClick={onNextClick}
            style={{ background: '#fff', color: 'rgba(8, 148, 215, 1)', borderRadius: '0px' }}
          >
            연결하기
          </Button>
        </ConnectButton>
      </ConnectWrap>
    );
  }
}
