import React, { Component } from 'react';
import { Form, Message } from 'semantic-ui-react';
import { AccountForm, AccountWrap, AccountInput } from './AccountComponents';

export default class Names extends Component {
  render() {
    const {
      onChange, last, first, msg,
    } = this.props;
    return (
      <div>
        <AccountForm>
          <AccountWrap>
            <Form size="large">
              <div className="field">
                <div className="ui fluid input">
                  <AccountInput
                    placeholder="성"
                    name="last"
                    type="text"
                    value={last}
                    onChange={onChange}
                  />
                </div>
              </div>
              <div className="field">
                <div className="ui fluid input">
                  <AccountInput
                    placeholder="이름"
                    name="first"
                    type="text"
                    value={first}
                    onChange={onChange}
                  />
                </div>
              </div>
            </Form>
          </AccountWrap>
        </AccountForm>
        <Message negative hidden={!msg} style={{ marginBottom: '3rem' }}>
          <Message.Header>
            {msg}
          </Message.Header>
        </Message>
      </div>
    );
  }
}
