import React, { Component } from 'react';
import {
  Header, Form, Button, Icon,
} from 'semantic-ui-react';
import styled from 'styled-components';
import withLoading from '../../hocs/withLoading';

const FilledBg = styled.div`
  width: 100vw;
  height: 100vh;
  position: absolute;
  background: linear-gradient(45deg, #fff, #03bfd7, #0894d7);
  background-size: 400% 400%;
  -webkit-animation: colors 20s ease infinite;
  -moz-animation: colors 20s ease infinite;
  animation: colors 20s ease infinite;
`;

const LoginForm = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column;
  height: 100vh;
`;

const LoginWrap = styled.div`
  margin: 0 !important;
  width: 80vw;
`;

const LoginInput = styled.input`
  background: rgba(0, 0, 0, 0) !important;
  border: 0 !important;
  border-bottom: 1px solid #fff !important;
  border-radius: 0 !important;
  color: #fff !important;
  &::placeholder {
    color: #fff !important;
    font-weight: 100 !important;
  }
  &::selection {
    color: #fff;
  }
  &::-webkit-input-placeholder {
    color: #fff !important;
    font-weight: 100 !important;
  }
`;

const LoginButton = styled(Button)`
  background: rgba(0, 0, 0, 0) !important;
  border: 1px solid #fff !important;
  border-radius: 0px !important;
  color: #fff !important;
  margin-top: 2rem !important;
`;
const JoinButton = styled(Button)`
  color: rgba(8, 148, 215, 0.5) !important;
  border: 1px solid #fff !important;
  border-radius: 0px !important;
  background: #fff !important;
  margin-top: 1rem !important;
`;
const LoginHeader = styled(Header)`
  color: #fff !important;
  font-weight: 300 !important;
  margin: 1.5rem 0 !important;
`;
const SkipButton = styled(Button)`
  background: rgba(0, 0, 0, 0) !important;
  border: 0px !important;
  color: #fff !important;
  height: 50px !important;
  position: absolute !important;
  bottom: 10vw !important;
  right: 10vw !important;
  text-align: right !important;
  padding: 0px !important;
`;

class Login extends Component {
  render() {
    const {
      onLogin, onJoin, onSkip, onChange, id, pw, msg,
    } = this.props;
    return (
      <FilledBg>
        <LoginForm>
          <LoginWrap>
            <LoginHeader as="h4" textAlign="center">
              서큘러스 계정으로 로그인하세요.
            </LoginHeader>
            <Form size="large">
              <div className="field">
                <div className="ui fluid input">
                  <LoginInput placeholder="아이디" name="userid" value={id} onChange={onChange} />
                </div>
              </div>
              <div className="field">
                <div className="ui fluid input">
                  <LoginInput
                    placeholder="비밀번호"
                    type="password"
                    name="password"
                    value={pw}
                    onChange={onChange}
                  />
                </div>
              </div>
              {
                (msg.length > 0) && (
                  <p style={{ color: 'rgba(86, 91, 91, 1)', fontWeight: '700' }}>
                    {msg}
                  </p>
                )
              }
              <LoginButton fluid size="large" onClick={onLogin}>
                로그인
              </LoginButton>
              <JoinButton fluid size="large" onClick={onJoin}>
                회원가입
              </JoinButton>
            </Form>
          </LoginWrap>
          <SkipButton labelPosition="right" onClick={onSkip}>
            건너뛰기
            <Icon name="angle right" />
          </SkipButton>
        </LoginForm>
      </FilledBg>
    );
  }
}

export default withLoading(Login);
