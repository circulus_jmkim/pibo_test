import React, { Component } from 'react';
import { Form, Message } from 'semantic-ui-react';
import { AccountForm, AccountWrap, AccountInput } from './AccountComponents';

export default class Info extends Component {
  render() {
    const {
      onChange, year, month, day, gender, genderList, msg,
    } = this.props;
    const selectMonth = month ? `${month}월` : '';
    const selectGender = (select) => {
      switch (select) {
        case 'male':
          return '남성';
        case 'female':
          return '여성';
        case 'unknown':
          return '공개 안함';
        default:
          return '';
      }
    };
    return (
      <div>
        <AccountForm>
          <AccountWrap>
            <Form size="large">
              <div className="field">
                <div className="ui input" style={{ width: '32%' }}>
                  <AccountInput
                    placeholder="연도"
                    name="year"
                    type="number"
                    value={year}
                    onChange={onChange}
                  />
                </div>
                <div className="ui input" style={{ width: '32%', margin: '0 2%' }}>
                  <AccountInput
                    placeholder="월"
                    name="month"
                    type="text"
                    list="monthlist"
                    value={selectMonth}
                    onChange={onChange}
                  />
                  <datalist id="monthlist">
                    {[...Array(12).keys()].map(item => (
                      <option key={`mm${item + 1}`} id={`mm${item + 1}`} value={`${item + 1}월`} />
                    ))}
                  </datalist>
                </div>
                <div className="ui input" style={{ width: '32%' }}>
                  <AccountInput
                    placeholder="일"
                    name="day"
                    type="number"
                    value={day}
                    onChange={onChange}
                  />
                </div>
              </div>
              <div className="field">
                <div className="ui fluid input">
                  <AccountInput
                    placeholder="성별"
                    name="gender"
                    type="text"
                    list="genderlist"
                    value={selectGender(gender)}
                    onChange={onChange}
                  />
                  <datalist id="genderlist">
                    {genderList.map(item => (
                      <option key={item} id={item} value={item} />
                    ))}
                  </datalist>
                </div>
              </div>
            </Form>
          </AccountWrap>
        </AccountForm>
        <Message negative hidden={!msg} style={{ marginBottom: '3rem' }}>
          <Message.Header>
            {msg}
          </Message.Header>
        </Message>
      </div>
    );
  }
}
