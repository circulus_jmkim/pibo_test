import React, { Component } from 'react';
import { Form, Checkbox, Message } from 'semantic-ui-react';
import styled from 'styled-components';

const TermsFormField = styled(Form.Field)`
  margin-bottom: 1rem !important;
`;

const TermsTA = styled.textarea`
  margin: 1rem 0 !important;
  -webkit-appearance: none;
  tap-highlight-color: rgba(255, 255, 255, 0);
  padding: 0.78571429em 1em;
  background: #fff;
  border: 1px solid rgba(8, 148, 215, 1) !important;
  outline: 0;
  color: rgba(0, 0, 0, 0.87);
  border-radius: 0 !important;
  -webkit-box-shadow: 0 0 0 0 transparent inset;
  box-shadow: 0 0 0 0 transparent inset;
  -webkit-transition: color 0.1s ease, border-color 0.1s ease;
  -o-transition: color 0.1s ease, border-color 0.1s ease;
  transition: color 0.1s ease, border-color 0.1s ease;
  font-size: 1em;
  line-height: 1.2857;
  resize: none !important;
`;

export default class Terms extends Component {
  render() {
    const {
      service, privacy, location, promotion, msg, onChange,
    } = this.props;
    return (
      <div>
        <Form>
          <Form.Group grouped>
            <TermsFormField>
              <Checkbox
                name="all"
                label="이용약관, 개인정보 수집 및 이용, 위치정보 이용약관(선택), 프로모션 안내 메일
                수신(선택)에 모두 동의합니다."
                onClick={onChange}
                checked={service && privacy && location && promotion}
              />
            </TermsFormField>
            <TermsFormField>
              <Checkbox
                label="서큘러스 이용약관 동의"
                name="service"
                checked={service}
                onClick={onChange}
              />
              <TermsTA rows="4" readonly />
            </TermsFormField>
            <TermsFormField>
              <Checkbox
                label="개인정보 수집 및 이용에 대한 안내"
                name="privacy"
                checked={privacy}
                onClick={onChange}
              />
              <TermsTA rows="4" readonly />
            </TermsFormField>
            <TermsFormField>
              <Checkbox
                label="위치정보 이용약관 동의"
                name="location"
                checked={location}
                onClick={onChange}
              />
              <TermsTA rows="4" readonly />
            </TermsFormField>
            <TermsFormField>
              <Checkbox
                label="이벤트 등 프로모션 알림 메일 수신"
                name="promotion"
                checked={promotion}
                onClick={onChange}
              />
              <TermsTA rows="4" readonly />
            </TermsFormField>
          </Form.Group>
        </Form>
        <Message negative hidden={!msg} style={{ marginBottom: '3rem' }}>
          <Message.Header>
            {msg}
          </Message.Header>
        </Message>
      </div>
    );
  }
}
