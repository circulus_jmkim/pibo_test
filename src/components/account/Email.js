import React, { Component } from 'react';
import { Form, Message, Button } from 'semantic-ui-react';
import { AccountForm, AccountWrap, AccountInput } from './AccountComponents';

export default class Email extends Component {
  render() {
    const {
      onChange, address, msg, hadSent, onSend,
    } = this.props;
    return (
      <div style={{ display: 'flex', flexFlow: 'column', alignItems: 'center' }}>
        <AccountForm>
          <AccountWrap>
            <Form size="large">
              <div className="field">
                <div className="ui fluid input">
                  <AccountInput
                    placeholder="이메일"
                    name="email"
                    type="email"
                    value={address}
                    onChange={onChange}
                  />
                </div>
              </div>
            </Form>
          </AccountWrap>
        </AccountForm>
        <Message negative hidden={!msg} style={{ marginBottom: '3rem' }}>
          <Message.Header>
            {msg}
          </Message.Header>
        </Message>
        <Button
          color="blue"
          disabled={!address || msg || hadSent}
          style={{ width: '70vw', margin: '1rem 0', borderRadius: '0' }}
          name="send"
          onClick={onSend}
        >
          인증메일 보내기
        </Button>
      </div>
    );
  }
}
