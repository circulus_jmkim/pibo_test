import React, { Component } from 'react';
import { Form, Message } from 'semantic-ui-react';
import { AccountForm, AccountWrap, AccountInput } from './AccountComponents';

export default class Account extends Component {
  render() {
    const {
      onChange, id, pw, rePw, msg,
    } = this.props;
    return (
      <div>
        <AccountForm>
          <AccountWrap>
            <Form size="large">
              <div className="field">
                <div className="ui fluid input">
                  <AccountInput
                    placeholder="아이디"
                    name="id"
                    type="text"
                    value={id}
                    onChange={onChange}
                  />
                </div>
              </div>
              <div className="field">
                <div className="ui fluid input">
                  <AccountInput
                    placeholder="비밀번호"
                    name="pw"
                    type="password"
                    value={pw}
                    onChange={onChange}
                  />
                </div>
              </div>
              <div className="field">
                <div className="ui fluid input">
                  <AccountInput
                    placeholder="비밀번호 확인"
                    name="rePw"
                    type="password"
                    value={rePw}
                    onChange={onChange}
                  />
                </div>
              </div>
            </Form>
          </AccountWrap>
        </AccountForm>
        <Message negative hidden={!msg} style={{ marginBottom: '3rem' }}>
          <Message.Header>
            {msg}
          </Message.Header>
        </Message>
      </div>
    );
  }
}
