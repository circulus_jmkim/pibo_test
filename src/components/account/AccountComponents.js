import styled from 'styled-components';

export const AccountForm = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column;
`;

export const AccountWrap = styled.div`
  margin: 0 !important;
  width: 80vw;
`;

export const AccountInput = styled.input`
  background: rgba(0, 0, 0, 0) !important;
  border: 0 !important;
  border-bottom: 1px solid rgba(8, 148, 215, 1) !important;
  border-radius: 0 !important;
  &::selection {
    color: #fff;
  }
`;
