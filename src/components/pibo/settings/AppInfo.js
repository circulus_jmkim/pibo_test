import React, { Component } from 'react';
import { Icon, Grid, Accordion } from 'semantic-ui-react';
import styled from 'styled-components';

const AccordionIcon = styled(Icon)`
  position: absolute;
  right: 1rem;
  transform: rotate(${props => (props.active ? 90 : 0)}deg);
`;

export default class AppInfo extends Component {
  state = { activeIndex: 0 };

  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? 0 : index;

    this.setState({ activeIndex: newIndex });
  };

  render() {
    const { curVer, latestVer } = this.props;
    const { activeIndex } = this.state;
    return (
      <Grid centered padded>
        <Grid.Row>
          <Grid.Column>
            <Accordion fluid styled exclusive={false} style={{ position: 'relative' }}>
              <Accordion.Title active>
                앱 버전
                <span
                  style={{
                    position: 'absolute',
                    right: '1rem',
                    color: 'rgba(8,148,215,1)',
                    textAlign: 'right',
                  }}
                >
                  {`현재 ${curVer} / 최신 ${latestVer}`}
                </span>
              </Accordion.Title>
              <Accordion.Title active={activeIndex === 1} index={1} onClick={this.handleClick}>
                정보 제공처
                <AccordionIcon name="angle right" active={activeIndex === 1} />
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 1}>
                <p>
                  There are many breeds of dogs. Each breed varies in size and temperament. Owners
                  often select a breed of dog that they find to be compatible with their own
                  lifestyle and desires from a companion.
                </p>
              </Accordion.Content>
              <Accordion.Title active={activeIndex === 2} index={2} onClick={this.handleClick}>
                오픈소스 라이선스
                <AccordionIcon name="angle right" active={activeIndex === 2} />
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 2}>
                <p>
                  There are many breeds of dogs. Each breed varies in size and temperament. Owners
                  often select a breed of dog that they find to be compatible with their own
                  lifestyle and desires from a companion.
                </p>
              </Accordion.Content>
              <Accordion.Title active={activeIndex === 3} index={3} onClick={this.handleClick}>
                서비스 이용약관
                <AccordionIcon name="angle right" active={activeIndex === 3} />
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 3}>
                <p>
                  There are many breeds of dogs. Each breed varies in size and temperament. Owners
                  often select a breed of dog that they find to be compatible with their own
                  lifestyle and desires from a companion.
                </p>
              </Accordion.Content>
              <Accordion.Title active={activeIndex === 4} index={4} onClick={this.handleClick}>
                위치정보 이용약관
                <AccordionIcon name="angle right" active={activeIndex === 4} />
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 4}>
                <p>
                  There are many breeds of dogs. Each breed varies in size and temperament. Owners
                  often select a breed of dog that they find to be compatible with their own
                  lifestyle and desires from a companion.
                </p>
              </Accordion.Content>
              <Accordion.Title active={activeIndex === 5} index={5} onClick={this.handleClick}>
                개인정보 처리방침
                <AccordionIcon name="angle right" active={activeIndex === 5} />
              </Accordion.Title>
              <Accordion.Content active={activeIndex === 5}>
                <p>
                  Three common ways for a prospective owner to acquire a dog is from pet shops,
                  private owners, or shelters.
                </p>
                <p>
                  A pet shop may be the most convenient way to buy a dog. Buying a dog from a
                  private owner allows you to assess the pedigree and upbringing of your dog before
                  choosing to take it home. Lastly, finding your dog from a shelter, helps give a
                  good home to a dog who may not find one so readily.
                </p>
              </Accordion.Content>
            </Accordion>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
