import React, { Component } from 'react';
import { Grid, Form } from 'semantic-ui-react';

export default class AppSendOpinion extends Component {
  render() {
    return (
      <Grid centered padded>
        <Grid.Row>
          <Grid.Column>
            <p>
접수된 의견은 모두 서비스 개선을 위해 소중히 활용되고 있습니다.
            </p>
            <p>
              <span>
                본 경로는 제안 및 의견 접수 전용 채널이며, 별도의 결과를 안내하지 않습니다.
              </span>
              {' '}
              답변이 필요한 그 외 문의는 고객센터를 통해 접수해주세요.
            </p>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Form>
              <Form.TextArea label="의견내용" style={{ height: '10rem' }} />
              <Form.Button color="blue" fluid>
                보내기
              </Form.Button>
            </Form>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
