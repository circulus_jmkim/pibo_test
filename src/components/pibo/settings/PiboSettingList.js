import React, { Component } from 'react';
import {
  Grid, Segment, Image, Icon, Menu,
} from 'semantic-ui-react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

export default class PiboSettingList extends Component {
  static defaultProps = {
    pibo: {
      name: '',
      wifi: '',
      battery: 0,
      temper: 0,
      volume: 0,
      align: 0,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      volume: 50,
      align: 50,
    };
  }

  componentDidMount() {
    const { pibo } = this.props;
    this.setState({
      volume: pibo.volume,
      align: pibo.align,
    });
  }

  getRobotTemperature = (temp, unit) => {
    if (unit === 'fahrenheit') {
      return `${(temp * 9) / 5 + 32} Ｆ`;
    }
    return `${temp} ℃`;
  };

  handleVolumeChange = (value) => {
    this.setState({
      volume: value,
    });
  };

  handleAlignChange = (value) => {
    this.setState({
      align: value,
    });
  };

  render() {
    const { pibo, handleItemClick, pid } = this.props;
    const {
      name, battery, temper, tempUnit,
    } = pibo;

    const { volume, align } = this.state;
    const volSettings = {
      defaultValue: volume,
      min: 0,
      max: 100,
      step: 1,
      onChange: this.handleVolumeChange,
      railStyle: { backgroundColor: 'rgba(209,211,212,.5)' },
      trackStyle: { backgroundColor: 'rgba(3,191,215,1)' },
      handleStyle: {
        backgroundColor: '#03BFD7',
        border: '0',
      },
    };

    const alignSettings = {
      defaultValue: align,
      min: 1,
      max: 100,
      step: 1,
      onChange: this.handleAlignChange,
      railStyle: { backgroundColor: 'rgba(209,211,212,.5)' },
      trackStyle: { backgroundColor: 'rgba(3,191,215,1)' },
      handleStyle: {
        backgroundColor: '#03BFD7',
        border: '0',
      },
    };

    return (
      <Grid centered padded>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Grid padded centered columns={16}>
                <Grid.Row>
                  <Grid.Column verticalAlign="middle" width={8}>
                    <Image src="../image/img_pibo.png" />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row textAlign="left">
                  <Grid.Column width={16} verticalAlign="middle">
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right">
                          호출명
                        </Grid.Column>
                        <Grid.Column width={12}>
                          {name}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right">
                          시리얼 넘버
                        </Grid.Column>
                        <Grid.Column width={12}>
                          {pid}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right">
                          배터리
                        </Grid.Column>
                        <Grid.Column width={12}>
                          {battery}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right">
                          온도
                        </Grid.Column>
                        <Grid.Column width={12}>
                          {this.getRobotTemperature(temper, tempUnit)}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right">
                          사운드
                        </Grid.Column>
                        <Grid.Column width={1}>
                          <Icon name="volume off" color="teal" />
                        </Grid.Column>
                        <Grid.Column width={9}>
                          <Slider {...volSettings} />
                        </Grid.Column>
                        <Grid.Column width={1}>
                          {volume}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right">
                          기기정렬
                        </Grid.Column>
                        <Grid.Column width={1}>
                          <Icon name="cogs" color="teal" />
                        </Grid.Column>
                        <Grid.Column width={9}>
                          <Slider {...alignSettings} />
                        </Grid.Column>
                        <Grid.Column width={1}>
                          {align}
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Menu vertical fluid>
              <Menu.Item name="name" onClick={handleItemClick}>
                <Icon name="angle right" />
                호출명 설정
              </Menu.Item>
              <Menu.Item name="tempUnit" onClick={handleItemClick}>
                <Icon name="angle right" />
                온도 단위 설정
              </Menu.Item>
              {
              //   <Menu.Item name="servo" onClick={handleItemClick}>
              //     <Icon name="angle right" />
              //     기기정렬 상세 설정
              //   </Menu.Item>
              }
              <Menu.Item name="wifi" onClick={handleItemClick}>
                <Icon name="angle right" />
                와이파이 설정
              </Menu.Item>
              <Menu.Item name="geo" onClick={handleItemClick}>
                <Icon name="angle right" />
                위치 설정
              </Menu.Item>
              <Menu.Item name="locale" onClick={handleItemClick}>
                <Icon name="angle right" />
                언어 설정
              </Menu.Item>
              <Menu.Item name="instruction" onClick={handleItemClick}>
                <Icon name="angle right" />
                사용 안내
              </Menu.Item>
              <Menu.Item name="initialize" onClick={handleItemClick}>
                설정 초기화
              </Menu.Item>
              <Menu.Item name="disconnect" onClick={handleItemClick}>
                연결 해제
              </Menu.Item>
            </Menu>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
