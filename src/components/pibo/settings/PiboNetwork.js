import React, { Component } from 'react';
import {
  Grid, Header, Icon, Divider, Segment, Button, List,
} from 'semantic-ui-react';
import styled from 'styled-components';

const ConnectWrap = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  min-height: 100vh;
  background-color: rgba(251, 251, 251, 1);
  padding: 2.75rem 1rem 0rem;
`;

export default class PiboNetwork extends Component {
  static defaultProps = {
    ap: { essid: '', encrypted: false, addres: '' },
    aplist: [],
    onChange: () => {},
    onReSearch: () => {},
    onPrevClick: () => {},
    isInit: false,
  };

  render() {
    const {
      ap, aplist, onChange, onReSearch, onPrevClick, isInit, loading,
    } = this.props;
    const { essid, encrypted, address } = ap;
    return (
      <ConnectWrap>
        {loading && (
          <Header style={{ paddingTop: '2rem', textAlign: 'center' }}>
            와이파이를 찾는 중입니다.
          </Header>
        )}
        {!loading
          && aplist.length === 0 && (
            <div>
              <Header style={{ paddingTop: '2rem' }}>
와이파이를 찾지 못했습니다.
              </Header>
              <Button
                fluid
                name="prev"
                size="large"
                style={{
                  background: 'rgba(209, 211, 212, 1)',
                  color: 'rgba(86,91,91,1)',
                  borderRadius: '0px',
                }}
                onClick={onReSearch}
              >
                Wi-Fi 다시 찾기
              </Button>
            </div>
        )}
        {!loading
          && aplist.length > 0 && (
            <div>
              <Header style={{ paddingTop: '2rem' }}>
연결할 와이파이를 선택해주세요.
              </Header>
              <Segment>
                <Grid centered>
                  <Grid.Row>
                    <Grid.Column>
                      <Header size="small">
연결 된 Wi-Fi
                      </Header>
                    </Grid.Column>
                  </Grid.Row>
                  {address && (
                    <Grid.Row>
                      <Grid.Column>
                        <List divided relaxed>
                          <List.Item as="a">
                            <List.Content
                              style={{
                                display: 'flex',
                                flexFlow: 'row-reverse',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}
                            >
                              {encrypted && <Icon name="lock" />}
                              <Icon name="wifi" />
                              {essid}
                            </List.Content>
                          </List.Item>
                        </List>
                      </Grid.Column>
                    </Grid.Row>
                  )}
                  {!address && (
                    <Grid.Row>
                      <Grid.Column>
연결 된 Wi-Fi가 없습니다.
                      </Grid.Column>
                    </Grid.Row>
                  )}
                  <Divider fitted />
                  <Grid.Row>
                    <Grid.Column>
                      <Header size="small">
연결 했던 Wi-Fi
                      </Header>
                    </Grid.Column>
                  </Grid.Row>
                  <Divider fitted />
                  <Grid.Row>
                    <Grid.Column>
                      <Header size="small">
새로운 Wi-Fi
                      </Header>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row centered>
                    <Grid.Column>
                      <List divided relaxed>
                        {aplist.map((apitem, idx) => (
                          <List.Item
                            as="a"
                            value={apitem.address}
                            name={`apitem_${idx}`}
                            key={apitem.address}
                            onClick={onChange}
                          >
                            <List.Content
                              verticalAlign="middle"
                              style={{
                                display: 'flex',
                                flexFlow: 'row-reverse',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                height: '45px',
                              }}
                            >
                              <div>
                                {apitem.encrypted && <Icon name="lock" />}
                                <Icon name="wifi" />
                              </div>
                              {apitem.essid}
                            </List.Content>
                          </List.Item>
                        ))}
                      </List>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Segment>
              <Button
                fluid
                name="prev"
                size="large"
                style={{
                  background: 'rgba(209, 211, 212, 1)',
                  color: 'rgba(86,91,91,1)',
                  borderRadius: '0px',
                }}
                onClick={onReSearch}
              >
                Wi-Fi 다시 찾기
              </Button>
              {isInit && (
                <Button
                  fluid
                  name="prev"
                  size="large"
                  style={{
                    background: 'rgba(209, 211, 212, 1)',
                    color: '#fff',
                    borderRadius: '0px',
                    marginTop: '1rem',
                  }}
                  onClick={onPrevClick}
                >
                  취소
                </Button>
              )}
            </div>
        )}
      </ConnectWrap>
    );
  }
}
