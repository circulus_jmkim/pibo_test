import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import InstructionList from './InstructionList';

export default class PiboInstruction extends Component {
  static defaultProps = {
    list: [],
    results: [],
  };

  render() {
    const { list, results } = this.props;
    return (
      <Grid centered padded>
        <InstructionList
          list={list}
          results={results}
          handleDeleteClick={this.handleDeleteClick}
          handleUseClick={this.handleUseClick}
          addSearchChange={this.addSearchChange}
        />
      </Grid>
    );
  }
}
