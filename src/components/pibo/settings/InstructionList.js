import React, { Component } from 'react';
import {
  Grid, Image, Header, Radio, Search, Icon, Accordion,
} from 'semantic-ui-react';
import styled from 'styled-components';
import _ from 'lodash';

const searchBg = styled.div`
  background-color: rgba(8, 148, 215, 1);
  width: 100vw;
  height: auto;
`;

export default class InstructionList extends Component {
  state = {
    isLoading: false,
    results: [],
    list: [
      {
        title: '파이보, 오늘 날씨 알려줘',
        content: [
          '파이보 오늘 날씨 어때?',
          '파이보 내일 날씨 알려줘',
          '파이보 을지로3가 날씨 알려줘',
          '파이보 오늘 덥니?',
        ],
      },
      {
        title: '파이보, 오늘 일정 알려줘',
        content: [
          '파이보 오늘 날씨 어때?',
          '파이보 내일 날씨 알려줘',
          '파이보 을지로3가 날씨 알려줘',
          '파이보 오늘 덥니?',
        ],
      },
      {
        title: '파이보, 음악 틀어줘',
        content: [
          '파이보 오늘 날씨 어때?',
          '파이보 내일 날씨 알려줘',
          '파이보 을지로3가 날씨 알려줘',
          '파이보 오늘 덥니?',
        ],
      },
      {
        title: '파이보, 오늘 일기 기록해줘',
        content: [
          '파이보 오늘 날씨 어때?',
          '파이보 내일 날씨 알려줘',
          '파이보 을지로3가 날씨 알려줘',
          '파이보 오늘 덥니?',
        ],
      },
    ],
    value: '',
  };

  componentWillMount() {
    // const { list } = this.props;
    // this.setState({ list });
  }

  componentWillReceiveProps(nextProps) {
    const { list, results } = this.props;
    if (list !== nextProps.list) {
      this.setState({ list: nextProps.list });
    }
    if (results !== nextProps.results) {
      const { value } = this.state;
      if (value) {
        this.setState({ list: nextProps.results });
      } else {
        this.setState({ list });
      }
    }
  }

  handleUseToggleClick = (e, { id, checked }) => {
    const { handleUseClick } = this.props;
    handleUseClick(id, checked);
  };

  handleSearchResultClick = (e, { result }) => {
    this.setState({ list: [result] });
  };

  resetComponent = () => {
    const { list } = this.props;
    this.setState({ isLoading: false, list, value: '' });
  };

  handleSearchRemove = () => {
    this.resetComponent();
  };

  handleSearchResult = () => {
    const { resultSelect } = this.props;
    resultSelect();
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ value });
    setTimeout(() => {
      this.addSearchChange();
    }, 500);
  };

  addSearchChange = () => {
    const { isLoading, value } = this.state;
    if (!isLoading) {
      const { addSearchChange } = this.props;
      addSearchChange(value);
    }
  };

  render() {
    const { list, isLoading, value } = this.state;

    const panels = _.times(3, i => ({
      key: `panel-${i}`,
      title: list[i].title,
      content: list[i].content.map((line, idx) => (
        <span key={`content-${idx}`}>
          {line}
          <br />
        </span>
      )),
    }));
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <Accordion
              defaultActiveIndex={[0]}
              panels={panels}
              exclusive={false}
              styled
              fluid
              style={{ position: 'relative' }}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
