import React, { Component } from 'react';
import {
  Card, Menu, Icon, Grid, Segment, Image, Button,
} from 'semantic-ui-react';
import * as moment from 'moment';

export default class UserSettingsCard extends Component {
  static defaultProps = {
    user: {
      nickName: '',
      email: '',
      birthDate: 0,
      interest: [],
    },
  };

  render() {
    const { user, handleItemClick } = this.props;
    const {
      nickName, email, birthDate, interest,
    } = user;
    const mm = moment(birthDate);

    return (
      <Grid centered padded>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Grid padded>
                <Grid.Row>
                  <Grid.Column verticalAlign="middle" textAlign="center">
                    <Image avatar src="../image/img_user.png" size="small" />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column verticalAlign="middle">
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={5} textAlign="right">
                          닉네임
                        </Grid.Column>
                        <Grid.Column width={11}>
                          {nickName}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={5} textAlign="right">
                          메일
                        </Grid.Column>
                        <Grid.Column width={11}>
                          {email}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={5} textAlign="right">
                          생년월일
                        </Grid.Column>
                        <Grid.Column width={11}>
                          {mm.format('YYYY-MM-DD')}
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={5} textAlign="right">
                          관심사
                        </Grid.Column>
                        <Grid.Column width={11}>
                          {interest
                            && interest.map((item, idx) => (
                              <Button size="mini" color="teal" key={idx} compact>
                                {item}
                              </Button>
                            ))}
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Card fluid>
              <Menu vertical fluid>
                <Menu.Item name="update" onClick={handleItemClick}>
                  <Icon name="angle right" />
내 정보 수정
                </Menu.Item>
                <Menu.Item name="logout" onClick={handleItemClick}>
                  로그아웃
                </Menu.Item>
                <Menu.Item name="withdraw" onClick={handleItemClick}>
                  <Icon name="angle right" />
                  탈퇴
                </Menu.Item>
              </Menu>
            </Card>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row />
        <Grid.Row />
      </Grid>
    );
  }
}
