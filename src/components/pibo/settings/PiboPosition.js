import React, { Component } from 'react';
import Leaflet from 'leaflet';
import {
  Map, TileLayer, Marker, Popup,
} from 'react-leaflet';
import {
  Grid, Segment, Button, Icon,
} from 'semantic-ui-react';
import NodeGeocoder from 'node-geocoder';

Leaflet.Icon.Default.imagePath = '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/';

export default class PiboPosition extends Component {
  static defaultProps = {
    value: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      value: [],
      addressStr: '',
      results: [],
      addLoading: false,
    };
  }

  componentDidMount() {
    const { value } = this.props;
    if (value && value.length === 2) {
      console.log(value);
      this.getAddress(value[0], value[1]);
    }
  }

  // onChange = async (e, { value }) => {
  //   const { ...state } = this.state;
  //   this.setState({
  //     ...state,
  //     loading: false,
  //     addressStr: value,
  //   });

  //   if (value.length > 0) {
  //     setTimeout(async () => {
  //       const { mapPosition, ...rest } = this.state;
  //       const geocoder = NodeGeocoder({
  //         provider: 'openstreetmap',
  //         httpAdapter: 'https',
  //         formatter: 'jsonv2',
  //         addressdetails: 1,
  //       });
  //       const result = await geocoder.geocode(value);
  //       const results = result.reduce((acc, cur) => {
  //         const { latitude, longitude, formattedAddress } = cur;
  //         const userLat = mapPosition[0];
  //         const userLon = mapPosition[1];
  //         const gap = Math.abs(userLat - latitude) + Math.abs(userLon - longitude);
  //         acc.push({
  //           lat: latitude, lng: longitude, address: formattedAddress, gap,
  //         });
  //         return acc;
  //       }, []).sort((a, b) => a.gap - b.gap);
  //       console.log(results);
  //       this.setState({
  //         ...rest,
  //         results,
  //       });
  //     }, 1000);
  //   }
  // }

  onMapMouseUp = (e) => {
    const { ...state } = this.state;
    this.setState({
      ...state,
      addLoading: true,
    });
    this.getAddress(e.latlng.lat, e.latlng.lng);
  };

  getAddress = async (lat, lng) => {
    console.log(lat, lng);
    const geocoder = NodeGeocoder({
      provider: 'openstreetmap',
      httpAdapter: 'https',
      formatter: 'jsonv2',
      addressdetails: 1,
    });
    const result = await geocoder.reverse({ lat, lon: lng });
    const { address } = result.raw;
    const {
      country, city, town, road,
    } = address;
    const formattedAddress = `${country} ${city} ${town} ${road}`;
    // const { addressStr, loading, ...rest } = this.state;
    const { handleChange } = this.props;
    handleChange([lat, lng]);
    this.setState({
      addLoading: false,
      value: [lat, lng],
      addressStr: formattedAddress,
    });
  };

  render() {
    const { loading } = this.props;
    const { addressStr, addLoading, value } = this.state;
    const mapZoom = 16;

    return (
      <Grid centered padded>
        <Grid.Row columns={15}>
          <Grid.Column width={15}>
            <div
              style={{
                height: '36vh',
                flex: '1 0',
                display: 'flex',
                position: 'relative',
              }}
            >
              {value.length > 1 && (
                <Map
                  center={value}
                  zoom={mapZoom}
                  style={{ flex: '1 0' }}
                  onmouseup={this.onMapMouseUp}
                >
                  <TileLayer
                    url="https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png"
                    attribution="&copy; <a href=&quot;http://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a>, &copy; <a href=&quot;https://carto.com/attribution&quot;>CARTO</a>"
                  />
                  <Marker position={value}>
                    <Popup>
                      <span>
                        {addressStr}
                      </span>
                    </Popup>
                  </Marker>
                </Map>
              )}
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={15}>
          <Grid.Column width={15}>
            <Segment loading={addLoading}>
              {addressStr}
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={15}>
          <Grid.Column width={15}>
            <Button fluid disabled={loading} onClick={this.getCurrentPosition}>
              <Icon name="crosshairs" />
              현재 위치로 설정
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
