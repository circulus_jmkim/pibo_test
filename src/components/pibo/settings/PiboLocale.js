import React, { Component } from 'react';
import {
  Grid, Menu, Form, Radio,
} from 'semantic-ui-react';

export default class PiboLocale extends Component {
  static defaultProps = {
    list: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

  componentDidMount() {
    const { value } = this.props;
    console.log(value);
    this.setState({
      value,
    });
  }

  handleChange = (e, { value }) => {
    const { handleChange } = this.props;
    handleChange(value);
    this.setState({ value });
  };

  render() {
    const { list } = this.props;
    const { value } = this.state;
    return (
      <Grid centered padded>
        <Grid.Row>
          <Grid.Column>
            <Menu vertical fluid>
              <Form>
                {list.length > 0
                  && list.map((item, idx) => (
                    <Menu.Item key={idx} value={item} onClick={this.handleChange}>
                      <Form.Field>
                        <Radio
                          label={item}
                          name="radioGroup"
                          value={item}
                          checked={value === item}
                          onChange={this.handleChange}
                        />
                      </Form.Field>
                    </Menu.Item>
                  ))}
              </Form>
            </Menu>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
