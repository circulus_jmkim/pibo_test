import React, { Component } from 'react';
import {
  Grid, Segment, Radio, Form, TextArea, Menu, Header,
} from 'semantic-ui-react';

const agreement = '다양한 (주)서큘러스 서비스에 대한 액세스를 제공하는 (주)서큘러스 계정을 삭제하려고 합니다. 더 이상 (주)서큘러스 서비스를 이용할 수 없으며 계정과 데이터가 삭제됩니다. 또한 해당 계정을(를) 사용하는 (주)서큘러스 외부의 서비스에 액세스하지 못할 수도 있습니다. 예를 들어, 이 이메일 주소를 은행 계좌에 대한 복구 이메일로 사용 중이라면 은행 계좌 비밀번호를 재설정하지 못할 수 있습니다. 계속하는 경우, (주)서큘러스 외부에서 사용하는 모든 곳에서 이메일 주소를 업데이트해야 합니다.';
export default class UserWithdraw extends Component {
  state = {
    checked: false,
  };

  handleRadioToggle = () => {
    const { checked } = this.state;
    this.setState({ checked: !checked });
  };

  handleWithdrawClick = () => {
    const { checked } = this.state;
    const { handleWithdrawClick } = this.props;
    if (checked) handleWithdrawClick();
  };

  render() {
    const { checked } = this.state;

    return (
      <Grid centered padded>
        <Grid.Row verticalAlign="bottom">
          <Grid.Column>
            <Header as="h4">
탈퇴하기 전에 꼭 확인하세요.
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Form>
                <Form.Field control={TextArea} rows={7} value={agreement} readOnly />
              </Form>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Radio
                label="계정을 삭제하고 탈퇴합니다."
                onChange={this.handleRadioToggle}
                checked={checked}
              />
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Menu fixed="bottom" widths={1} color="blue" inverted>
          <Menu.Item name="확인" disabled={!checked} onClick={this.handleWithdrawClick} />
        </Menu>
      </Grid>
    );
  }
}
