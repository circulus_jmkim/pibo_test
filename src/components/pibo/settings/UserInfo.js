import React, { Component } from 'react';
import {
  Grid, Image, Segment, Button, Input,
} from 'semantic-ui-react';
import * as moment from 'moment';

const emailReg = /([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}/igm;
const nameReg = /([a-zA-Z|가-힣]+)/g;
const nickReg = /([a-zA-Z|가-힣|0-9_-]+)/g;
const dateReg = /^(19|20)\d{2}[-](0?[1-9]|1[0-2])[-](0?[1-9]|[12]\d|3[01])$/g;

export default class UserInfo extends Component {
  state = {
    lastName: '',
    firstName: '',
    nickName: '',
    email: '',
    birthDate: 0,
    interest: [],
    interestList: [],
  }

  componentDidMount() {
    const {
      interest, interestList, birthDate, handleChange, ...rest
    } = this.props;
    const checkedInterests = this.checkInterest(interest);
    const mm = moment(birthDate);
    this.setState({
      interestList: checkedInterests,
      birthDate: mm.format('YYYY-MM-DD'),
      interest,
      ...rest,
    });

    // handleChange({ item: 'interest', value: interest });
  }

  handleChange= (e, { name, value }) => {
    const { handleChange } = this.props;

    this.setState({
      [name]: value,
    });

    const birthDate = new Date(value).valueOf();

    setTimeout(() => {
      switch (name) {
        case 'lastName':
          if (value !== value.match(nameReg).join('')) { return this.setState({ [name]: value.match(nameReg)[0] }); }
          handleChange({ item: name, value });
          break;
        case 'firstName':
          if (value !== value.match(nameReg).join('')) { return this.setState({ [name]: value.match(nameReg)[0] }); }
          handleChange({ item: name, value });
          break;
        case 'nickName':
          if (value !== value.match(nickReg).join('')) { return this.setState({ [name]: value.match(nickReg)[0] }); }
          handleChange({ item: name, value });
          break;
        case 'birthDate':
          if (birthDate > 0 && value.match(dateReg)) {
            this.setState({ [name]: value });
            return handleChange({ item: name, value: new Date(value).valueOf() });
          }
          break;
        case 'email':
          if (value.match(emailReg)) { return this.setState({ [name]: value }); }
          handleChange({ item: name, value });
          break;
        default:
          break;
      }
    }, 300);
  }

  handleBtnClick = (e, { inverted, value }) => {
    const { interest } = this.state;
    const { handleChange } = this.props;
    let newOne;
    if (inverted) { newOne = interest.concat([value]); }
    if (!inverted) { newOne = interest.filter(item => item !== value); }
    const checkedInterests = this.checkInterest(newOne);
    this.setState({
      interest: newOne,
      interestList: checkedInterests,
    });
    handleChange({ item: 'interest', value: newOne });
  }

  checkInterest = (interest) => {
    const { interestList } = this.props;
    const newList = Array.from(interestList, item => item);
    if (!interest) return newList;
    return interest.reduce((acc, cur) => {
      const idx = acc.findIndex(el => el.name === cur);
      if (acc[idx]) acc[idx] = { name: cur, selected: true };
      return acc;
    }, newList);
  }

  render() {
    const {
      nickName, email, birthDate, interestList, lastName, firstName,
    } = this.state;

    return (
      <Grid centered padded>
        <Grid.Row>
          <Grid.Column>
            <Segment>
              <Grid padded>
                <Grid.Row>
                  <Grid.Column verticalAlign="middle" textAlign="center">
                    <Image avatar src="../../image/img_user.png" size="small" />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column verticalAlign="middle">
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right" verticalAlign="middle">
                          성
                        </Grid.Column>
                        <Grid.Column width={11}>
                          <Input type="text" name="lastName" value={lastName} onChange={this.handleChange} />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right" verticalAlign="middle">
                          이름
                        </Grid.Column>
                        <Grid.Column width={11}>
                          <Input type="text" name="firstName" value={firstName} onChange={this.handleChange} />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right" verticalAlign="middle">
                          닉네임
                        </Grid.Column>
                        <Grid.Column width={11}>
                          <Input type="text" name="nickName" placeholder={`파이보가 ${firstName}님을 부를 닉네임을 입력하세요.`} value={nickName} onChange={this.handleChange} />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right" verticalAlign="middle">
                          이메일
                        </Grid.Column>
                        <Grid.Column width={11}>
                          <Input type="email" name="email" value={email} onChange={this.handleChange} />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right" verticalAlign="middle">
                          생년월일
                        </Grid.Column>
                        <Grid.Column width={11}>
                          <Input type="date" name="birthDate" value={birthDate} onChange={this.handleChange} />
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column width={4} textAlign="right">
                          관심사
                        </Grid.Column>
                        <Grid.Column width={11}>
                          {interestList.map((item, idx) => (
                            <Button key={idx} compact size="tiny" color="teal" value={item.name} inverted={!item.selected} onClick={this.handleBtnClick}>
                              {item.name}
                            </Button>
                          ))}
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
