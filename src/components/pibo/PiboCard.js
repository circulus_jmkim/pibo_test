import React, { Component } from 'react';
import {
  Grid, Image, Header, Segment, Icon, Menu,
} from 'semantic-ui-react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

export default class PiboCard extends Component {
  static defaultProps = {
    pibo: {
      name: '',
      wifi: '',
      battery: 0,
      temper: 0,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      volume: 0,
    };
  }

  getRobotTemperature = (temp = 0, unit) => {
    if (unit === 'fahrenheit') {
      return `${(temp * 9) / 5 + 32} Ｆ`;
    }
    return `${temp} ℃`;
  };

  // componentDidMount() {
  //   const { pibo } = this.props;
  //   this.setState({
  //     volume: pibo.volume,
  //   });
  // }

  handleValueChange = (value) => {
    console.log(value);
    this.setState({
      volume: value,
    });
  };

  render() {
    const { pibo, onClickCog } = this.props;
    const {
      name, wifi, battery, temper, tempUnit,
    } = pibo;

    const { volume } = this.state;

    const volSettings = {
      defaultValue: volume,
      min: 0,
      max: 100,
      step: 1,
      onChange: this.handleValueChange,
      railStyle: { backgroundColor: 'rgba(209,211,212,.5)' },
      trackStyle: { backgroundColor: 'rgba(3,191,215,1)' },
      handleStyle: {
        backgroundColor: '#03BFD7',
        border: '0',
      },
    };

    return (
      <Grid.Row>
        <Grid.Column>
          <Header>
            나의 파이보
            <Menu compact secondary>
              <Menu.Item icon="cog" float="right" onClick={onClickCog} fitted />
            </Menu>
          </Header>
          <Segment>
            <Grid padded>
              <Grid.Row>
                <Grid.Column width={7} verticalAlign="middle">
                  <Image src="./image/img_pibo.png" />
                </Grid.Column>
                <Grid.Column width={9} verticalAlign="middle">
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={1}>
                        <Icon name="quote left" color="teal" />
                      </Grid.Column>
                      <Grid.Column width={8}>
                        {name}
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column width={1}>
                        <Icon name="wifi" color="teal" />
                      </Grid.Column>
                      <Grid.Column width={8}>
                        {wifi}
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column width={1}>
                        <Icon name="battery half" color="teal" />
                      </Grid.Column>
                      <Grid.Column width={8}>
                        {battery}
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column width={1}>
                        <Icon name="thermometer half" color="teal" />
                      </Grid.Column>
                      <Grid.Column width={8}>
                        {this.getRobotTemperature(temper, tempUnit)}
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column width={1}>
                        <Icon name="volume off" color="teal" />
                      </Grid.Column>
                      <Grid.Column width={10}>
                        <Slider {...volSettings} />
                      </Grid.Column>
                      <Grid.Column width={1}>
                        {volume}
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    );
  }
}
