import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Grid, Header, Segment, Image,
} from 'semantic-ui-react';

export default class PiboFavoriteCard extends Component {
  static defaultProps = {
    bots: [],
  };

  render() {
    const { bots, loading } = this.props;

    return (
      <Grid.Row>
        <Grid.Column>
          <Header>
즐겨찾는 bot
          </Header>
          <Segment loading={loading}>
            <Grid>
              <Grid.Row columns={4}>
                {bots.map(({ projectId, image }, idx) => (
                  <Grid.Column key={idx} style={{ overflow: 'hidden' }}>
                    <Link to={`/bots/${projectId}`}>
                      <Image
                        src="../image/image.png"
                        style={{ width: '60px', height: '60px', margin: '0 10px' }}
                      />
                      <p
                        style={{
                          marginTop: '5px',
                          width: '80px',
                          fontSize: '0.8rem',
                          fontWeight: '400',
                          textOverflow: 'ellipsis',
                          whiteSpace: 'nowrap',
                          overflow: 'hidden',
                        }}
                      >
                        {projectId}
                      </p>
                    </Link>
                  </Grid.Column>
                ))}
              </Grid.Row>
            </Grid>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    );
  }
}
