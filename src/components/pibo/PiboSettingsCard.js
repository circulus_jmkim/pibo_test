import React, { Component } from 'react';
import {
  Card, Menu, Header, Icon, Grid,
} from 'semantic-ui-react';

export default class PiboSettingsCard extends Component {
  render() {
    const { onClickCog } = this.props;
    return (
      <Grid.Row>
        <Grid.Column>
          <Header>
설정
          </Header>
          <Card fluid>
            <Menu vertical fluid>
              <Menu.Item name="locale" onClick={onClickCog}>
                <Icon name="angle right" />
                언어 설정
              </Menu.Item>
              <Menu.Item name="info" onClick={onClickCog}>
                <Icon name="angle right" />
앱 정보
              </Menu.Item>
              <Menu.Item name="opinion" onClick={onClickCog}>
                <Icon name="angle right" />
                의견 보내기
              </Menu.Item>
              <Menu.Item name="cs" onClick={onClickCog}>
                <Icon name="angle right" />
                고객센터
              </Menu.Item>
            </Menu>
          </Card>
        </Grid.Column>
      </Grid.Row>
    );
  }
}
