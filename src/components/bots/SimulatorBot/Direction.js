import React, { Component } from 'react';
import { Grid, Image } from 'semantic-ui-react';
import styled from 'styled-components';

export default class Direction extends Component {
  render() {
    const { direction, enable, onClick } = this.props;
    const directionStyle = {
      down: 'margin: 0 0 0 30px',
      left: 'margin: 30px 0 0 0',
      right: 'margin: 0 0 30px 0',
      up: 'margin: 0 30px 0 0',
    };

    const StyledImage = styled(Image)`
    margin: auto;
    opacity: 0.9;
    -ms-transform: rotate(-10deg); /* IE 9 */
    -webkit-transform: rotate(-10deg); /* Safari 3-8 */
    transform: rotate(-10deg);
  }
  `;

    const StyledGridColumn = styled(Grid.Column)`
    ${directionStyle[direction]} !important;
  }
  `;

    return (
      <StyledGridColumn onClick={onClick}>
        {enable ? (
          <StyledImage src={`/image/bots/simulator/${direction}-arrow.png`} />
        ) : <StyledImage src={`/image/bots/simulator/${direction}-arrow-disable.png`} />}
      </StyledGridColumn>
    );
  }
}
