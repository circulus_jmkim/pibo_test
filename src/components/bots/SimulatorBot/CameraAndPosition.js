import React, { Component } from 'react';
import {
  Grid, Button, Icon, Modal,
} from 'semantic-ui-react';

export default class CameraAndPosition extends Component {
  constructor() {
    super();
    this.state = { cameraModal: false };
  }

  cameraHandler = () => {
    console.log('cameraHandler');
    this.setState({ cameraModal: true });
  }

  closeCameraModal = () => this.setState({ cameraModal: false });

  render() {
    const { cameraModal } = this.state;
    const { position, changePosition } = this.props;

    return (
      <Grid style={{ marginTop: '20px' }}>
        <Grid.Column textAlign="center" floated="left" width={5} style={{ marginLeft: '20px' }}>
          <Button
            icon
            size="small"
            color="teal"
            onClick={this.cameraHandler}
          >
            <Icon name="camera" />
          </Button>
        </Grid.Column>
        <Modal size="mini" open={cameraModal} onClose={this.closeCameraModal}>
          <Modal.Header>촬영 완료</Modal.Header>
          <Modal.Content>
            <p>Photo앱에서 사진을 확인하세요.</p>
          </Modal.Content>
          <Modal.Actions>
            <Button size="tiny" color="teal" content="확인" onClick={this.closeCameraModal} />
          </Modal.Actions>
        </Modal>
        <Grid.Column floated="right" width={5} style={{ marginRight: '20px' }}>
          <Button
            icon
            labelPosition="right"
            size="small"
            color="teal"
            onClick={changePosition}
          >
            {position ? '앞' : '뒤'}
            <Icon name="exchange" />
          </Button>
        </Grid.Column>
      </Grid>
    );
  }
}
