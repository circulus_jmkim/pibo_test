import React, { Component } from 'react';
import { Grid, Button, Dropdown } from 'semantic-ui-react';

export default class Dance extends Component {
  handleChange = (e, { name, value }) => {
    const { pibo } = this.props;
    console.log('dance~', value);
    this.setState({ [name]: value });
  };

  render() {
    return (
      <Grid.Column textAlign="center" floated="left" width={6} style={{ marginLeft: '20px' }}>
        <Button.Group color="teal" fluid>
          <Dropdown
            name="dance"
            button
            color="teal"
            options={[{ key: '1', text: '춤1', value: '1' }, { key: '2', text: '춤2', value: '2' }]}
            text="춤추기"
            fluid
            onChange={this.handleChange}
          />
        </Button.Group>
      </Grid.Column>
    );
  }
}
