import React, { Component } from 'react';
import { Grid } from 'semantic-ui-react';
import styled from 'styled-components';

import Direction from './Direction';

export default class Move extends Component {
  constructor() {
    super();
    this.state = {
      upEnable: false, leftEnable: false, rightEnable: false, downEnable: false,
    };
  }

  onControllerHandler = (direction) => {
    const { pibo } = this.props;
    const { [`${direction}Enable`]: selectedButtonState } = this.state;
    const { position } = this.props;
    const buttonState = {
      upEnable: false, leftEnable: false, rightEnable: false, downEnable: false,
    };

    buttonState[`${direction}Enable`] = !selectedButtonState;
    this.setState(buttonState);
    console.log('all stop~');
    let move = '';
    if (buttonState[`${direction}Enable`]) {
      switch (direction) {
        case 'down': move = position ? 'forward_f' : 'backward_f'; break;
        case 'up': move = position ? 'back' : 'forward'; break;
        case 'left': move = position ? 'right_f' : 'left_f'; break;
        case 'right': move = position ? 'left_f' : 'right_f'; break;
        default:
          break;
      }
      pibo.action(move, { cycle: 1 });
    }
  }

  render() {
    const { position } = this.props;
    const {
      upEnable, leftEnable, rightEnable, downEnable,
    } = this.state;

    const StyledGrid = styled(Grid)`
      padding: 0 !important;
      background: linear-gradient(rgba(255,255,255,0.8), rgba(255,255,255,0.8)), url("/image/bots/simulator/pibo-${position ? 'front' : 'back'}-side.png") center center no-repeat;
      background-repeat: no-repeat;
      background-size: contain;
    }
    `;

    return (
      <StyledGrid
        columns={4}
        centered
      >
        <Grid.Row>
          <Direction direction="up" enable={upEnable} onClick={() => this.onControllerHandler('up')} />
        </Grid.Row>
        <Grid.Row>
          <Direction direction="left" enable={leftEnable} onClick={() => this.onControllerHandler('left')} />
          <Grid.Column>
          </Grid.Column>
          <Direction direction="right" enable={rightEnable} onClick={() => this.onControllerHandler('right')} />
        </Grid.Row>
        <Grid.Row>
          <Direction direction="down" enable={downEnable} onClick={() => this.onControllerHandler('down')} />
        </Grid.Row>
      </StyledGrid>
    );
  }
}
