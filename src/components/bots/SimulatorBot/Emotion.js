import React, { Component } from 'react';
import { Grid, Button, Dropdown } from 'semantic-ui-react';

export default class Emotion extends Component {
  handleChange = (e, { name, value }) => {
    console.log('emotion~');
    this.setState({ [name]: value });
  };

  render() {
    return (
      <Grid.Column floated="right" width={7} style={{ marginRight: '20px' }}>
        <Button.Group color="teal" fluid>
          <Dropdown
            name="emotion"
            button
            color="teal"
            options={[{ key: '1', text: '표현1', value: '1' }, { key: '2', text: '표현2', value: '2' }]}
            text="감정표현"
            fluid
            onChange={this.handleChange}
          />
        </Button.Group>
      </Grid.Column>
    );
  }
}
