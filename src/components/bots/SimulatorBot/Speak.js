import React, { Component } from 'react';
import { Grid, Input } from 'semantic-ui-react';

export default class Speak extends Component {
  constructor() {
    super();
    this.state = { speak: '' };
  }

  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  speakHandler = (event) => {
    // 너와, 나의, 연결, 고리, 이건, 우리, 안의, 소리, 너와, 나의, 연결, 고리, 이건, 우리, 안의, 소리, turn up, 너와, 나의, 연결, 고리, 이건, 우리, 안의, 소리 turn up
    const { pibo } = this.props;
    const { speak } = this.state;
    if (event.key === 'Enter' || event.type === 'click') {
      pibo.speak(speak);
      this.setState({ speak: '' });
    }
  };

  render() {
    const { speak } = this.state;
    return (
      <Grid.Row>
        <Input
          name="speak"
          value={speak}
          placeholder="말할 내용을 적어주세요"
          action={{ color: 'teal', content: 'Speak', onClick: this.speakHandler }}
          onChange={this.handleChange}
          onKeyPress={this.speakHandler}
        />
      </Grid.Row>
    );
  }
}
