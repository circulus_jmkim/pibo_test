import React, { Component } from 'react';
import {
  Grid,
  Card,
  Dropdown,
  Button,
} from 'semantic-ui-react';
import { connect } from 'react-redux';

import DateDivider from '../common/DateDivider';
import { deleteHistoryItem } from '../../../ducks/bots/historyBot/historyBot';

class HistoryItem extends Component {
  deleteHistoryItemHandler = async (index) => {
    const { deleteHistoryDispatch } = this.props;
    try {
      await deleteHistoryDispatch(index);
    } catch (e) {
      console.log('deletePhotos error', e);
    }
  }

  render() {
    const {
      dayChange, createdAt, question, answer, index,
    } = this.props;
    return (
      <Grid.Row>
        {dayChange ? <DateDivider date={createdAt} /> : null }
        <Grid style={{ margin: '0 20px 5px 20px', width: '100%' }}>
          <Grid.Column width={15} style={{ padding: '0' }}>
            <span
              as="h4"
              style={{ color: '#248ED8', fontSize: '16px', fontWeight: 700 }}
            >
              {question}
            </span>
          </Grid.Column>
          <Grid.Column floated="right" width={1} style={{ margin: '0', padding: '0' }}>
            <Dropdown icon="ellipsis vertical" direction="left">
              <Dropdown.Menu>
                <Button.Group size="mini">
                  <Button color="blue">
                    공유
                  </Button>
                  <Button color="red" onClick={() => this.deleteHistoryItemHandler(index)}>
                    삭제
                  </Button>
                </Button.Group>
              </Dropdown.Menu>
            </Dropdown>
          </Grid.Column>
        </Grid>
        <Card style={{ margin: '0 20px 5px 20px', width: '100%' }}>
          <Card.Content>
            <Card.Description content={answer} />
          </Card.Content>
        </Card>
      </Grid.Row>
    );
  }
}

export default connect(
  null,
  dispatch => ({
    deleteHistoryDispatch: index => dispatch(deleteHistoryItem(index)),
  }),
)(HistoryItem);
