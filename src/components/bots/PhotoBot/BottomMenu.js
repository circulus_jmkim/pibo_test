import React, { Component } from 'react';
import {
  Menu,
  Icon,
} from 'semantic-ui-react';
import { connect } from 'react-redux';

import { deletePhotos } from '../../../ducks/bots/photoBot/photoBot';

class BottomMenu extends Component {
  deletePhotosHandler = async () => {
    const { deletePhotosDispatch } = this.props;
    try {
      await deletePhotosDispatch();
    } catch (e) {
      console.log('deletePhotos error', e);
    }
  }

  render() {
    return (
      <Menu
        widths={2}
        fixed="bottom"
        size="small"
        style={{ backgroundColor: '#E8E7EA' }}
      >
        <Menu.Item name="bots">
          <Icon name="download" size="large" />
        </Menu.Item>
        <Menu.Item name="botstore" onClick={this.deletePhotosHandler}>
          <Icon name="trash" size="large" />
        </Menu.Item>
      </Menu>
    );
  }
}

export default connect(
  null,
  dispatch => ({
    deletePhotosDispatch: () => dispatch(deletePhotos()),
  }),
)(BottomMenu);
