import React, { Component, Fragment } from 'react';
import {
  Grid,
  Header,
  Checkbox,
  Image,
} from 'semantic-ui-react';
import shortid from 'shortid';
import { withRouter } from 'react-router-dom';
import DateDivider from '../common/DateDivider';

class PhotoList extends Component {
  onClickPhotoHandler = (robotId, uuid) => {
    const { history } = this.props;
    history.push(`/bots/photo/detail?robotId=${robotId}&uuid=${uuid}`);
  }

  render() {
    const { photoData, editing, checkPhoto } = this.props;
    console.log(photoData);
    return (
      photoData.length > 0 && photoData.map((list, i) => (
        <Grid key={shortid.generate()} style={{ marginBottom: 10 }}>
          {list[0].newYear ? <DateDivider date={list[0].firstTime} year /> : null}
          <Header size="medium" style={{ marginBottom: 0 }}>
            {`${list[0].firstTime.month() + 1}월`}
            {` ${list[0].firstTime.date()}일`}
          </Header>
          <Grid.Row columns={4} style={{ padding: 0 }}>
            {list.map(({ robotId, uuid, checked }, l) => (
              <Grid.Column key={shortid.generate()} style={{ padding: 5, position: 'relative', top: 0 }}>
                {editing ? (
                  <Fragment>
                    <Image src={`http://dev.circul.us/photo/${robotId}/${uuid}`} onClick={() => checkPhoto(i, l)} />
                    <Checkbox style={{ position: 'absolute', bottom: 0, right: 0 }} checked={checked} onClick={() => checkPhoto(i, l)} />
                  </Fragment>
                ) : <Image src={`http://dev.circul.us/photo/${robotId}/${uuid}`} onClick={() => this.onClickPhotoHandler(robotId, uuid)} />}
              </Grid.Column>
            ))}
          </Grid.Row>
        </Grid>
      ))
    );
  }
}

export default withRouter(PhotoList);
