import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Grid, Image, Header, Radio,
} from 'semantic-ui-react';
import BotsSearch from './BotsSearch';

export default class BotsList extends Component {
  state = {
    isLoading: false,
    results: [],
    bots: [],
    value: '',
  };

  componentWillMount() {
    const { bots } = this.props;
    this.setState({ bots });
  }

  componentWillReceiveProps(nextProps) {
    const { bots, results, isSetting } = this.props;
    if (bots !== nextProps.bots) {
      this.setState({ bots: nextProps.bots });
    }
    if (results !== nextProps.results) {
      const { value } = this.state;
      if (value) {
        this.setState({ bots: nextProps.results });
      } else {
        this.setState({ bots });
      }
    }
    if (isSetting !== nextProps.isSetting) {
      this.resetComponent();
    }
  }

  handleUseToggleClick = (e, { id, checked }) => {
    const { handleUseClick } = this.props;
    handleUseClick(id, checked);
  }

  handleSearchResultClick = (e, { result }) => {
    this.setState({ bots: [result] });
  };

  resetComponent = () => {
    const { bots } = this.props;
    this.setState({ isLoading: false, bots, value: '' });
  };

  handleSearchRemove = () => {
    this.resetComponent();
  };

  handleSearchResult = () => {
    const { resultSelect } = this.props;
    resultSelect();
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ value });
    setTimeout(() => {
      this.addSearchChange();
    }, 500);
  };

  addSearchChange = () => {
    const { isLoading, value } = this.state;
    if (!isLoading) {
      const { addSearchChange } = this.props;
      addSearchChange(value);
    }
  };

  render() {
    const { isSetting } = this.props;
    const { bots } = this.state;
    return (
      <div>
        <BotsSearch
          {...this.state}
          handleSearchChange={this.handleSearchChange}
          handleSearchResult={this.handleSearchResult}
          handleSearchRemove={this.handleSearchRemove}
        />
        {
          isSetting && (
            <Grid>
              {bots && bots.map(({
                id, title, icon, use,
              }) => (
                <Grid.Row key={id} stretched>
                  <Grid.Column width={4}>
                    <Image src={icon} />
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <Grid widths={12}>
                      <Grid.Row>
                        <Header size="small" textAlign="left">
                          {title}
                        </Header>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column widths={6} floated="right" textAlign="right">
                          <Radio toggle color="blue" size="mini" id={id} checked={use} onClick={this.handleUseToggleClick} />
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </Grid.Column>
                </Grid.Row>
              ))}
            </Grid>
          )
        }
        {
          !isSetting && (
            <Grid>
              <Grid.Row columns={4} style={{ marginBottom: '1rem' }}>
                {bots && bots.map(({
                  id, title, icon, use,
                }) => use && (
                <Grid.Column key={id}>
                  <Image src={icon} />
                  <div style={{
                    overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis', fontWeight: 'bold',
                  }}
                  >
                    {title}
                  </div>
                </Grid.Column>
                ))}
              </Grid.Row>
              <Link to='bots/OFFICIAL_MESSAGE'>message</Link>
              <Link to='bots/OFFICIAL_CALENDAR'>calendar</Link>
              <Link to='bots/OFFICIAL_HISTORY'>history</Link>
              <Link to='bots/OFFICIAL_PHOTO'>photo</Link>
            </Grid>
          )
        }
      </div>
    );
  }
}
