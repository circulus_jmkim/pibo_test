import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Icon,
  List,
  Container,
  Checkbox,
} from 'semantic-ui-react';
import moment from 'moment';
import shortid from 'shortid';

import { checkItem } from '../../../ducks/bots/calendarBot/calendarBot';

class CalendarItemList extends Component {
  checkItemHandler = async (index) => {
    const { checkItemDispatch } = this.props;
    try {
      await checkItemDispatch(index);
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const {
      monthData,
      date,
      selectedMenu,
    } = this.props;

    const monthDataElement = monthData[date.get('date')];
    const listItem = [];
    if (monthDataElement) {
      monthDataElement.data.forEach(({
        title, execTime, kind, checked,
      }, index) => {
        if (selectedMenu === 'all' || selectedMenu === kind) {
          listItem.push(
            <List.Item key={shortid.generate()}>
              <List.Content floated="right">
                <Checkbox checked={checked} onClick={() => this.checkItemHandler(index)} />
              </List.Content>
              <Icon name="circle" size="tiny" color={kind === 'schedule' ? 'teal' : 'yellow'} />
              <List.Content>
                <span style={kind === 'schedule' ? { color: '#03BFD7', fontWeight: '600', marginRight: '0.5em' } : { color: '#fbbd08', fontWeight: '600', marginRight: '0.5em' }}>
                  {moment(execTime).format('HH:mm')}
                </span>
                <span>
                  {title}
                </span>
              </List.Content>
            </List.Item>,
          );
        }
      });
    }

    return (
      <Container textAlign="center" style={{ marginTop: '2em' }}>
        <List divided style={{ width: '90%', margin: 'auto' }}>
          {listItem}
        </List>
      </Container>
    );
  }
}

export default connect(
  ({ calendarBot }) => (
    {
      pending: calendarBot.get('pending'),
      date: calendarBot.get('date'),
      monthData: calendarBot.get('monthData'),
      selectedMenu: calendarBot.get('selectedMenu'),
    }
  ),
  dispatch => ({
    checkItemDispatch: index => dispatch(checkItem(index)),
  }),
)(CalendarItemList);
