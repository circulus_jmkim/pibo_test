import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import shortId from 'shortid';
import { TimeInput } from 'semantic-ui-calendar-react';

import './addScheduleForm.css';
import { setCalendar } from '../../../ducks/bots/calendarBot/calendarBot';
import RepeatValueSetting from './RepeatValueSetting';

const botOptions = [
  { key: 1, text: '메세지', value: 'message' },
  { key: 2, text: '히스토리', value: 'history' },
  { key: 3, text: '포토', value: 'photo' },
  { key: 4, text: '뉴스', value: 'news' },
];

const radioListData = [
  { name: '없음', value: 'none' },
  { name: '매일', value: 'daily' },
  { name: '매주', value: 'weekly' },
  { name: '매월', value: 'monthly' },
  { name: '매년', value: 'yearly' },
];

class AddScheduleForm extends Component {
  state = this.initialState = {
    title: '', bot: '', botOrder: '', date: '', time: '', repeat: 'none', day: '', week: '', month: '',
  };

  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  handleSubmit = async () => {
    const { setCalendarDispatch, history } = this.props;
    const {
      title, bot, botOrder, time, repeat, day, week, month,
    } = this.state;
    let { date } = this.state;
    const repeatValue = {
      day, week, month, date,
    };

    if (!date) {
      date = moment().format('YYYY-MM-DD');
    }

    try {
      await setCalendarDispatch({
        robotId: 'test', title, repeat, repeatValue, execTime: moment(`${date} ${time}`).format(), bot, botOrder, kind: 'schedule',
      });
      history.push('/bots/calendar');
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const {
      title, bot, botOrder, date, time, repeat,
    } = this.state;

    return (
      <Form style={{ marginTop: 125 }} onSubmit={this.handleSubmit}>
        <Form.Input name="title" value={title} label="일정 내용 (필수)" placeholder="일정을 입력하세요" onChange={this.handleChange} />
        <Form.Group inline>
          <label>
            반복(필수)
          </label>
        </Form.Group>
        <Form.Group inline>
          {radioListData.map(({ name, value }) => (
            <Form.Radio
              key={shortId.generate()}
              name="repeat"
              label={name}
              value={value}
              checked={repeat === value}
              onChange={this.handleChange}
            />
          ))}
        </Form.Group>
        <RepeatValueSetting repeat={repeat} date={date} handleChange={this.handleChange} />
        <TimeInput
          name="time"
          placeholder="Time"
          closable
          value={time}
          label="실행 시간"
          iconPosition="left"
          popupPosition="bottom left"
          onChange={this.handleChange}
        />
        <Form.Select name="bot" value={bot} label="실행할 봇" options={botOptions} placeholder="실행할 봇" onChange={this.handleChange} />
        <Form.Input name="botOrder" value={botOrder} label="명령어 입력" placeholder="명령을 입력하세요" onChange={this.handleChange} />
        <Form.Button color="blue" content="일정추가" position="right" style={{ float: 'right' }} />
      </Form>
    );
  }
}

export default connect(
  ({ calendarBot }) => (
    {
      apiMessage: calendarBot.get('apiMessage'),
      pending: calendarBot.get('pending'),
      error: calendarBot.get('error'),
    }
  ),
  dispatch => ({
    setCalendarDispatch: schedule => dispatch(setCalendar(schedule)),
  }),
)(withRouter(AddScheduleForm));
