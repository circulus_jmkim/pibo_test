import React, { Component } from 'react';
import { DateInput } from 'semantic-ui-calendar-react';
import { Form } from 'semantic-ui-react';

const weekOption = [
  { key: 0, text: '일요일', value: 0 },
  { key: 1, text: '월요일', value: 1 },
  { key: 2, text: '화요일', value: 2 },
  { key: 3, text: '수요일', value: 3 },
  { key: 4, text: '목요일', value: 4 },
  { key: 5, text: '금요일', value: 5 },
  { key: 6, text: '토요일', value: 6 },
];

const dayOption = [];
for (let i = 1; i < 32; i += 1) {
  dayOption.push({ key: i, text: i, value: i });
}

const monthOption = [];
for (let i = 1; i < 13; i += 1) {
  monthOption.push({ key: i, text: i, value: i - 1 });
}
export default class RepeatValueSetting extends Component {
  render() {
    const { repeat, date, handleChange } = this.props;

    let settingComponent = null;
    switch (repeat) {
      case 'none':
        settingComponent = (
          <DateInput
            name="date"
            dateFormat="YYYY-MM-DD"
            placeholder="Date"
            closable
            value={date}
            label="실행 날짜 (필수)"
            iconPosition="left"
            popupPosition="bottom left"
            onChange={handleChange}
          />
        );
        break;
      case 'daily':
        settingComponent = null;
        break;
      case 'weekly':
        settingComponent = (
          <Form.Select name="week" label="요일" options={weekOption} placeholder="선택" onChange={handleChange} width={6} />
        );
        break;
      case 'monthly':
        settingComponent = (
          <Form.Select name="day" label="일" options={dayOption} placeholder="선택" onChange={handleChange} width={6} />
        );
        break;
      case 'yearly':
        settingComponent = (
          <Form.Group>
            <Form.Select name="month" label="월" options={monthOption} placeholder="선택" onChange={handleChange} width={6} />
            <Form.Select name="day" label="일" options={dayOption} placeholder="선택" onChange={handleChange} width={6} />
          </Form.Group>
        );
        break;
      default: settingComponent = '기본달력';
        break;
    }
    return (
      settingComponent
    );
  }
}
