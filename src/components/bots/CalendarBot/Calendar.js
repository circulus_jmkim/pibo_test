import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {
  Label,
  Icon,
  Container,
} from 'semantic-ui-react';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import './calendar.css';

import DayPickerSingleDateControllerWrapper from './DayPickerSingleDateControllerWrapper';

import { setDate, changeMonth } from '../../../ducks/bots/calendarBot/calendarBot';

class Calendar extends Component {
  onDayClickHandler = async (day) => {
    const { setDateDispatch } = this.props;
    try {
      await setDateDispatch(day);
    } catch (error) {
      console.log(error);
    }
  }

  navIconHandler = async (e, nav) => {
    if (nav === 'prev' || nav === 'next') {
      const { changeMonthDispatch } = this.props;
      try {
        await changeMonthDispatch(nav);
      } catch (error) {
        console.log(error);
      }
    } else {
      e.preventDefault();
    }
  }

  renderDayContents = (day) => {
    const { monthData } = this.props;
    let dayComponent = null;

    const monthDataElement = monthData[day.get('date')];
    if (monthDataElement) {
      dayComponent = (
        <Fragment>
          {day.format('D')}
          <div>
            {monthDataElement.scheduleCount > 0 ? <Label size="mini" circular color="teal" empty /> : null}
            {monthDataElement.recordCount > 0 ? <Label size="mini" circular color="yellow" empty /> : null}
          </div>
        </Fragment>);
    } else {
      dayComponent = day.format('D');
    }
    return dayComponent;
  }

  render() {
    const NavPrevIcon = () => (
      <Icon onClick={e => this.navIconHandler(e, 'prev')} color="teal" circular name="chevron left" style={{ position: 'absolute', top: '18px', left: '22px' }} />
    );

    const NavNextIcon = () => (
      <Icon onClick={e => this.navIconHandler(e, 'next')} color="teal" circular name="chevron right" style={{ position: 'absolute', top: '18px', right: '22px' }} />
    );

    return (
      <Container textAlign="center" style={{ marginTop: '4em' }}>
        <DayPickerSingleDateControllerWrapper
          monthFormat="YYYY MMMM"
          navPrev={<NavPrevIcon />}
          navNext={<NavNextIcon />}
          onDateChange={this.onDayClickHandler}
          numberOfMonths={1}
          renderDayContents={this.renderDayContents}
          isOutsideRange={() => false}
        />
      </Container>
    );
  }
}

export default connect(
  ({ calendarBot }) => (
    {
      monthData: calendarBot.get('monthData'),
    }
  ),
  dispatch => ({
    changeMonthDispatch: nav => dispatch(changeMonth(nav)),
    setDateDispatch: day => dispatch(setDate(day)),
  }),
)(Calendar);
