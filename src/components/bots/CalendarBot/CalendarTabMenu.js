import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Container,
} from 'semantic-ui-react';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import { changeMenu } from '../../../ducks/bots/calendarBot/calendarBot';

class CalendarTabMenu extends Component {
  changeMenuHandler = async (menu) => {
    const { changeMenuDispatch } = this.props;
    try {
      await changeMenuDispatch(menu);
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { date } = this.props;

    return (
      <Container textAlign="center" style={{ marginTop: '1em' }}>
        <Button.Group size="tiny" widths="3" style={{ width: '95%', marginTop: 10 }}>
          <Button inverted color="blue" onClick={() => this.changeMenuHandler('schedule')}>
            Schedule
          </Button>
          <Button inverted color="blue" onClick={() => this.changeMenuHandler('all')}>
            {date.format('YYYY-MM-DD')}
          </Button>
          <Button inverted color="blue" onClick={() => this.changeMenuHandler('record')}>
            Record
          </Button>
        </Button.Group>
      </Container>
    );
  }
}

export default connect(
  ({ calendarBot }) => (
    {
      date: calendarBot.get('date'),
    }
  ),
  dispatch => ({
    changeMenuDispatch: menu => dispatch(changeMenu(menu)),
  }),
)(CalendarTabMenu);
