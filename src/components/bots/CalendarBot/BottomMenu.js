import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  Menu,
  Icon,
} from 'semantic-ui-react';
import { connect } from 'react-redux';

import './bottomMenu.css';
import { deleteItems } from '../../../ducks/bots/calendarBot/calendarBot';

class BottomMenu extends Component {
  menuItemHandler = (url) => {
    const { history } = this.props;
    history.push(`/bots/calendar/${url}`);
  }

  deleteMenuHandler = async () => {
    const { deleteItemsDispatch } = this.props;
    try {
      await deleteItemsDispatch();
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <Menu
        widths={3}
        fixed="bottom"
        size="small"
      >
        <Menu.Item onClick={() => this.menuItemHandler('addRecord')}>
          <span style={{ color: '#2185d0', fontWeight: 600, fontSize: '1.2em' }}>
            Record
          </span>
          <Icon name="add" size="large" color="blue" />
        </Menu.Item>
        <Menu.Item onClick={() => this.menuItemHandler('addSchedule')}>
          <span style={{ color: '#2185d0', fontWeight: 600, fontSize: '1.2em' }}>
            Schedule
          </span>
          <Icon name="add" size="large" color="blue" />
        </Menu.Item>
        <Menu.Item onClick={this.deleteMenuHandler}>
          <Icon name="trash" size="large" color="blue" />
        </Menu.Item>
      </Menu>
    );
  }
}

export default connect(
  null,
  dispatch => ({
    deleteItemsDispatch: () => dispatch(deleteItems()),
  }),
)(withRouter(BottomMenu));
