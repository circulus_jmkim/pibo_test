import React, { Component } from 'react';
import { Search, Grid, Icon } from 'semantic-ui-react';

export default class BotsSearch extends Component {
  render() {
    const {
      isLoading,
      value,
      handleSearchResult,
      handleSearchChange,
      handleSearchRemove,
    } = this.props;

    return (
      <Grid centered padded>
        <Grid.Column>
          <Search
            open={false}
            loading={isLoading}
            onResultSelect={handleSearchResult}
            onSearchChange={handleSearchChange}
            value={value}
            input={{ fluid: true, prompt: '검색어를 입력하세요.' }}
            icon={
              value.length ? (
                <Icon name="delete" link onClick={handleSearchRemove} />
              ) : (
                <Icon name="search" />
              )
            }
            fluid
          />
        </Grid.Column>
      </Grid>
    );
  }
}
