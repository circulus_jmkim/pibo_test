import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Menu, Header, Icon } from 'semantic-ui-react';

class BotTopMenu extends Component {
  onClickClose = () => {
    const { history } = this.props;
    history.push('/bots');
  }

  render() {
    const { firstChar, restChar } = this.props;
    return (
      <Menu fixed="top">
        <Menu.Item>
          <Header as="h2">
            <span style={{ color: '#03BFD7' }}>
              {firstChar}
            </span>
            <span>
              {restChar}
            </span>
          </Header>
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item onClick={this.onClickClose}>
            <Icon name="close" size="big" />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

export default withRouter(BotTopMenu);
