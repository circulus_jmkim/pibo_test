import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Menu, Header, Icon } from 'semantic-ui-react';

class BotTopSubMenu extends Component {
  onClickBack = () => {
    const { history, backLocation } = this.props;
    history.push(`/bots/${backLocation}`);
  }

  render() {
    const { title } = this.props;
    return (
      <Menu fixed="top" style={{ top: 55 }}>
        <Menu.Item>
          <Header as="h3">
            <span>
              <Icon name="chevron left" size="small" color="teal" onClick={this.onClickBack} style={{ cursor: 'pointer' }} />
              {title}
            </span>
          </Header>
        </Menu.Item>
      </Menu>
    );
  }
}

export default withRouter(BotTopSubMenu);
