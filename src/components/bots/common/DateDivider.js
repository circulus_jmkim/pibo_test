import React, { Component } from 'react';
import { Divider } from 'semantic-ui-react';
import moment from 'moment';

export default class DateDivider extends Component {
  render() {
    const { date, year } = this.props;

    if (year) {
      return (
        <div>
          <Divider horizontal>
            {moment(date).format('LL')}
          </Divider>
        </div>
      );
    }
    return (
      <div>
        <Divider horizontal>
          {moment(date).format('MMM Do dddd')}
        </Divider>
      </div>
    );
  }
}
