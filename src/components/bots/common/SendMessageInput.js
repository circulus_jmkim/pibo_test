import React, { Component, Fragment } from 'react';
import {
  Menu,
  Input,
  Icon,
} from 'semantic-ui-react';

export default class SendMessageInput extends Component {
  constructor(props) {
    super(props);
    this.state = { messageText: '' };
  }

  sendMessage = async (event) => {
    const { messageText } = this.state;
    const { sendMessageDispatch, sendMessageResult } = this.props;
    if (event.key === 'Enter' || event.type === 'click') {
      try {
        await sendMessageDispatch(messageText);
        sendMessageResult(true);
      } catch (e) {
        console.log('sendMessageDispatch error', e);
      }
      this.setState({ messageText: '' });
    }
  }

  changeText = (event) => {
    const { target: { name, value } } = event;
    this.setState({ [name]: value });
  }

  render() {
    const { icon, placeholder } = this.props;
    const { messageText } = this.state;
    return (
      <Fragment>
        <Menu
          widths={16}
          fixed="bottom"
          size="huge"
          style={{ backgroundColor: '#E8E7EA' }}
        >
          <Menu.Item style={{ borderColor: '#E8E7EA', borderStyle: 'solid' }}>
            <Input
              name="messageText"
              value={messageText}
              size="small"
              placeholder={placeholder}
              style={{ width: '90%' }}
              onChange={this.changeText}
              onKeyPress={this.sendMessage}
              icon={<Icon name={icon} inverted circular link color="blue" onClick={this.sendMessage} />}
            />
          </Menu.Item>
        </Menu>
      </Fragment>
    );
  }
}
