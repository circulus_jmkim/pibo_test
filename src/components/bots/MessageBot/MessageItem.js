import React, { Component, Fragment } from 'react';
import {
  Grid,
  Dropdown,
  Button,
  Label,
} from 'semantic-ui-react';
import { connect } from 'react-redux';
import moment from 'moment';

import DateDivider from '../common/DateDivider';
import { deleteMessageItem } from '../../../ducks/bots/messageBot/messageBot';

class MessageItem extends Component {
  labelColor = (status) => {
    switch (status) {
      case 'pending': return 'grey';
      case 'succese': return 'teal';
      case 'reacted': return 'yellow';
      default: break;
    }
    return 'grey';
  }

  deleteMessageItemHandler = async (index) => {
    const { deleteMessageDispatch } = this.props;
    try {
      await deleteMessageDispatch(index);
    } catch (e) {
      console.log('deletePhotos error', e);
    }
  }

  render() {
    const { message: messageData, index } = this.props;
    return (
      <Fragment>
        {messageData.dayChange ? (
          <Grid.Row>
            <DateDivider date={messageData.firstTime} />
          </Grid.Row>
        ) : null}
        <Grid.Row style={{ margin: '1em 1em 0.4em 1em' }}>
          <Label pointing="right" size="large" color={this.labelColor(messageData.status)} style={{ position: 'absolute', right: '1.2em', top: '0em' }}>
            {messageData.message}
          </Label>
          <Dropdown icon="ellipsis vertical" direction="left" style={{ position: 'absolute', right: '0em', top: '0em' }}>
            <Dropdown.Menu>
              <Button.Group size="mini">
                <Button color="blue">
                  공유
                </Button>
                <Button color="red" onClick={() => this.deleteMessageItemHandler(index)}>
                  삭제
                </Button>
              </Button.Group>
            </Dropdown.Menu>
          </Dropdown>
        </Grid.Row>
        <Grid.Row style={{ margin: '0' }}>
          <div style={{
            color: 'rgba(0,0,0,.4)', fontSize: '.875em', position: 'absolute', right: '4em', top: '0em',
          }}
          >
            {moment(messageData.firstTime).format('LT')}
          </div>
        </Grid.Row>
      </Fragment>
    );
  }
}

export default connect(
  null,
  dispatch => ({
    deleteMessageDispatch: index => dispatch(deleteMessageItem(index)),
  }),
)(MessageItem);
