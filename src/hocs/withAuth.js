import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Dimmer, Loader } from 'semantic-ui-react';

export default function withAuth(AuthWrappedComponent, NotAuthWrappedComponent) {
  return class extends Component {
    state = {
      currentUser: null,
      loading: false,
      redirectToLogin: false,
    };

    componentWillMount() {
      // 어떤 방식으로 하게 될지? jwt 참고
      // 로그인 상태를 체크하여 state를 맞게 변경해준다.
      this.setState({ loading: true });

      const currentUser = localStorage.getItem('currentUser');
      if (currentUser) {
        this.setState({
          currentUser: JSON.parse(currentUser),
          loading: false,
        });
      } else {
        this.setState({ redirectToLogin: true, loading: false });
      }
    }

    // componentWillUpdate(nexProps, nextState) {
    //   localStorage.setItem('currentUser', JSON.stringify(nextState.currentUser));
    // }

    render() {
      const { redirectToLogin, loading, currentUser } = this.state;
      if (redirectToLogin && !NotAuthWrappedComponent) {
        return <Redirect to="/login" />;
      }
      if (redirectToLogin && NotAuthWrappedComponent) {
        return <NotAuthWrappedComponent {...this.props} />;
      }

      if (loading) {
        return (
          <Dimmer active={loading}>
            <Loader>
Loading
            </Loader>
          </Dimmer>
        );
      }

      return <AuthWrappedComponent {...this.props} {...currentUser} />;
    }
  };
}
