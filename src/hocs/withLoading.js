import React, { Component, Fragment } from 'react';
import Loading from '../components/Loading';

export default function withLoading(WrappedComponent) {
  return class extends Component {
    render() {
      const { loading } = this.props;
      if (loading) {
        return (
          <Fragment>
            <Loading />
            <WrappedComponent {...this.props} />
          </Fragment>
        );
      }
      return <WrappedComponent {...this.props} />;
    }
  };
}
