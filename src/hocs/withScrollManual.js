import React from 'react';

const withScrollManual = Component => (
  class WithScrollManual extends React.Component {
    componentDidMount() {
      window.history.scrollRestoration = 'manual';
    }

    componentWillUnmount() {
      window.history.scrollRestoration = 'auto';
    }

    render() {
      return <Component {...this.props} />;
    }
  }
);

export default withScrollManual;
