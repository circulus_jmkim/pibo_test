import React from 'react';

const withInfiniteScroll = Component => (
  class WithInfiniteScroll extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        scrollTop: false,
        scrollBottom: false,
        scrollHeight: 0,
        update: true,
        scrollTopUpdate: false,
      };
    }

    componentDidMount() {
      window.addEventListener('scroll', this.onScroll, false);
    }

    componentDidUpdate() {
      this.onScroll();
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.onScroll, false);
    }

    onScroll = () => {
      const { scrollHeight: oldScrollHeight, update, scrollTopUpdate } = this.state;
      const scrollTop = (document.documentElement && document.documentElement.scrollTop)
        || document.body.scrollTop;
      const scrollHeight = (document.documentElement && document.documentElement.scrollHeight)
        || document.body.scrollHeight;
      const clientHeight = document.documentElement.clientHeight || window.innerHeight;
      const scrollBottom = Math.ceil(scrollTop + clientHeight) >= scrollHeight;
      const scrollTopState = scrollTop === 0;

      if (update && scrollBottom) {
        this.setState({ scrollBottom, update: false },
          () => this.setState({ scrollBottom: false }));
      }

      if (scrollTopUpdate && scrollTopState) {
        this.setState({ scrollTop: true, scrollTopUpdate: false });
      }

      if (!scrollTopUpdate && scrollTop !== 0) {
        this.setState({ scrollTop: false, scrollTopUpdate: true });
      }

      if (oldScrollHeight !== scrollHeight) {
        this.setState({ scrollHeight, update: true });
      }
    }

    render() {
      return <Component {...this.props} scroll={this.state} />;
    }
  }
);

export default withInfiniteScroll;
