import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Segment, Button, Form, Header, Accordion, Icon, List, Input,
} from 'semantic-ui-react';
import {
  fetchCurrentSSID, connectAp, fetchConnectResult, fetchApList,
} from '../../ducks/connect';
import pibo from '../../pibo/index';

class TestPage extends Component {
  static defaultProps = {
    onMount: () => {},
    robotId: '',
    ssid: 'ssid',
    aplist: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      showDirection: false,
      showaplist: false,
      ap: {
        essid: '',
        encrypted: false,
        address: '',
        password: '',
        signal: 0,
        wpa: false,
      },
    };
  }

  componentDidMount() {
    pibo.onload();
    const { onRobotConnect } = this.props;
    // console.log('robotId', robotId);
    // if (robotId) {
    //   pibo.init(robotId);
    //   pibo.info((data) => {
    //     console.log(data);
    //   });
    //   onMount(robotId);
    // }
    onRobotConnect();
  }

  componentWillReceiveProps(nextProps) {
    const { robotId, aplist } = this.props;
    if (robotId !== nextProps.robotId || robotId.length > 0) {
      localStorage.setItem('robotId', nextProps.robotId);
      pibo.init(nextProps.robotId);
      // pibo.info((data) => { alert(data); });
    }
    if (aplist !== nextProps.aplist) {
      this.setState({
        showaplist: true,
      });
    }
  }

  onConnect() {
    const { getSSID } = this.props;
    getSSID();
  }

  onChange = (e) => {
    const { aplist } = this.props;
    const { ap, ...rest } = this.state;
    const changeTarget = e.currentTarget.name;
    const apIdx = changeTarget.split('_')[1];
    const apItem = aplist[apIdx];
    return this.setState({
      ap: {
        ...apItem,
        password: '',
      },
      ...rest,
    });
  }

  handlePassChange = (e, { value }) => {
    const { ap, ...rest } = this.state;
    const { password, ...restap } = ap;
    this.setState({
      ...rest,
      ap: {
        ...restap,
        password: value,
      },
    });
  }

  handleClickBots = (e, { name }) => {
    const { robotId, ssid, history } = this.props;
    if (robotId.length > 0 && ssid.length > 0) {
      history.push(`/bots/${name}`);
    }
    history.push(`/bots/${name}`);
  }

  handleClickApList = () => {
    const { onFetchApList } = this.props;
    onFetchApList();
  }

  handleClickConnect = () => {
    const { onApConnect } = this.props;
    const { ap } = this.state;
    onApConnect(ap);
  }

  render() {
    const {
      robotId, ssid, aplist, loading,
    } = this.props;
    const { showDirection, showaplist, ap } = this.state;
    return (
      <Segment.Group>
        <Segment padded>
          <Header as="h1">파이보 테스트</Header>
        </Segment>
        <Segment.Group>
          <Segment>
            <Accordion>
              <Accordion.Title
                active={showDirection}
                onClick={() => this.setState({ showDirection: !showDirection })}
              >
                <Icon name="dropdown" />
                로봇 연결하기
              </Accordion.Title>
              <Accordion.Content active={showDirection}>
                <p>1. Wi-Fi 목록에서 "pibo_#####" 과 같은 형식의 Wi-Fi를 선택합니다.</p>
                <p>2. 비밀번호 "1234567890" 을 입력하여 연결합니다.</p>
                <p>3. 로봇과 연결 되면 하단 Wi-Fi 선택 버튼을 눌러 연결할 Wi-Fi를 선택합니다.</p>
                <p>4. 로봇이 Wi-Fi에 연결되면 로봇 기능을 사용할 수 있습니다.`</p>
              </Accordion.Content>
            </Accordion>
          </Segment>
          <Segment>
            <Form>
              <Form.Field>
                <label>연결 된 로봇 serial</label>
                <Input value={robotId} readOnly />
              </Form.Field>
              <Form.Field>
                <label>연결 된 Wi-Fi</label>
                <Input value={ssid || ap.essid} readOnly />
              </Form.Field>
              {(ap.essid.length > 0 && ap.encrypted) && (
                <Input prompt="비밀번호" type="password" fluid value={ap.password} onChange={this.handlePassChange} />
              )}
            </Form>
            {!showaplist && (
              <Button onClick={this.handleClickApList} loading={loading}>Wi-Fi 선택</Button>
            )}
            {showaplist && (
              <Button onClick={this.handleClickConnect} loading={loading}>연결</Button>
            )}
            {showaplist && (
              <Accordion>
                <Accordion.Title
                  active={showaplist}
                  onClick={() => this.setState({ showDirection: !showaplist })}
                >
                  <Icon name="dropdown" />
                  Wi-Fi 목록
                </Accordion.Title>
                <Accordion.Content active={showaplist}>
                  <List divided relaxed>
                    {aplist.length > 0 && aplist.map((apitem, idx) => (
                      <List.Item
                        as="a"
                        value={apitem.address}
                        name={`apitem_${idx}`}
                        key={apitem.address}
                        onClick={this.onChange}
                      >
                        <List.Content
                          verticalAlign="middle"
                          style={{
                            display: 'flex',
                            flexFlow: 'row-reverse',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            height: '45px',
                          }}
                        >
                          <div>
                            {apitem.encrypted && <Icon name="lock" />}
                            <Icon name="wifi" />
                          </div>
                          {apitem.essid}
                        </List.Content>
                      </List.Item>
                    ))}
                  </List>
                </Accordion.Content>
              </Accordion>
            )}
          </Segment>
        </Segment.Group>
        <Segment.Group>
          <Segment>
            <List>
              <List.Item as="a" name="OFFICIAL_SIMULATOR" onClick={this.handleClickBots}>
                <Header as="h4">시뮬레이터</Header>
              </List.Item>
              <List.Item as="a" name="OFFICIAL_PHOTO" onClick={this.handleClickBots}>
                <Header as="h4">포토</Header>
              </List.Item>
            </List>
          </Segment>
        </Segment.Group>
      </Segment.Group>
    );
  }
}

export default connect(
  state => ({
    error: state.connect.errorMsg,
    loading: state.connect.loading,
    aplist: state.connect.aplist,
    robotId: state.connect.robotId,
    ssid: state.connect.ssid,
    stepComplete: state.connect.stepComplete,
  }),
  dispatch => ({
    getSSID: () => {
      dispatch(fetchCurrentSSID());
    },
    onApConnect: (ap) => {
      dispatch(connectAp(ap));
    },
    onRobotConnect: () => {
      dispatch(fetchConnectResult());
    },
    onFetchApList: () => {
      dispatch(fetchApList());
    },
  }),
)(TestPage);
