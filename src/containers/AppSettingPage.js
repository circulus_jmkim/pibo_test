import React, { Component } from 'react';
import TopMenu from '../components/TopMenu';
import AppLocale from '../components/pibo/settings/PiboLocale';
import AppInfo from '../components/pibo/settings/AppInfo';
import AppCS from '../components/pibo/settings/AppCustomerService';
import AppOpinion from '../components/pibo/settings/AppSendOpinion';
import { setLocalStorage } from '../utils/common';
import pjson from '../../package.json';

export default class AppSettingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: '',
      value: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCompleteClick = this.handleCompleteClick.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    const { params } = match;
    const { menu } = params;
    const value = menu === 'locale' ? localStorage.getItem('locale') : '';
    this.setState({
      item: menu,
      value,
    });
  }

  getMenuObject = (item) => {
    const menuObj = {};
    const data = { list: ['ko', 'en', 'cn', 'jp'], value: '' };
    // 'WITH_BACK_AND_COMPLETE' 모드의 모든 메뉴는 완료 처리 이벤트도 함께 보내줘야 함.
    switch (item) {
      case 'locale':
        data.value = localStorage.getItem(item);
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '언어 설정';
        menuObj.component = <AppLocale {...data} handleChange={this.handleChange} />;
        break;
      case 'info':
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '앱 정보';
        menuObj.component = (
          <AppInfo curVer={pjson.version} latestVer="0.0.0" onClcik={this.handleChange} />
        );
        break;
      case 'opinion':
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '의견 보내기';
        menuObj.component = <AppOpinion />;
        break;
      case 'cs':
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '고객센터';
        menuObj.component = <AppCS />;
        break;
      default:
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '앱 설정';
        break;
    }

    return menuObj;
  };

  handleChange = (value) => {
    const { item } = this.state;
    this.setState({
      item,
      value,
    });
  };

  handleBackClick = () => {
    const { history } = this.props;
    history.push('/pibo');
  };

  handleCompleteClick = () => {
    const { value } = this.state;
    setLocalStorage('locale', value);
    const { history } = this.props;
    history.push('/pibo');
  };

  render() {
    const { item } = this.state;
    const menuObj = this.getMenuObject(item);

    return (
      <div>
        <TopMenu
          title={menuObj.title}
          menuMode={menuObj.mode}
          handleBackClick={this.handleBackClick}
          handleCompleteClick={this.handleCompleteClick}
        />
        {menuObj.component}
      </div>
    );
  }
}
