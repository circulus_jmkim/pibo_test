import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Grid, Segment, Button, Header, Icon,
} from 'semantic-ui-react';
import TopMenu from '../components/TopMenu';
import BottomMenu from '../components/BottomMenu';
import PiboCard from '../components/pibo/PiboCard';
import PiboFavoriteCard from '../components/pibo/PiboFavoriteCard';
import PiboSettingsCard from '../components/pibo/PiboSettingsCard';
import { getPiboData } from '../ducks/pibo';
import { getFavBotsList } from '../ducks/bots';
import withLoading from '../hocs/withLoading';
import { getPid, getUid } from '../utils/common';

const PiboCardWithLoading = withLoading(PiboCard);
const PiboFavoriteCardWithLoading = withLoading(PiboFavoriteCard);

class PiboPage extends Component {
  static defaultProps = {
    onMount: () => {},
  }

  constructor(props) {
    super(props);
    this.state = {
      signin: false,
      link: false,
    };
  }

  componentDidMount() {
    const { onMount } = this.props;
    const pid = getPid();
    const uid = getUid();
    if (!pid || !uid) {
      return this.setState({
        signin: uid !== '',
        link: pid !== '',
      });
    }
    onMount(pid);
    return this.setState({
      signin: true,
      link: true,
    });
  }

  handleClickUserCog = () => {
    const { history } = this.props;
    const uid = getUid();
    history.push(`/setting/${uid}`);
  }

  handleClickAppCog = (e, { name }) => {
    const { history } = this.props;
    history.push(`/app/${name}`);
  }

  handleClickCog = () => {
    const { history } = this.props;
    const pid = getUid();
    history.push(`/pibo/${pid}`);
  }

  handleMenuItemClick = (e, { name }) => {
    const { history } = this.props;
    history.push(`/${name}`);
  };

  handleSignInClick = () => {
    const { history } = this.props;
    history.push('/login');
  }

  handleConnectClick = () => {
    const { history } = this.props;
    history.push('/connect');
  }

  render() {
    const {
      pibo, loading,
    } = this.props;
    const {
      bot, ...rest
    } = pibo;
    const {
      link, signin,
    } = this.state;
    const menuMode = (getUid()) ? 'WITH_USER' : 'WITH_LOGIN';
    const handleCompleteClick = (menuMode === 'WITH_USER') ? this.handleClickUserCog : this.handleSignInClick;
    return (
      <div>
        <TopMenu title="pibo" menuMode={menuMode} handleCompleteClick={handleCompleteClick} />
        {
          !signin && (
          <Grid centered padded>
            <Grid.Row>
              <Segment
                loading={loading}
                style={{
                  position: 'absolute', left: '0', right: '0', margin: '1rem', padding: '2rem 1rem 1rem',
                }}
              >
                <Icon size="huge" color="blue" name="sign in alternate" />
                <Header as="h3">
  앱을 사용하려면 로그인하세요.
                </Header>
                <Button inverted color="blue" size="big" fluid onClick={this.handleSignInClick}>
  로그인
                </Button>
              </Segment>
            </Grid.Row>
          </Grid>
          )
          }
        {
          (signin && !link) && (
            <Grid centered padded>
              <Grid.Row>
                <Segment
                  loading={loading}
                  style={{
                    position: 'absolute', left: '0', right: '0', margin: '1rem', padding: '2rem 1rem 1rem',
                  }}
                >
                  <Icon size="huge" color="blue" name="unlink" />
                  <Header as="h3">
연결된 파이보 없음
                  </Header>
                  <Button inverted color="blue" size="big" fluid onClick={this.handleConnectClick}>
연결하기
                  </Button>
                </Segment>
              </Grid.Row>
            </Grid>
          )
          }
        {
          (signin && link) && (
            <Grid centered padded>
              <PiboCardWithLoading
                pibo={rest}
                loading={loading}
                onClickCog={this.handleClickCog}
              />
              <PiboFavoriteCardWithLoading bots={bot} loading={loading} />
              <PiboSettingsCard onClickCog={this.handleClickAppCog} />
              <Grid.Row />
              <Grid.Row />
            </Grid>
          )
          }
        <BottomMenu selectMenu="pibo" handleMenuItemClick={this.handleMenuItemClick} />
      </div>
    );
  }
}

export default connect(
  state => ({
    pibo: state.pibo.pibo,
    loading: state.pibo.loading,
    errorMsg: state.pibo.errorMsg,
  }),
  dispatch => ({
    onMount: (pid) => {
      dispatch(getPiboData(pid));
      dispatch(getFavBotsList());
    },
  }),
)(PiboPage);
// 로그인 상태 체크: TopMenu에 로그인 상태에 따른 상태 넘겨줄 것 (로그인 여부)
// 파이보 연결 상태 체크: 연결 여부에 따라 본문의 카드가 아닌 연결 화면이 보여져야 함 (파이보 연결 여부)
// PiboCard에 파이보 정보(시리얼, 콜네임, 연결된 와이파이명, 배터리, 온도, 사운드볼륨) 넘겨줄 것
// PiboFavoriteCard에 사용자의 즐겨찾는 bot 리스트 넘겨줄 것
