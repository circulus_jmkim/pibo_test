import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import TopMenu from '../components/TopMenu';
import UserInfo from '../components/pibo/settings/UserInfo';
import UserWithdraw from '../components/pibo/settings/UserWithdraw';
import withLoading from '../hocs/withLoading';
import { updateUserInfo, fetchUserInfo, withdrawUserAccount } from '../ducks/user';
import { getUid } from '../utils/common';

const UserInfoWithLoading = withLoading(UserInfo);
const UserWithdrawWithLoading = withLoading(UserWithdraw);
const interestList = [
  { name: 'living', selected: false },
  { name: 'food', selected: false },
  { name: 'car', selected: false },
  { name: 'sports', selected: false },
  { name: 'health', selected: false },
  { name: 'fashion&beauty', selected: false },
  { name: 'game', selected: false },
  { name: 'tv&entertain', selected: false },
  { name: 'music', selected: false },
  { name: 'movie', selected: false },
  { name: 'book&culture', selected: false },
  { name: 'travel', selected: false },
  { name: 'design', selected: false },
  { name: 'economy', selected: false },
  { name: 'science', selected: false },
  { name: 'business', selected: false },
  { name: 'perform&exhibit', selected: false },
  { name: 'law', selected: false },
  { name: 'pet', selected: false },
  { name: 'love&marriage', selected: false },
  { name: 'tech', selected: false },
  { name: 'sensibility', selected: false },
  { name: 'politics', selected: false },
];

class UserSettingDetailPage extends Component {
  static defaultProps = {
    onMount: () => {},
    user: {
      lastName: '',
      firstName: '',
      nickName: '',
      email: '',
      birthDate: 0,
      interest: [],
    },
  }

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleCompleteClick = this.handleCompleteClick.bind(this);
  }

  componentDidMount() {
    const {
      onMount, match,
    } = this.props;
    const { params } = match;
    const { menu } = params;
    if (menu === 'update') { onMount(getUid()); }
  }

  getMenuObject = () => {
    const {
      loading, match, user,
    } = this.props;
    const { params } = match;
    const { menu } = params;
    const menuObj = {};
    switch (menu) {
      case 'update':
        user.interestList = interestList;
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '내 정보 수정';
        menuObj.component = (
          <UserInfoWithLoading
            {...user}
            loading={loading}
            handleChange={this.handleChange}
          />
        );
        break;
      case 'withdraw':
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '탈퇴';
        menuObj.component = (
          <UserWithdrawWithLoading
            loading={loading}
            handleWithdrawClick={this.handleWithdrawClick}
          />
        );
        break;
      default:
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '계정 설정';
        break;
    }
    return menuObj;
  };

  handleChange = (data) => {
    const { item, value } = data;
    const { user } = this.props;
    this.setState({
      user: {
        ...user,
        [item]: value,
      },
    });
  }

  handleBackClick = () => {
    const { history } = this.props;
    history.push(`/setting/${getUid()}`);
  };

  handleCompleteClick = () => {
    const { onSave } = this.props;
    const { user } = this.state;
    onSave(getUid(), user);
  };

  handleWithdrawClick = () => {
    const { onConfirmWithdraw } = this.props;
    onConfirmWithdraw(getUid());
  }

  render() {
    const menuObj = this.getMenuObject();
    const { isComplete, match } = this.props;
    const { menu } = match.params;
    const uid = getUid();
    if (isComplete && menu === 'update') {
      return <Redirect to={`/setting/${uid}`} />;
    }
    if (isComplete && menu === 'withdraw') {
      return <Redirect to="/pibo" />;
    }

    return (
      <div>
        <TopMenu
          title={menuObj.title}
          menuMode={menuObj.mode}
          handleBackClick={this.handleBackClick}
          handleCompleteClick={this.handleCompleteClick}
        />
        {menuObj.component}
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user.user,
    loading: state.user.loading,
    errorMsg: state.user.errorMsg,
    isComplete: state.user.isComplete,
  }),
  dispatch => ({
    // 계정 설정 완료 후 저장
    onSave: (uid, data) => {
      dispatch(updateUserInfo(uid, data));
    },
    onConfirmWithdraw: (uid) => {
      dispatch(withdrawUserAccount(uid));
    },
    // 설정 할 항목에 대한 내 파이보 설정값과 선택사항 로드
    onMount: (uid) => {
      dispatch(fetchUserInfo(uid));
    },
  }),
)(UserSettingDetailPage);
