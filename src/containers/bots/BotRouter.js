import React, { Component } from 'react';
import qs from 'qs';

import HistoryBot from './HistoryBot/Index';
import PhotoBot from './PhotoBot/Index';
import MessageBot from './MessageBot/Index';
import CalendarBot from './CalendarBot/Index';
import SimulatorBot from './SimulatorBot/Index';

export default class BotRouter extends Component {
  matchingComponent = (botName, page, search) => {
    const query = qs.parse(search, { ignoreQueryPrefix: true });

    switch (botName) {
      case 'OFFICIAL_HISTORY': return <HistoryBot />;
      case 'OFFICIAL_PHOTO': return <PhotoBot page={!page ? 'main' : 'detail'} query={query} />;
      case 'OFFICIAL_MESSAGE': return <MessageBot />;
      case 'OFFICIAL_CALENDAR': return <CalendarBot page={!page ? 'main' : page} />;
      case 'OFFICIAL_SIMULATOR': return <SimulatorBot />;
      default: return '해당 봇이 없습니다.';
    }
  }

  render() {
    const { location: { search }, match: { params: { name, page } } } = this.props;
    const selectedComponet = this.matchingComponent(name, page, search);
    return (
      <div>
        {selectedComponet}
      </div>
    );
  }
}
