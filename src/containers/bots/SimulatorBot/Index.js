import React, { Component, Fragment } from 'react';
import {
  Grid, Button, Icon, Input,
} from 'semantic-ui-react';
import styled from 'styled-components';

import BotTopMenu from '../../../components/bots/common/BotTopMenu';
import Direction from '../../../components/bots/SimulatorBot/Direction';

export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      upEnable: false, leftEnable: false, rightEnable: false, downEnable: false, position: true,
    };
  }

  onControllerHandler = (direction) => {
    const { [`${direction}Enable`]: selectedButtonState } = this.state;
    const buttonState = {
      upEnable: false, leftEnable: false, rightEnable: false, downEnable: false,
    };

    buttonState[`${direction}Enable`] = !selectedButtonState;
    this.setState(buttonState);
  }

  editButton = () => {
    const { position } = this.state;
    this.setState({ position: !position });
  }

  render() {
    const {
      upEnable, leftEnable, rightEnable, downEnable, position,
    } = this.state;

    const StyledGrid = styled(Grid)`
      margin-top: 60px !important;
      padding: 0 !important;
      background: linear-gradient(rgba(255,255,255,0.8), rgba(255,255,255,0.8)), url("/image/bots/simulator/pibo-${position ? 'front' : 'back'}-side.png") center center no-repeat;
      background-repeat: no-repeat;
      background-size: contain;
    }
    `;

    console.log(this.state);

    return (
      <Fragment>
        <BotTopMenu firstChar="S" restChar="imulator" />
        <Button
          icon
          labelPosition="right"
          floated="right"
          size="small"
          color="teal"
          onClick={this.editButton}
          style={{
            position: 'fixed', top: '70px', right: '20px', zIndex: 1,
          }}
        >
          {position ? '앞' : '뒤'}
          <Icon name="right arrow" />
        </Button>
        <StyledGrid
          columns={4}
          centered
        >
          <Grid.Row>
            <Direction direction="up" enable={upEnable} onClick={() => this.onControllerHandler('up')} />
          </Grid.Row>
          <Grid.Row>
            <Direction direction="left" enable={leftEnable} onClick={() => this.onControllerHandler('left')} />
            <Grid.Column>
            </Grid.Column>
            <Direction direction="right" enable={rightEnable} onClick={() => this.onControllerHandler('right')} />
          </Grid.Row>
          <Grid.Row>
            <Direction direction="down" enable={downEnable} onClick={() => this.onControllerHandler('down')} />
          </Grid.Row>
          <Grid.Row>
            <Input placeholder="말할 내용을 적어주세요" action={{ color: 'teal', content: 'Speak' }} />
          </Grid.Row>
        </StyledGrid>
      </Fragment>
    );
  }
}
