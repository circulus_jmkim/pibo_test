import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import BotTopMenu from '../../../components/bots/common/BotTopMenu';
import BottomMenu from '../../../components/bots/CalendarBot/BottomMenu';
import Calendar from '../../../components/bots/CalendarBot/Calendar';
import CalendarTabMenu from '../../../components/bots/CalendarBot/CalendarTabMenu';
import CalendarItemList from '../../../components/bots/CalendarBot/CalendarItemList';
import Loading from '../../../components/Loading';

import { getDefaultData } from '../../../ducks/bots/calendarBot/calendarBot';

class MainContainer extends Component {
  async componentDidMount() {
    const { getDefaultDataDispatch } = this.props;
    try {
      await getDefaultDataDispatch();
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { getDefaultDataStatus, loading } = this.props;
    return (
      <Fragment>
        {getDefaultDataStatus && !loading ? null : <Loading />}
        <BotTopMenu firstChar="C" restChar="alendar" />
        <Calendar />
        <CalendarTabMenu />
        <CalendarItemList />
        <BottomMenu />
      </Fragment>
    );
  }
}

export default connect(
  ({ calendarBot }) => (
    {
      getDefaultDataStatus: calendarBot.get('getDefaultDataStatus'),
      loading: calendarBot.get('pending'),
    }
  ),
  dispatch => ({
    getDefaultDataDispatch: () => dispatch(getDefaultData()),
  }),
)(withRouter(MainContainer));
