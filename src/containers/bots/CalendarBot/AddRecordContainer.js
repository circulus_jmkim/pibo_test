import React, { Component } from 'react';
import {
  Container,
  Button,
  Form,
} from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactLoading from 'react-loading';
import moment from 'moment';

import BotTopSubMenu from '../../../components/bots/common/BotTopSubMenu';
import { setCalendar, getRecords } from '../../../ducks/bots/calendarBot/calendarBot';

class AddRecordContainer extends Component {
  state = {
    timeElapsed: 0,
    isRecording: false,
    title: '',
    blobURL: '',
    log: 'log',
  };

  componentDidMount() {
    this.getRecord(0, 10);
  }

  getRecord = async (offset, limit) => {
    const { getRecordDispatch } = this.props;
    try {
      await getRecordDispatch(offset, limit);
    } catch (e) {
      console.log('getHistory error', e);
    }
  }

  getUnits() {
    const { timeElapsed } = this.state;
    const seconds = timeElapsed / 1000;
    return {
      min: Math.floor(seconds / 60).toString(),
      sec: Math.floor(seconds % 60).toString(),
      msec: (seconds % 1).toFixed(1).substring(2),
    };
  }

  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  startRecording = () => {
    const { Media } = window;
    if (Media) {      
      const media = new Media(`${moment().format('YYYYMMDDhhmmss')}.mp3`,
        // success callback
        function() {
          alert("recordAudio():Audio Success");
        },

        // error callback
        function(err) {
          alert("recordAudio():Audio Error: "+ err.code);
        }
      );
      media.startRecord();
      this.startTimer();
      this.setState({
        isRecording: true,
        media,
        log: `media : ${JSON.stringify(media)}`,
      });
    } else {
      this.setState({
        log: 'window.Media nothing',
      });
    }
  };

  stopRecording = () => {
    const { FileUploadOptions, FileTransfer } = window;
    const { media } = this.state;
    if (media && FileUploadOptions && FileTransfer) {
      media.stopRecord();
      const options = new FileUploadOptions();
      options.fileKey = 'file';
      options.mimeType = 'audio/mpeg';
      options.fileName = media.src; // fileURL.substr(fileURL.lastIndexOf('/') + 1);
      options.chunkedMode = false;
      options.params = {
        userId: 'test',
        robotId: 'test',
        projectId: 'voice',
      };

      const ft = new FileTransfer();
      ft.upload(`/storage/emulated/0/${media.src}`, encodeURI('http://dev.circul.us/content'), (r) => {
         alert('R Code : '+ JSON.stringify(r));
         this.setState({ response: true, log: `media.src : 성공, ${media.src}`, media: false });
      }, (e) => {
         alert('E Code : '+ e.code);
         this.setState({
          log: `media.src : 실패, ${media.src}`, media: false, response: false,
        });
      }, options);
      this.getRecord(0, 10);
    } else {
      this.setState({
        log: 'window.Media nothing',
      });
    }
    clearInterval(this.timer);
    this.setState({
      isRecording: false,
      timeElapsed: 0,
    });
  };

  startTimer = () => {
    this.startTime = Date.now();
    this.timer = setInterval(this.update, 10);
  };

  update = () => {
    const { timeElapsed } = this.state;
    const delta = Date.now() - this.startTime;
    this.setState({ timeElapsed: timeElapsed + delta });
    this.startTime = Date.now();
  };

  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  addRecordHandler = async () => {
    const { title, blobURL } = this.state;
    const { setCalendarDispatch, history } = this.props;

    try {
      await setCalendarDispatch({
        robotId: 'test', title, recordUrl: blobURL, kind: 'record',
      });
      history.push('/bots/calendar');
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const units = this.getUnits();
    const { isRecording, response, title, log } = this.state;
    const { records } = this.props;
    console.log('records', records);
    console.log('response', response);
    return (
      <Container color="grey">
        <BotTopSubMenu title="음성기록 추가" backLocation="calendar" />
        <Form style={{ marginTop: 125 }}>
          <Form.Input name="title" value={title} label="제목" placeholder="음성 기록 제목을 입력하세요." onChange={this.handleChange} />
        </Form>
        <Container textAlign="center" style={{ marginTop: 20 }}>
          {isRecording ? (
            <div style={{ marginTop: 10 }}>
              <ReactLoading
                type="bars"
                style={{
                  fill: '#03BFD7', height: '64px', width: '64px', margin: 'auto',
                }}
              />
            </div>
          ) : null}
          {isRecording ? (
            <div style={{ marginTop: 10 }}>
              <div style={{ marginTop: 10, fontSize: 20 }}>
                <span>
                  {units.min}
                  :
                </span>
                <span>
                  {units.sec}
                  .
                </span>
                <span>
                  {units.msec}
                </span>
              </div>
            </div>
          ) : null}
          <div style={{ marginTop: 10 }}>
            <Button onClick={this.startRecording}>
              Start
            </Button>
            <Button onClick={this.stopRecording}>
              Stop
            </Button>
          </div>
          {isRecording && records && records.length > 0 ? (
            <div style={{ marginTop: 10 }}>
              <audio controls preload="false">
                <track kind="captions" />
                <source src={`http://dev.circul.us/voice/${records[0].robotId}/${records[0].uuid}`} controls />
              </audio>
            </div>
          ) : null}
          <div style={{ marginTop: 10 }}>
            <input type="text" value={log} />
          </div>
          <div style={{ marginTop: 10 }}>
            <Button color="blue" onClick={this.addRecordHandler} floated="right">
              추가
            </Button>
          </div>
        </Container>
      </Container>
    );
  }
}

export default connect(
  ({ calendarBot }) => (
    {
      records: calendarBot.get('records').toJS(),
      apiMessage: calendarBot.get('apiMessage'),
      pending: calendarBot.get('pending'),
      error: calendarBot.get('error'),
    }
  ),
  dispatch => ({
    setCalendarDispatch: record => dispatch(setCalendar(record)),
    getRecordDispatch: (skip, limit) => dispatch(getRecords(skip, limit)),
  }),
)(withRouter(AddRecordContainer));
