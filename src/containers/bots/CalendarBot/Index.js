import React, { Component, Fragment } from 'react';

import MainContainer from './MainContainer';
import AddScheduleContainer from './AddScheduleContainer';
import AddRecordContainer from './AddRecordContainer';
import BotTopMenu from '../../../components/bots/common/BotTopMenu';

export default class Index extends Component {
  childComponent = (page) => {
    switch (page) {
      case 'main': return <MainContainer />;
      case 'addSchedule': return <AddScheduleContainer />;
      case 'addRecord': return <AddRecordContainer />;
      default: return <MainContainer />;
    }
  };

  render() {
    const { page } = this.props;
    const component = this.childComponent(page);
    return (
      <Fragment>
        <BotTopMenu firstChar="C" restChar="alendar" />
        {component}
      </Fragment>
    );
  }
}
