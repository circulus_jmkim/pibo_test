import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';

import BotTopSubMenu from '../../../components/bots/common/BotTopSubMenu';
import AddScheduleForm from '../../../components/bots/CalendarBot/AddScheduleForm';

export default class AddScheduleContainer extends Component {
  render() {
    return (
      <Container color="grey">
        <BotTopSubMenu title="일정 추가" backLocation="calendar" />
        <AddScheduleForm />
      </Container>
    );
  }
}
