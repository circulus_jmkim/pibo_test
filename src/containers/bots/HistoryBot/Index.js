import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Grid } from 'semantic-ui-react';
import shortId from 'shortid';

import BotTopMenu from '../../../components/bots/common/BotTopMenu';
import HistoryItem from '../../../components/bots/HistoryBot/HistoryItem';
import SendMessageInput from '../../../components/bots/common/SendMessageInput';
import * as historyBotActions from '../../../ducks/bots/historyBot/historyBot';
import withScrollManual from '../../../hocs/withScrollManual';
import withInfiniteScroll from '../../../hocs/withInfiniteScroll';

class Index extends Component {
  componentDidMount() {
    this.getHistory(0, 10);
  }

  componentWillReceiveProps(nextProps) {
    const { scroll: { scrollTop: nextScrollTop }, historyData } = nextProps;
    const { scroll: { scrollTop } } = this.props;

    if ((nextScrollTop && !scrollTop) && historyData.length > 0) {
      this.getHistory(historyData.length, 10);
    }
  }

  shouldComponentUpdate(nextProps) {
    const {
      pending,
      sendHistoryPending,
      scroll: { scrollHeight },
    } = this.props;
    const {
      pending: nextPending,
      sendHistoryPending: nextSendHistoryPending,
      scroll: { scrollHeight: nextScrollHeight },
    } = nextProps;

    if (pending && !nextPending) {
      return true;
    }

    if (sendHistoryPending && !nextSendHistoryPending) {
      return true;
    }

    if (scrollHeight !== nextScrollHeight) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps) {
    const { scroll: { scrollHeight: prevScrollHeight } } = prevProps;
    const { scroll: { scrollHeight } } = this.props;
    if (scrollHeight > prevScrollHeight) {
      this.scrollTo(scrollHeight - prevScrollHeight);
    }
  }

  getHistory = async (offset, limit) => {
    const { historyBotActions: { getHistory } } = this.props;
    try {
      await getHistory(offset, limit);
    } catch (e) {
      console.log('getHistory error', e);
    }
  }

  scrollTo = (scrollHeight) => {
    window.scrollTo(0, scrollHeight);
  }

  sendMessageResult = (result) => {
    const { scroll: { scrollHeight } } = this.props;
    if (result) {
      this.scrollTo(scrollHeight);
    }
  }

  render() {
    const {
      historyData,
      historyBotActions: { sendMessage },
    } = this.props;

    return (
      <Fragment>
        <BotTopMenu firstChar="H" restChar="istory" />
        <Grid style={{ margin: '0 0 61px 0' }}>
          {historyData && historyData.map((history, index) => (
            <HistoryItem {...history} index={index} key={shortId.generate()} />
          ))}
        </Grid>
        <SendMessageInput
          icon="send"
          placeholder="파이보에게 하고 싶은 말을 입력하세요."
          sendMessageDispatch={sendMessage}
          sendMessageResult={this.sendMessageResult}
        />
      </Fragment>
    );
  }
}

export default connect(
  ({ historyBot }) => (
    {
      pending: historyBot.get('pending'),
      error: historyBot.get('error'),
      historyData: historyBot.get('historyData').toJS(),
      sendHistoryPending: historyBot.get('sendHistoryPending'),
      sendHistoryError: historyBot.get('sendHistoryError'),
    }
  ),
  dispatch => ({
    historyBotActions: bindActionCreators(historyBotActions, dispatch),
  }),
)(withScrollManual(withInfiniteScroll(Index)));
