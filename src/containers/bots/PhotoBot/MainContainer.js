import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Grid,
  Button,
} from 'semantic-ui-react';
import BottomMenu from '../../../components/bots/PhotoBot/BottomMenu';
import PhotoList from '../../../components/bots/PhotoBot/PhotoList';
import * as photoBotActions from '../../../ducks/bots/photoBot/photoBot';
import withInfiniteScroll from '../../../hocs/withInfiniteScroll';
import withScrollManual from '../../../hocs/withScrollManual';

class Index extends Component {
  componentDidMount() {
    const { getInitDataStatus, photoBotActions: { getInitData } } = this.props;
    if (!getInitDataStatus) {
      this.getPhotos(0, 30);
      getInitData();
    }
  }

  componentWillReceiveProps(props) {
    const { scroll: { scrollBottom }, photoDataLength } = props;
    if (scrollBottom && photoDataLength > 0) {
      console.log('scrollBottom', scrollBottom);
      this.getPhotos(photoDataLength, photoDataLength + 30);
    }
  }

  // shouldComponentUpdate(nextProps) {
  //   const { loading: nextPropsLoading, editing: nextEditing } = nextProps;
  //   const { loading, editing } = this.props;
  //   if (loading !== nextPropsLoading || editing !== nextEditing) {
  //     return true;
  //   }
  //   return false;
  // }

  getPhotos = async (skip, limit) => {
    const { photoBotActions: { getPhotos } } = this.props;
    try {
      await getPhotos(skip, limit);
    } catch (e) {
      console.log('getPhotos error', e);
    }
  }

  editButton = () => {
    const { photoBotActions: { toggleEdit } } = this.props;
    toggleEdit();
  }

  checkPhoto = (parentIndex, childIndex) => {
    const { photoBotActions: { checkPhoto }, editing } = this.props;
    if (editing) {
      checkPhoto(parentIndex, childIndex);
    }
  }

  render() {
    const { photoData, editing } = this.props;
    return (
      <Fragment>
        <Button
          floated="right"
          circular
          icon="settings"
          onClick={this.editButton}
          style={editing ? {
            color: '#248ED8', position: 'fixed', top: '70px', right: '20px', zIndex: 1,
          } : {
            position: 'fixed', top: '70px', right: '20px', zIndex: 1,
          }}
        />
        <Grid style={{ margin: '70px 20px 50px' }} onScroll={this.scrollTracking}>
          <Grid.Row>
            <PhotoList photoData={photoData} editing={editing} checkPhoto={this.checkPhoto} />
          </Grid.Row>
        </Grid>
        {editing ? <BottomMenu /> : null}
      </Fragment>
    );
  }
}

export default connect(
  ({ photoBot }) => (
    {
      getInitDataStatus: photoBot.get('getInitDataStatus'),
      loading: photoBot.get('pending'),
      error: photoBot.get('error'),
      editing: photoBot.get('editing'),
      photoData: photoBot.get('photoData').toJS(),
      photoDataLength: photoBot.get('photoDataLength'),
    }
  ),
  dispatch => ({
    photoBotActions: bindActionCreators(photoBotActions, dispatch),
  }),
)(withScrollManual(withInfiniteScroll(Index)));
