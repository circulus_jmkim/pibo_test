import React, { Component, Fragment } from 'react';
import { Image, Grid } from 'semantic-ui-react';
import MainContainer from './MainContainer';
import BotTopMenu from '../../../components/bots/common/BotTopMenu';
import BotTopSubMenu from '../../../components/bots/common/BotTopSubMenu';

export default class Index extends Component {
  childComponent = (page) => {
    const { query } = this.props;
    if (page === 'main') {
      return <MainContainer />;
    }
    return (
      <Fragment>
        <BotTopSubMenu title="자세히 보기" backLocation="photo" />
        <Grid verticalAlign="middle" columns={1} centered style={{ marginTop: '110px' }}>
          <Grid.Row>
            <Grid.Column>
              <Image src={`http://dev.circul.us/photo/${query.robotId}/${query.uuid}`} fluid />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Fragment>
    );
  };

  render() {
    const { page } = this.props;
    const component = this.childComponent(page);
    return (
      <Fragment>
        <BotTopMenu firstChar="P" restChar="hoto" />
        {component}
      </Fragment>
    );
  }
}
