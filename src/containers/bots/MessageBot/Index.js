import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Grid } from 'semantic-ui-react';
import shortId from 'shortid';
import moment from 'moment';

import BotTopMenu from '../../../components/bots/common/BotTopMenu';
import MessageItem from '../../../components/bots/MessageBot/MessageItem';
import SendMessageInput from '../../../components/bots/common/SendMessageInput';
import * as messageBotActions from '../../../ducks/bots/messageBot/messageBot';
import withScrollManual from '../../../hocs/withScrollManual';
import withInfiniteScroll from '../../../hocs/withInfiniteScroll';
import 'moment/locale/ko';

class Index extends Component {
  componentDidMount() {
    moment.locale('ko');
    this.getMessages(0, 30);
  }

  componentWillReceiveProps(nextProps) {
    const { scroll: { scrollTop: nextScrollTop }, messageData } = nextProps;
    const { scroll: { scrollTop } } = this.props;

    if ((nextScrollTop && !scrollTop) && messageData.length > 0) {
      this.getMessages(messageData.length, 10);
    }
  }

  shouldComponentUpdate(nextProps) {
    const { pending, sendMessagePending, scroll: { scrollHeight } } = this.props;
    const {
      pending: nextPending,
      sendMessagePending: nextSendMessagePending,
      scroll: { scrollHeight: nextScrollHeight },
    } = nextProps;

    if (pending && !nextPending) {
      return true;
    }

    if (sendMessagePending && !nextSendMessagePending) {
      return true;
    }

    if (scrollHeight !== nextScrollHeight) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps) {
    const { scroll: { scrollHeight: prevScrollHeight } } = prevProps;
    const { scroll: { scrollHeight } } = this.props;
    if (scrollHeight > prevScrollHeight) {
      this.scrollTo(scrollHeight - prevScrollHeight);
    }
  }

  getMessages = async (offset, limit) => {
    const { messageBotActions: { getMessages } } = this.props;
    try {
      await getMessages(offset, limit);
    } catch (e) {
      console.log('getMessage error', e);
    }
  }

  scrollTo = (scrollHeight) => {
    window.scrollTo(0, scrollHeight);
  }

  sendMessageResult = (result) => {
    const { scroll: { scrollHeight } } = this.props;
    if (result) {
      this.scrollTo(scrollHeight);
    }
  }

  render() {
    const {
      messageData,
      messageBotActions: { sendMessage },
    } = this.props;

    return (
      <Fragment>
        <BotTopMenu firstChar="M" restChar="essage" />
        <Grid style={{ margin: '0 0 61px 0' }}>
          {messageData && messageData.map((message, index) => (
            <MessageItem
              message={message}
              key={shortId.generate()}
              index={index}
            />
          ))}
        </Grid>
        <SendMessageInput
          icon="microphone"
          placeholder="파이보가 전달 할 메시지를 입력하세요."
          sendMessageDispatch={sendMessage}
          sendMessageResult={this.sendMessageResult}
        />
      </Fragment>
    );
  }
}

export default connect(
  ({ messageBot }) => (
    {
      pending: messageBot.get('pending'),
      error: messageBot.get('error'),
      messageData: messageBot.get('messageData').toJS(),
      sendMessagePending: messageBot.get('sendMessagePending'),
      sendMessageError: messageBot.get('sendMessageError'),
    }
  ),
  dispatch => ({
    messageBotActions: bindActionCreators(messageBotActions, dispatch),
  }),
)(withScrollManual(withInfiniteScroll(Index)));
