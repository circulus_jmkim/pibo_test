import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Reveal, Sidebar } from 'semantic-ui-react';
import TopMenu from '../components/TopMenu';
import BottomMenu from '../components/BottomMenu';
import StoreBotDetail from '../components/botstore/StoreBotDetail';
import StoreMenu from '../components/botstore/StoreMenu';
import { fetchBotDetails } from '../ducks/botstore';
import { getPid } from '../utils/common';

class BotsStoreDetailPage extends Component {
  static defaultProps = {
    bot: {
      category: '',
      icon: '',
      makerImg: '',
      makerNickname: '',
      reviews: [],
      score: 0,
      title: '',
      user: {
        isInstalled: false,
        img: '',
        name: '',
        review: {},
      },
      detail: { update: '업데이트 내역', description: '설명', requires: '요구사항 명세' },
    },
    loading: false,
    errorMsg: '',
    onMount: () => {},
  };

  state = {
    visible: false,
  };

  componentDidMount() {
    const { onMount, match } = this.props;
    const { bid } = match.params;
    onMount(bid);
  }

  handleSidebarBtnClick = () => {
    const { visible } = this.state;
    this.setState({ visible: !visible });
  };

  handleMenuItemClick = (e, { name }) => {
    const { history } = this.props;
    history.push(`/${name}`);
  };

  handleSidebarHide = () => this.setState({ visible: false });

  handleMyBotClick = () => {
    this.setState({ visible: false });
    const { history } = this.props;
    history.push(`/botstore/my/${getPid()}`);
  };

  mainContent = () => {
    const { visible } = this.state;
    const { bot } = this.props;
    return (
      <Sidebar.Pusher dimmed={visible}>
        <TopMenu
          title="botstore"
          menuMode="WITH_BACK_AND_MENU"
          handleCompleteClick={this.handleSidebarBtnClick}
        />
        <StoreBotDetail {...bot} />
      </Sidebar.Pusher>
    );
  };

  render() {
    const { visible } = this.state;
    return (
      <div>
        <StoreMenu
          visible={visible}
          handleSidebarHide={this.handleSidebarHide}
          handleMyBotClick={this.handleMyBotClick}
          MainPusher={this.mainContent}
        />
        <Reveal>
          <Reveal.Content hidden={visible}>
            <BottomMenu selectMenu="botstore" handleMenuItemClick={this.handleMenuItemClick} />
          </Reveal.Content>
        </Reveal>
      </div>
    );
  }
}

export default connect(
  state => ({
    loading: state.botstore.loading,
    errorMsg: state.botstore.errorMsg,
    bot: state.botstore.bot,
  }),
  dispatch => ({
    onMount: bid => dispatch(fetchBotDetails(bid)),
  }),
)(BotsStoreDetailPage);

// 로그인 여부 따로 체크하지 않음
// botstore에 올라와 있는 모든 bot 리스트 가져오기
// 광고할 bot 배너 이미지와 링크 리스트 (2, 3개)
// bot 리스트 아이템의 정보 (아이템id, 제목, 썸네일, 평점, 평가개수, 명령어 || 상세설명)
