import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Image, Header, Loader,
} from 'semantic-ui-react';
import styled from 'styled-components';
import * as moment from 'moment';
import Terms from '../components/account/Terms';
import Names from '../components/account/Names';
import Info from '../components/account/Info';
import Account from '../components/account/Account';
import Email from '../components/account/Email';
import { insertUserData } from '../ducks/user';

const JoinWrap = styled.div`
  margin: 5vw;
  width: 90vw;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column;
`;

const JoinButton = styled(Button.Group)`
  margin: 0 !important;
  position: fixed !important;
  bottom: 0;
  left: 0;
  right: 0;
`;

const steps = ['terms', 'name', 'info', 'account', 'email', 'complete'];

class JoinPage extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  state = {
    terms: {
      service: false,
      privacy: false,
      location: false,
      promotion: false,
      msg: '',
    },
    names: {
      first: '',
      last: '',
      msg: '',
    },
    info: {
      year: '',
      month: '',
      day: '',
      gender: '',
      genderList: ['female', 'male', 'unknown'],
      msg: '',
    },
    account: {
      id: '',
      pw: '',
      rePw: '',
      msg: '',
    },
    email: {
      address: '',
      hadSent: false,
      msg: '',
    },
    step: steps[0],
  }

  onChange = (e, data) => {
    const { step } = this.state;
    switch (step) {
      case 'terms':
        return this.onTermsChange(e, data);
      case 'name':
        return this.onNamesChange(e);
      case 'info':
        return this.onInfoChange(e);
      case 'account':
        return this.onAccountChange(e);
      case 'email':
        return this.onEmailChange(e);
      default:
        break;
    }
    return '';
  }

  onTermsChange = (e, { name }) => {
    const { terms, ...rest } = this.state;
    const {
      service, privacy, location, promotion, msg,
    } = terms;

    switch (name) {
      case 'all':
        if (service === privacy === location === promotion) {
          return this.setState({
            terms: {
              service: !service,
              privacy: !privacy,
              location: !location,
              promotion: !promotion,
              msg: this.getTermsMsg(!service, !privacy),
            },
            ...rest,
          });
        }
        return this.setState({
          terms: {
            service: true,
            privacy: true,
            location: true,
            promotion: true,
            msg: this.getTermsMsg(true, true),
          },
          ...rest,
        });
      case 'service':
        return this.setState({
          terms: {
            service: !service,
            privacy,
            location,
            promotion,
            msg: this.getTermsMsg(!service, privacy),
          },
          ...rest,
        });
      case 'privacy':
        return this.setState({
          terms: {
            service,
            privacy: !privacy,
            location,
            promotion,
            msg: this.getTermsMsg(service, !privacy),
          },
          ...rest,
        });
      case 'location':
        return this.setState({
          terms: {
            service,
            privacy,
            location: !location,
            promotion,
            msg,
          },
          ...rest,
        });
      case 'promotion':
        return this.setState({
          terms: {
            service,
            privacy,
            location,
            promotion: !promotion,
            msg,
          },
          ...rest,
        });
      default:
        return this.setState({
          terms,
          ...rest,
        });
    }
  }

  onNamesChange = (e) => {
    const { names } = this.state;
    const { last, first } = names;
    const changeTarget = e.target.name;
    const changeValue = e.target.value;
    if (changeTarget === 'first') {
      return this.setState({
        names: {
          first: changeValue,
          last,
          msg: this.getNameMsg(last, changeValue),
        },
      });
    }

    return this.setState({
      names: {
        last: changeValue,
        first,
        msg: this.getNameMsg(changeValue, first),
      },
    });
  }

  onInfoChange = (e) => {
    const { info, ...rest } = this.state;
    const {
      year, month, day, gender, genderList, msg,
    } = info;
    let inputYY = year;
    let inputMM = month;
    let inputDD = day;
    let inputGender = gender;
    const changeTarget = e.target.name;
    const changeValue = e.target.value;
    const selectGender = genderList.find(item => (item === changeValue));
    switch (changeTarget) {
      case 'year':
        inputYY = Math.abs(parseInt(changeValue.slice(0, 4), 10));
        break;
      case 'month':
        inputMM = Math.abs(parseInt(changeValue.slice(0, -1), 10));
        break;
      case 'day':
        inputDD = Math.abs(parseInt(changeValue.slice(0, 2), 10));
        break;
      case 'gender':
        inputGender = selectGender;
        break;
      default:
        break;
    }
    return this.setState({
      info: {
        year: inputYY,
        month: inputMM,
        day: inputDD,
        gender: inputGender,
        genderList,
        msg,
      },
      ...rest,
    });
  }

  onAccountChange = (e) => {
    const { account } = this.state;
    const { id, pw, rePw } = account;
    const changeTarget = e.target.name;
    const changeValue = e.target.value;
    if (changeTarget === 'id') {
      return this.setState({
        account: {
          id: changeValue,
          pw,
          rePw,
          msg: this.getAccountMsg(changeValue, pw, rePw),
        },
      });
    }

    if (changeTarget === 'pw') {
      return this.setState({
        account: {
          id,
          pw: changeValue,
          rePw,
          msg: this.getAccountMsg(id, changeValue, rePw),
        },
      });
    }

    return this.setState({
      account: {
        id,
        pw,
        rePw: changeValue,
        msg: this.getAccountMsg(id, pw, changeValue),
      },
    });
  }

  onEmailChange = (e) => {
    const { email, ...rest } = this.state;
    const { hadSent } = email;
    const changeValue = e.target.value;
    const newMsg = this.getEmailMsg(changeValue);
    // 유효한 이메일 주소 형식인지 확인
    // 등록 된 이메일인지 확인
    if (newMsg) {
      return this.setState({
        email: {
          address: changeValue,
          hadSent,
          msg: newMsg,
        },
        ...rest,
      });
    }

    return this.setState({
      email: {
        address: changeValue,
        hadSent,
        msg: '',
      },
      ...rest,
    });
  }

  onConfirm = (e, { name }) => {
    const { history } = this.props;
    const { step, ...rest } = this.state;
    const stepIdx = steps.findIndex(el => el === step);
    if (name === 'cancel') {
      if (stepIdx < 1 || stepIdx === steps.length - 1) {
        return history.go(-1);
      }
      return this.setState({
        step: steps[stepIdx - 1],
        ...rest,
      });
    }
    if (name === 'ok') {
      switch (step) {
        case 'terms':
          return this.confirmTerms();
        case 'name':
          return this.confirmName();
        case 'info':
          return this.confirmInfo();
        case 'account':
          return this.confirmAccount();
        case 'email':
          return this.confirmEmail();
        default:
          break;
      }
    }
    return '';
  }

  onComplete = () => {
    const { history } = this.props;
    history.push('/login');
  }

  getNameMsg = (last, first) => {
    if (!(last || first)) {
      return '성과 이름을 입력해주세요.';
    }
    if (!last) {
      return '성을 입력해주세요.';
    }
    if (!first) {
      return '이름을 입력해주세요.';
    }
    const nameRegExp = new RegExp(/^[a-zA-Z가-힣]+$/, 'ig');
    if (last.match(nameRegExp) > -1 || first.match(nameRegExp) > -1) {
      return '숫자, 공백 및 특수문자는 입력할 수 없습니다.';
    }
    return '';
  }

  getInfoMsg = (yy, mm, dd, gd) => {
    if (!(yy && mm && dd)) {
      return '생년월일을 입력해주세요.';
    }
    const today = moment();
    const birth = moment(`${yy}-${mm + 1}-${dd}`, 'YYYY-MM-DD');
    if (!(birth.isValid() || birth.isBefore(today))) {
      return '유효한 날짜가 아닙니다.';
    }

    const { info } = this.state;
    const { genderList } = info;
    if (!genderList.find(item => item === gd)) {
      return '유효한 성별이 아닙니다.';
    }
    return '';
  }

  getAccountMsg = (id, pw, rePw) => ''

  getEmailMsg = (email) => {
    const mailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    const validPattern = mailRegex.test(email);
    const validAccount = false;
    if (!validPattern) {
      return '유효한 메일 형식이 아닙니다.';
    }

    if (validAccount) {
      return '이미 등록 된 메일 입니다.';
    }

    return '';
  }

  getTermsMsg = (agreeService, agreePrivacy) => {
    if (!(agreeService || agreePrivacy)) {
      return '이용약관과 개인정보 수집 및 이용은 필수 동의 사항입니다.';
    }
    if (!agreeService) {
      return '이용약관은 필수 동의 사항입니다.';
    }
    if (!agreePrivacy) {
      return '개인정보 수집 및 이용은 필수 동의 사항입니다.';
    }
    return '';
  }

  confirmTerms = () => {
    const { terms, step, ...rest } = this.state;
    const {
      service, privacy, location, promotion,
    } = terms;
    if (service && privacy) {
      return this.setState({
        terms: {
          service,
          privacy,
          location,
          promotion,
          msg: '',
        },
        step: 'name',
        ...rest,
      });
    }
    return this.setState({
      terms: {
        service,
        privacy,
        location,
        promotion,
        msg: this.getTermsMsg(service, privacy),
      },
      step,
      ...rest,
    });
  }

  confirmName = () => {
    const { names, step, ...rest } = this.state;
    const { first, last } = names;
    const newMsg = this.getNameMsg(last, first);
    const nextStep = (newMsg) ? step : 'info';
    this.setState({
      names: {
        first,
        last,
        msg: newMsg,
      },
      step: nextStep,
      ...rest,
    });
  }

  confirmAccount = () => {
    const { account, step, ...rest } = this.state;
    const { id, pw, rePw } = account;
    const newMsg = this.getAccountMsg(id, pw, rePw);
    const nextStep = (newMsg) ? step : 'email';
    this.setState({
      account: {
        id,
        pw,
        rePw,
        msg: newMsg,
      },
      step: nextStep,
      ...rest,
    });
  }

  confirmEmail = () => {
    const { onJoin } = this.props;
    const { email, step, ...rest } = this.state;
    const { address, hadSent } = email;
    const newMsg = this.getEmailMsg(address);
    const nextStep = (newMsg) ? step : 'complete';
    if (nextStep === 'complete') {
      onJoin({ email, ...rest });
    }
    this.setState({
      email: {
        address,
        hadSent,
        msg: newMsg,
      },
      step: nextStep,
      ...rest,
    });
  }

  confirmInfo = () => {
    const { info, step, ...rest } = this.state;
    const {
      year, month, day, gender, genderList,
    } = info;
    const newMsg = this.getInfoMsg(year, month, day, gender);
    if (newMsg) {
      return this.setState({
        info: {
          year,
          month,
          day,
          gender,
          genderList,
          msg: newMsg,
        },
        step,
        ...rest,
      });
    }
    return this.setState({
      info: {
        year,
        month,
        day,
        gender,
        genderList,
        msg: '',
      },
      step: 'account',
      ...rest,
    });
  }

  sendMail = () => {
    const { email, ...rest } = this.state;
    const { address, hadSent } = email;
    const newMsg = this.getEmailMsg(address);
    if (newMsg) {
      return this.setState({
        email: {
          address,
          hadSent,
          msg: newMsg,
        },
        ...rest,
      });
    }

    // 인증메일 보내는 요청 후 정상발송 응답을 받은 경우 상태를 변경
    return this.setState({
      email: {
        address,
        hadSent: true,
        msg: '',
      },
      ...rest,
    });
  }

  render() {
    const { loading } = this.props;
    const {
      terms, names, info, step, account, email,
    } = this.state;
    const { first } = names;
    return (
      <div>
        <JoinWrap>
          <Image
            src="../image/logo_circulus.png"
            size="small"
            style={{ margin: '2rem 0 4rem' }}
            centered
          />
          {
            (step === 'terms') && (
              <Terms {...terms} onChange={this.onChange} />
            )
          }
          {
            (step === 'name') && (
              <Names {...names} onChange={this.onChange} />
            )
          }
          {
            (step === 'info') && (
              <Info {...info} onChange={this.onInfoChange} />
            )
          }
          {
            (step === 'account') && (
              <Account {...account} onChange={this.onChange} />
            )
          }
          {
            (step === 'email') && (
              <Email {...email} onChange={this.onChange} onSend={this.sendMail} />
            )
          }
          {
            (step === 'complete') && (
              <div>
                <Loader active={loading} />
                <Header as="h3" centered>
                  {!loading && `${first}님 가입이 완료 되었습니다.`}
                </Header>
              </div>
            )
          }
        </JoinWrap>
        {
          (step === 'complete')
          && (
            <JoinButton widths="1" fluid attached="bottom">
              <Button color="blue" name="complete" onClick={this.onComplete}>
                완료
              </Button>
            </JoinButton>
          )
        }
        {
          (step !== 'complete')
          && (
            <JoinButton widths="2" fluid attached="bottom">
              <Button name="cancel" onClick={this.onConfirm}>
                {
                (step === 'terms') ? '비동의' : '이전'
              }
              </Button>
              <Button color="blue" name="ok" onClick={this.onConfirm} disabled={(step === 'email' && !email.hadSent)}>
                {
                (step === 'terms') ? '동의' : '다음'
              }
              </Button>
            </JoinButton>
          )
        }
      </div>
    );
  }
}

export default connect(
  state => ({
    loading: state.user.loading,
    complete: state.user.joinSuccess,
  }),
  dispatch => ({
    onJoin: (userdata) => {
      dispatch(insertUserData(userdata));
    },
  }),
)(JoinPage);
