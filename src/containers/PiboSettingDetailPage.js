import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import TopMenu from '../components/TopMenu';
import PiboCallname from '../components/pibo/settings/PiboCallname';
import PiboTempUnit from '../components/pibo/settings/PiboTempUnit';
import PiboAlign from '../components/pibo/settings/PiboAlign';
import PiboNetwork from '../components/pibo/settings/PiboNetwork';
import PiboPosition from '../components/pibo/settings/PiboPosition';
import PiboLocale from '../components/pibo/settings/PiboLocale';
import PiboInstruction from '../components/pibo/settings/PiboInstruction';
import { savePiboCog, fetchCogList } from '../ducks/pibo';
import withLoading from '../hocs/withLoading';
import { getPid } from '../utils/common';

const CallnameWithLoading = withLoading(PiboCallname);
const TempUnitWithLoading = withLoading(PiboTempUnit);
const AlignWithLoading = withLoading(PiboAlign);
const NetworkWithLoading = withLoading(PiboNetwork);
const PositionWithLoading = withLoading(PiboPosition);
const LanguageWithLoading = withLoading(PiboLocale);
const InstructionWithLoading = withLoading(PiboInstruction);

class PiboSettingDetailPage extends Component {
  static defaultProps = {
    onMount: () => {},
    data: { itme: '', value: '', list: [] },
  }

  constructor(props) {
    super(props);
    this.state = {
      item: '',
      value: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCompleteClick = this.handleCompleteClick.bind(this);
  }

  componentDidMount() {
    const {
      onMount, match,
    } = this.props;
    const { params } = match;
    const { item } = params;
    this.setState({ item });
    onMount(getPid(), item);
  }

  getMenuObject = (item) => {
    const menuObj = {};
    // 'WITH_BACK_AND_COMPLETE' 모드의 모든 메뉴는 완료 처리 이벤트도 함께 보내줘야 함.
    switch (item) {
      case 'name':
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '호출명 설정';
        break;
      case 'tempUnit':
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '온도 단위 설정';
        break;
      case 'servo':
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '기기정렬 상세 설정';
        break;
      case 'wifi':
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '와이파이 설정';
        break;
      case 'geo':
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '위치 설정';
        break;
      case 'locale':
        menuObj.mode = 'WITH_BACK_AND_COMPLETE';
        menuObj.title = '언어 설정';
        break;
      case 'instruction':
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '사용 안내';
        break;
      default:
        menuObj.mode = 'WITH_BACK';
        menuObj.title = '파이보 설정';
        break;
    }

    return menuObj;
  };

  handleChange = (value) => {
    this.setState({
      value,
    });
  }

  handleBackClick = () => {
    const { history } = this.props;
    history.push(`/pibo/${getPid()}`);
  };

  handleCompleteClick = () => {
    const { onSave, data } = this.props;
    const { value } = this.state;
    onSave(getPid(), { [data.item]: value });
  }

  render() {
    const {
      data, isSaved, loading, onMount,
    } = this.props;
    const { item } = this.state;
    const menuObj = this.getMenuObject(item);

    if (isSaved) {
      return (
        <Redirect to={`/pibo/${getPid()}`} />
      );
    }

    return (
      <div>
        <TopMenu
          title={menuObj.title}
          menuMode={menuObj.mode}
          handleBackClick={this.handleBackClick}
          handleCompleteClick={this.handleCompleteClick}
        />
        {
          (item === 'name') && (
            <CallnameWithLoading
              loading={loading}
              {...data}
              handleChange={this.handleChange}
            />
          )
        }
        {
          (item === 'tempUnit') && (
            <TempUnitWithLoading
              loading={loading}
              {...data}
              handleChange={this.handleChange}
            />
          )
        }
        {
          (item === 'servo') && (
            <AlignWithLoading
              loading={loading}
              {...data}
              handleChange={this.handleChange}
            />
          )
        }
        {
          (item === 'wifi') && (
            <NetworkWithLoading
              loading={loading}
              isInit={false}
              ap={data.value}
              aplist={data.list}
              onChange={this.handleChange}
              onReSearch={() => onMount(getPid(), item)}
            />
          )
        }
        {
          (item === 'geo') && (
            <PositionWithLoading
              loading={loading}
              {...data}
              handleChange={this.handleChange}
            />
          )
        }
        {
          (item === 'locale') && (
            <LanguageWithLoading
              loading={loading}
              {...data}
              handleChange={this.handleChange}
            />
          )
        }
        {
          (item === 'instruction') && (
            <InstructionWithLoading
              loading={loading}
              {...data}
              handleChange={this.handleChange}
            />
          )
        }
      </div>
    );
  }
}

export default connect(
  state => ({
    data: state.pibo.data,
    loading: state.pibo.loading,
    errorMsg: state.pibo.errorMsg,
    isSaved: state.pibo.isSaved,
  }),
  dispatch => ({
    // 설정 완료 후 저장
    onSave: (pid, data) => {
      dispatch(savePiboCog(pid, data));
    },
    // 설정 할 항목에 대한 내 파이보 설정값과 선택사항 로드
    onMount: (pid, item) => {
      dispatch(fetchCogList(pid, item));
    },
  }),
)(PiboSettingDetailPage);
