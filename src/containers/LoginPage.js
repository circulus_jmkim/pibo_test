import { Redirect } from 'react-router-dom';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Login from '../components/account/Login';
import { signIn } from '../ducks/user';
import { getUid } from '../utils/common';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      pw: '',
      toMain: false,
      toConnect: false,
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      const { pid, uid } = currentUser;
      const { ...state } = this.state;
      if (pid.length === 0) {
        return this.setState({
          ...state,
          toConnect: true,
        });
      }

      if (pid && uid) {
        return this.setState({
          ...state,
          toMain: true,
        });
      }
    }

    return '';
  }

  onChange = (e) => {
    const { id, pw, ...rest } = this.state;
    this.setState({
      id: e.target.name === 'userid' ? e.target.value : id,
      pw: e.target.name === 'password' ? e.target.value : pw,
      ...rest,
    });
  };

  handleLoginClick = () => {
    const { id, pw } = this.state;
    const { onSignIn } = this.props;
    onSignIn({ id, pw });
  };

  handleJoinClick = () => {
    const { history } = this.props;
    history.push('/join');
  };

  handleSkipClick = () => {
    const { history } = this.props;
    history.push('/pibo');
  };

  render() {
    const {
      loading, redirectToMain, redirectToConnect, redirectToAccount, errorMsg,
    } = this.props;
    const { toConnect, toMain } = this.state;
    if (errorMsg.length < 1) {
      if (redirectToAccount) {
        return <Redirect to={`/setting/${getUid()}/update`} />;
      }
      if (redirectToConnect || toConnect) {
        return <Redirect to="/connect" />;
      }
      if (redirectToMain || toMain) {
        return <Redirect to="/pibo" />;
      }
    }
    return (
      <Login
        loading={loading}
        onLogin={this.handleLoginClick}
        onChange={this.onChange}
        onJoin={this.handleJoinClick}
        onSkip={this.handleSkipClick}
        msg={errorMsg}
      />
    );
  }
}

export default connect(
  state => ({
    loading: state.user.loading,
    redirectToConnect: state.user.toConnect,
    redirectToMain: state.user.toMain,
    redirectToAccount: state.user.toAccount,
    errorMsg: state.user.errorMsg,
  }),
  dispatch => ({
    onSignIn: (logindata) => {
      dispatch(signIn(logindata));
    },
  }),
)(LoginPage);
