import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import TopMenu from '../components/TopMenu';
import BotsList from '../components/bots/BotsList';
import BottomMenu from '../components/BottomMenu';
import { fetchBotsList, updateBotsList, fetchAutoCompleteData } from '../ducks/bots';
import { getPid } from '../utils/common';

class BotsPage extends Component {
  static defaultProps = {
    bots: [],
    onMount: () => {},
    updateBots: () => {},
  };

  constructor(props) {
    super(props);
    this.handleUseClick = this.handleUseClick.bind(this);
  }

  state = {
    isSetting: false,
  };

  componentDidMount() {
    const { onMount } = this.props;
    onMount(getPid());
  }

  handleMenuItemClick = (e, { name }) => {
    const { history } = this.props;
    history.push(`/${name}`);
  };

  handleUseClick = (id, use) => {
    const { updateBots } = this.props;
    // const { pid, bots, updateBots } = this.props;
    // const newBots = bots.reduce((acc, item) => {
    //   const { _id, title, _use } = item;
    //   (_id === id) ? acc.push({ id, title, use }) : acc.push({ id, title, use: _use });
    //   return acc;
    // }, []);
    updateBots(getPid(), id, use);
  };

  handleCompleteClick = () => {
    const { isSetting } = this.state;
    this.setState({ isSetting: !isSetting });
  };

  addSearchChange = (value) => {
    const { onChangeResult } = this.props;
    onChangeResult(getPid(), value);
  };

  render() {
    const { bots, results } = this.props;
    const { isSetting } = this.state;
    return (
      <div>
        <TopMenu
          title="bots"
          menuMode={isSetting ? 'WITH_BACK' : 'WITH_CONFIG'}
          handleBackClick={this.handleCompleteClick}
          handleCompleteClick={this.handleCompleteClick}
        />
        <Grid centered padded>
          <BotsList
            isSetting={isSetting}
            bots={bots}
            results={results}
            handleDeleteClick={this.handleDeleteClick}
            handleUseClick={this.handleUseClick}
            addSearchChange={this.addSearchChange}
          />
        </Grid>
        <BottomMenu selectMenu="bots" handleMenuItemClick={this.handleMenuItemClick} />
      </div>
    );
  }
}

export default connect(
  state => ({
    bots: state.bots.bots,
    loading: state.bots.loading,
    errorMsg: state.bots.errorMsg,
    searchStr: state.bots.searchStr,
    searching: state.bots.searching,
    results: state.bots.searchResult,
  }),
  dispatch => ({
    onMount: pid => dispatch(fetchBotsList(pid)),
    updateBots: (pid, bid, use) => dispatch(updateBotsList(pid, bid, use)),
    onChangeResult: (pid, value) => dispatch(fetchAutoCompleteData(pid, value)),
  }),
)(BotsPage);

// withAuth로 로그인 여부 체크하고 페이지로 진입
// 파이보에 설치된 내 앱 리스트(사용 상태 포함) 가져오기
