import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Reveal, Sidebar } from 'semantic-ui-react';
import StoreMenu from '../components/botstore/StoreMenu';
import StoreMybot from '../components/botstore/StoreMybot';
import TopMenu from '../components/TopMenu';
import BottomMenu from '../components/BottomMenu';
import { fetchBotsList, fetchAutoCompleteData } from '../ducks/bots';
import { getPid } from '../utils/common';

class BotstoreMyBotPage extends Component {
  static defaultProps = {
    loading: false,
    searchStr: '',
    searching: false,
    results: [],
    bots: [],
    errorMsg: '',
    onMount: () => {},
    onChangeResult: () => {},
  }

  state = {
    visible: false,
  };

  componentDidMount() {
    const { onMount } = this.props;
    const pid = getPid();
    if (!pid) return;
    onMount(pid);
  }

  handleSidebarHide = () => this.setState({ visible: false });

  handleMyBotClick = () => {
    this.setState({ visible: false });
    const { history } = this.props;
    history.refresh();
  }

  handleMyBotBackClick = () => {
    const { history } = this.props;
    history.goBack();
  }

  addSearchChange = (value) => {
    const { onChangeResult } = this.props;
    const pid = getPid();
    if (!pid) return;
    onChangeResult(value, pid);
  };

  handleMenuItemClick = (e, { name }) => {
    const { history } = this.props;
    history.push(`/${name}`);
  };

  mainContent = () => {
    const { visible } = this.state;
    const { ...rest } = this.props;
    return (
      <Sidebar.Pusher dimmed={visible}>
        <TopMenu title="내 봇" menuMode="WITH_BACK" handleBackClick={this.handleMyBotBackClick} />
        <StoreMybot
          {...rest}
          addSearchChange={this.addSearchChange}
        />
      </Sidebar.Pusher>
    );
  }

  render() {
    const { visible } = this.state;
    return (
      <div>
        <StoreMenu
          visible={visible}
          handleSidebarHide={this.handleSidebarHide}
          handleMyBotClick={this.handleMyBotClick}
          MainPusher={this.mainContent}
        />
        <Reveal>
          <Reveal.Content hidden={visible}>
            <BottomMenu selectMenu="botstore" handleMenuItemClick={this.handleMenuItemClick} />
          </Reveal.Content>
        </Reveal>
      </div>
    );
  }
}

export default connect(
  state => ({
    loading: state.bots.loading,
    searchStr: state.bots.searchStr,
    searching: state.bots.searching,
    results: state.bots.searchResult,
    errorMsg: state.bots.errorMsg,
    bots: state.bots.bots,
  }),
  dispatch => ({
    onMount: pid => dispatch(fetchBotsList(pid)),
    onChangeResult: (value, pid) => dispatch(fetchAutoCompleteData(value, pid)),
  }),
)(BotstoreMyBotPage);
