import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Container, Header } from 'semantic-ui-react';
import { fetchUserInfo, signOut } from '../ducks/user';
import TopMenu from '../components/TopMenu';
import UserSettingList from '../components/pibo/settings/UserSettingList';
import withLoading from '../hocs/withLoading';
import Popup from '../components/Popup';
import { getUid } from '../utils/common';

const UserSettingListWithLoading = withLoading(UserSettingList);

class UserSettingPage extends Component {
  static defaultProps = {
    onMount: () => {},
    user: {
      nickName: '',
      email: '',
      birthDate: 0,
      interest: [],
    },
  };

  state = {
    isPopup: false,
    popupObj: {
      leftLabel: '취소',
      rightLabel: '확인',
      mode: 'confirm',
      isPopup: true,
      leftFunc: () => { this.setState({ isPopup: false }); },
      rightFunc: () => { this.setState({ isPopup: false }); },
      contents: (
        <Container text textAlign="center" />
      ),
    },
  };

  componentDidMount() {
    const { onMount } = this.props;
    const uid = getUid();
    onMount(uid);
  }

  render() {
    const {
      history, onSignOut, isComplete, ...rest
    } = this.props;
    const { isPopup, popupObj } = this.state;

    if (isComplete && isPopup) {
      return <Redirect to="/pibo" />;
    }

    const handleItemClick = (e, { name }) => {
      if (name === 'logout') {
        popupObj.rightLabel = '로그아웃';
        popupObj.mode = 'alert';
        popupObj.contents = (
          <Container text textAlign="center">
            <Header>
              로그아웃
            </Header>
            <p>
              로그아웃 하시겠습니까?
            </p>
          </Container>
        );
        return this.setState({ isPopup: true, popupObj });
      }

      if (getUid()) { return history.push(`/setting/${getUid()}/${name}`); }
    };

    const handleBackClick = () => {
      history.push('/pibo');
    };

    return (
      <div>
        <TopMenu title="계정 설정" menuMode="WITH_BACK" handleBackClick={handleBackClick} />
        <UserSettingListWithLoading {...rest} handleItemClick={handleItemClick} />
        {
          isPopup && (<Popup {...popupObj} rightFunc={onSignOut} />)
        }
      </div>
    );
  }
}

export default connect(
  state => ({
    loading: state.user.loading,
    errorMsg: state.user.errorMsg,
    user: state.user.user,
    isComplete: state.user.isComplete,
  }),
  dispatch => ({
    onMount: (uid) => {
      dispatch(fetchUserInfo(uid));
    },
    onSignOut: () => {
      dispatch(signOut());
    },
  }),
)(UserSettingPage);
