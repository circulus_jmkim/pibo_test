import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header } from 'semantic-ui-react';
import Disconnect from '../components/connect/Disconnect';
import Connecting from '../components/connect/Connecting';
import Complete from '../components/connect/Complete';
import Wifi from '../components/connect/Wifi';
import {
  fetchCurrentSSID, fetchApList, fetchConnectResult, connectAp,
} from '../ducks/connect';
import { getUid, setLocalStorage, getLocale } from '../utils/common';
import PiboNetwork from '../components/pibo/settings/PiboNetwork';
import withLoading from '../hocs/withLoading';

const steps = ['disconnect', 'connecting', 'wifi', 'complete'];
const WifiListWithLoader = withLoading(PiboNetwork);

class ConnectPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stepIdx: 0,
      ap: {
        essid: '',
        encrypted: false,
        address: '',
        password: '',
        signal: 0,
        wpa: false,
      },
      listMode: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { robotId, aplist } = this.props;
    if (robotId !== nextProps.robotId || robotId.length > 0) {
      const uid = getUid();
      const pid = nextProps.robotId;
      setLocalStorage('currentUser', { uid, pid });
    }
    if (aplist !== nextProps.aplist) {
      this.setState({
        stepIdx: 2,
      });
    }
  }

  onPrevClick = () => {
    const { stepIdx, listMode, ...rest } = this.state;
    const { onFetchApList } = this.props;
    if (listMode) {
      return this.setState({
        ...rest,
        stepIdx,
        listMode: false,
      });
    }
    const { history } = this.props;
    const newStepIdx = stepIdx - 1;
    if (steps[newStepIdx] === 'wifi') {
      onFetchApList();
    }
    if (newStepIdx > 0) {
      return this.setState({
        stepIdx: newStepIdx,
      });
    }
    return history.go(-1);
  }

  onNextClick = () => {
    const { stepIdx, listMode, ap } = this.state;
    const { onApConnect } = this.props;
    const newStepIdx = stepIdx + 1;
    if (steps[newStepIdx] === 'complete') {
      if (!listMode && ap.essid) {
        onApConnect(ap);
      }
    }
    if (newStepIdx < steps.length) {
      return this.setState({
        stepIdx: newStepIdx,
      });
    }

    const { history } = this.props;
    return history.push('/pibo');
  }

  onConnect = async () => {
    const { getSSID } = this.props;
    getSSID();
  }

  onAPChange = (e) => {
    const changeTarget = e.currentTarget.name;
    const changeValue = e.currentTarget.value;
    const { listMode, ap, ...rest } = this.state;
    const { onFetchApList } = this.props;
    if (changeTarget === 'apinput') {
      onFetchApList();
      return this.setState({
        ...rest,
        ap,
        listMode: true,
      });
    }
    if (changeTarget === 'appw') {
      const { password, ...others } = ap;
      return this.setState({
        ap: {
          password: changeValue,
          ...others,
        },
        listMode,
        ...rest,
      });
    }

    const { aplist } = this.props;
    const apIdx = changeTarget.split('_')[1];
    const apItem = aplist[apIdx];
    return this.setState({
      ap: {
        ...apItem,
        password: '',
      },
      listMode: false,
      ...rest,
    });
  }

  onConnetComplete = () => {
    const uid = getUid();
    const locale = getLocale();
    const { onRobotConnect, robotId } = this.props;
    onRobotConnect({ uid, robotId, locale });
  }

  render() {
    const {
      onFetchApList, aplist, stepComplete, error, loading, ssid,
    } = this.props;
    const { stepIdx, ap, listMode } = this.state;

    return (
      <div>
        <Header as="h2" floated="left" style={{ alignSelf: 'flex-start', margin: '1rem', color: 'rgba(209, 211, 212, 1)' }}>
          <span style={{ color: 'rgba(3, 191, 215, 1)' }}>
            pi
          </span>
          bo
        </Header>
        {
          (steps[stepIdx] === 'disconnect') && (
            <Disconnect onPrevClick={this.onPrevClick} onNextClick={this.onNextClick} />
          )
        }
        {
          (steps[stepIdx] === 'connecting') && (
            <Connecting
              onPrevClick={this.onPrevClick}
              onNextClick={this.onNextClick}
              onConnect={this.onConnect}
              ssid={ssid}
              errorMsg={error}
              loading={loading}
            />
          )
        }
        {
          (steps[stepIdx] === 'wifi' && !listMode) && (
            <Wifi
              ap={ap}
              aplist={aplist}
              onPrevClick={this.onPrevClick}
              onNextClick={this.onNextClick}
              onChange={this.onAPChange}
            />
          )
        }
        {
          (steps[stepIdx] === 'wifi' && listMode) && (
            <WifiListWithLoader
              isInit
              ap={ap}
              aplist={aplist}
              loading={loading}
              onChange={this.onAPChange}
              onReSearch={onFetchApList}
              onPrevClick={this.onPrevClick}
            />
          )
        }
        {
          (stepComplete && steps[stepIdx] === 'complete') && (
            <Complete onNextClick={this.onNextClick} onMount={this.onConnetComplete} />
          )
        }
      </div>
    );
  }
}

export default connect(
  state => ({
    error: state.connect.errorMsg,
    loading: state.connect.loading,
    aplist: state.connect.aplist,
    robotId: state.connect.robotId,
    ssid: state.connect.ssid,
    stepComplete: state.connect.stepComplete,
  }),
  dispatch => ({
    getSSID: () => {
      dispatch(fetchCurrentSSID());
    },
    onApConnect: (ap) => {
      dispatch(connectAp(ap));
    },
    onRobotConnect: (data) => {
      dispatch(fetchConnectResult(data));
    },
    onFetchApList: () => {
      dispatch(fetchApList());
    },
  }),
)(ConnectPage);
