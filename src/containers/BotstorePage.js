import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Sidebar, Reveal,
} from 'semantic-ui-react';
import TopMenu from '../components/TopMenu';
import BottomMenu from '../components/BottomMenu';
import StoreMain from '../components/botstore/StoreMain';
import StoreMenu from '../components/botstore/StoreMenu';
import { fetchStoreMainData, fetchAutoCompleteData } from '../ducks/botstore';
import { getPid } from '../utils/common';

class BotstorePage extends Component {
  static defaultProps = {
    mainData: { adItems: [], categoryItems: [] },
    loading: false,
    searchStr: '',
    searching: false,
    results: [],
    errorMsg: '',
    onMount: () => {},
    onChangeResult: () => {},
  }

  state = {
    visible: false,
  };

  componentDidMount() {
    const { onMount } = this.props;
    onMount();
  }

  handleSidebarBtnClick = () => {
    const { visible } = this.state;
    this.setState({ visible: !visible });
  }

  handleSidebarHide = () => this.setState({ visible: false })

  handleMyBotClick = () => {
    this.setState({ visible: false });
    const { history } = this.props;
    const pid = getPid();
    if (!pid) return;
    history.push(`/botstore/my/${pid}`);
  }

  handleItemClick = (bid) => {
    const { history } = this.props;
    history.push(`/botstore/${bid}`);
  };

  handleMyBotBackClick = () => {
    const { history } = this.props;
    history.push('/botstore');
  }

  handleRemoveClick = (e) => {
    console.log(e);
  }

  handleUpdateClick = (e) => {
    console.log(e);
  }

  handleMenuItemClick = (e, { name }) => {
    const { history } = this.props;
    history.push(`/${name}`);
  };

  addSearchChange = (value) => {
    const { onChangeResult } = this.props;
    onChangeResult(value);
  };

  mainContent = () => {
    const { visible } = this.state;
    const { mainData, ...rest } = this.props;
    return (
      <Sidebar.Pusher dimmed={visible}>
        <TopMenu title="botstore" menuMode="WITH_MENU" handleCompleteClick={this.handleSidebarBtnClick} />
        <StoreMain
          {...mainData}
          {...rest}
          addSearchChange={this.addSearchChange}
          handleItemClick={this.handleItemClick}
        />
      </Sidebar.Pusher>
    );
  }

  render() {
    const { visible } = this.state;
    return (
      <div>
        <StoreMenu
          visible={visible}
          handleSidebarHide={this.handleSidebarHide}
          handleMyBotClick={this.handleMyBotClick}
          MainPusher={this.mainContent}
        />
        <Reveal>
          <Reveal.Content hidden={visible}>
            <BottomMenu selectMenu="botstore" handleMenuItemClick={this.handleMenuItemClick} />
          </Reveal.Content>
        </Reveal>
      </div>
    );
  }
}

export default connect(
  state => ({
    mainData: state.botstore.mainData,
    loading: state.botstore.loading,
    searchStr: state.botstore.searchStr,
    searching: state.botstore.searching,
    results: state.botstore.searchResult,
    errorMsg: state.botstore.errorMsg,
    bots: state.bots.bots,
  }),
  dispatch => ({
    onMount: () => dispatch(fetchStoreMainData),
    onChangeResult: value => dispatch(fetchAutoCompleteData(value)),
  }),
)(BotstorePage);
// 로그인 여부 따로 체크하지 않음
// botstore에 올라와 있는 모든 bot 리스트 가져오기
// 광고할 bot 배너 이미지와 링크 리스트 (2, 3개)
// bot 리스트 아이템의 정보 (아이템id, 제목, 썸네일, 평점, 평가개수, 명령어 || 상세설명)
