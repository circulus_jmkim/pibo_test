import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container, Header,
} from 'semantic-ui-react';
import TopMenu from '../components/TopMenu';
import PiboSettingList from '../components/pibo/settings/PiboSettingList';
import Popup from '../components/Popup';
import { getPiboData } from '../ducks/pibo';
import withLoading from '../hocs/withLoading';
import { getPid } from '../utils/common';

const PiboSettingListWithLoading = withLoading(PiboSettingList);

class PiboSettingPage extends Component {
  static defaultProps = {
    onMount: () => {},
  }

  state = {
    isPopup: false,
    popupObj: {
      leftLabel: '취소',
      rightLabel: '확인',
      mode: 'confirm',
      isPopup: true,
      leftFunc: () => { this.setState({ isPopup: false }); },
      rightFunc: () => { this.setState({ isPopup: false }); },
      contents: (
        <Container text textAlign="center" />
      ),
    },
  };

  componentDidMount() {
    const { onMount } = this.props;
    onMount(getPid());
  }

  handleOpen = () => this.setState({ isPopup: true })

  handleClose = () => this.setState({ isPopup: false })

  render() {
    const {
      history, match, ...rest
    } = this.props;
    const { path } = match;
    const { isPopup, popupObj } = this.state;

    const handleBackClick = () => {
      history.push(`/${path.split('/')[1]}`);
    };

    const handleItemClick = (e, { name }) => {
      if (name === 'initialize') {
        popupObj.rightLabel = '초기화';
        popupObj.mode = 'alert';
        popupObj.contents = (
          <Container text textAlign="center">
            <Header>
              설정 초기화
            </Header>
            <p>
              설정을 초기화하시겠습니까?
              현재 파이보에 설정된 정보가 삭제되며, Wi-fi 연결이 해제됩니다.
            </p>
          </Container>
        );
        return this.setState({ isPopup: true, popupObj });
      }

      if (name === 'disconnect') {
        popupObj.rightLabel = '연결 해제';
        popupObj.mode = 'alert';
        popupObj.contents = (
          <Container text textAlign="center">
            <Header>
              연결 해제
            </Header>
            <p>
              기기 연결을 해제하시겠습니까?
              연결 해제시 다른 사용자가 연결할 수 있는 상태가 됩니다.
            </p>
          </Container>
        );
        return this.setState({ isPopup: true, popupObj });
      }

      return history.push(`/pibo/${getPid()}/${name}`);
    };

    return (
      <div>
        <TopMenu title="파이보 설정" menuMode="WITH_BACK" handleBackClick={handleBackClick} />
        <PiboSettingListWithLoading {...rest} handleItemClick={handleItemClick} />
        {
          isPopup && (<Popup {...popupObj} />)
        }
      </div>
    );
  }
}

export default connect(
  state => ({
    loading: state.pibo.loading,
    pibo: state.pibo.pibo,
    errorMsg: state.pibo.errorMsg,
  }),
  dispatch => ({
    onMount: (pid) => {
      dispatch(getPiboData(pid));
    },
  }),

)(PiboSettingPage);
