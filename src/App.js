import React, { Component } from 'react';
import thunk from 'redux-thunk';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './ducks';
import './semantic/dist/semantic.min.css';
import TestPage from './containers/test/TestPage';
import botRouter from './containers/bots/BotRouter';

const store = createStore(rootReducer, applyMiddleware(thunk));

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Route path="/" exact component={TestPage} />
            <Switch>
              <Route path="/bots/:name/:page" component={botRouter} />
              <Route path="/bots/:name" component={botRouter} />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
